/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.Loader.require(['Ext.util.SizeMonitor', 'Ext.util.PaintMonitor'], function(){
    Ext.override(Ext.util.SizeMonitor, {
        constructor: function(config) {
            var namespace = Ext.util.sizemonitor;

            if (Ext.browser.is.Firefox) {
                return new namespace.OverflowChange(config);
            } else if (Ext.browser.is.WebKit) {
                if (!Ext.browser.is.Silk && Ext.browser.engineVersion.gtEq('535') && !Ext.browser.engineVersion.ltEq('537.36')) {
                    return new namespace.OverflowChange(config);
                } else {
                    return new namespace.Scroll(config);
                }
            } else if (Ext.browser.is.IE11) {
                return new namespace.Scroll(config);
            } else {
                return new namespace.Scroll(config);
            }
        }
    });

    Ext.override(Ext.util.PaintMonitor, {
        constructor: function(config) {
            if (Ext.browser.is.Firefox || (Ext.browser.is.WebKit && Ext.browser.engineVersion.gtEq('536') && !Ext.browser.engineVersion.ltEq('537.36') && !Ext.os.is.Blackberry)) {
                return new Ext.util.paintmonitor.OverflowChange(config);
            } else {
                return new Ext.util.paintmonitor.CssAnimation(config);
            }
        }
    });
});

Ext.application({
    name: 'Mentor',

    requires: [
        'Ext.MessageBox',
		 'Mentor.Global',
    ],
	controllers: [
        "Main",
		"AppDetail",
		"UpdateMeetingDetail",
		"UpdateProfilePicture"
    ],
	models: [
		"Mentee",
		"Topics",
		"MeetingType",
		"SubTopic",
		"EnterpreneurAction",
		"MentorAction",
		"MeetingHistory",
		"Skills",
		"InviteMentor",
		"PendingRequest",
		"WaitScreenMentor",
		"MeetingPlace"
	],
	stores : [
		"Mentee",
		"Topics",
		"MeetingType",
		"SubTopic",
		"EnterpreneurAction",
		"MentorAction",
		"MeetingHistory",
		"Skills",
		"InviteMentor",
		"PendingRequest",
		"WaitScreenMentor",
		"MeetingPlace"
	],
    views: [
        'Main',
		'Login',
		'SignUp',
		'ForgotPassword',
		'SignOut',
		'Entrepreneur',
		'MeetingDetails',
		'EnterpreneurActionItems',
		//'Timing',
		'TimePickerField',
		'MentorActionItems',
		'MentorReviewForm',
		'MenteeReviewForm',
		'MeetingHistory',
		'MeetingHistoryMentee',
		'DateTime',
		'DateTimePicker',
		'Profile',
		'InviteMentor',
		'MentorPendingRequest',
		'AcceptRejectPendingRequest',
		'WaitScreen',
		'IntroductionScreen',
		'MentorBottomTabView',
		'MenteeBottomTabView',
		'MenteeInvitesTabView',
		'InviteStatusList',
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
		
		Ext.Msg.defaultAllowedConfig.showAnimation = false;
		
        // Initialize the main view
        //Ext.Viewport.add(Ext.create('Mentor.view.Login'));
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		if(MentorLoginUser!=null){
			
			//Set the application config when the application start
			var ApplicationConfig = Ext.decode(localStorage.getItem(Mentor.Global.APPLICATION_CONFIGURATION));
			Mentor.Global.MENTOR_NAME = ApplicationConfig.mentorName;
			Mentor.Global.MENTEE_NAME = ApplicationConfig.menteeName;
			
			/*var MeetingHistory = Ext.create("Mentor.view.MeetingHistory");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[MeetingHistory]
			});*/
			//Load Tabbar for Mentor
			var MentorBottomTabView = Ext.create("Mentor.view.MentorBottomTabView");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[MentorBottomTabView]
			});
			this.getController('AppDetail').getMeetingHistory();
		}
		else if(MenteeLoginUser!=null){
			
			//Set the application config when the application start
			var ApplicationConfig = Ext.decode(localStorage.getItem(Mentor.Global.APPLICATION_CONFIGURATION));
			Mentor.Global.MENTOR_NAME = ApplicationConfig.mentorName;
			Mentor.Global.MENTEE_NAME = ApplicationConfig.menteeName;
			
			
			/*var MeetingHistoryMentee = Ext.create("Mentor.view.MeetingHistoryMentee");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[MeetingHistoryMentee]
			});*/
			//Load Tabbar for Mentee
			var MenteeBottomTabView = Ext.create("Mentor.view.MenteeBottomTabView");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[MenteeBottomTabView]
			});
			this.getController('AppDetail').getMeetingHistory();
		}
		else{
			var login = Ext.create("Mentor.view.Login");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[login]
			});
			/*var introductionScreen = Ext.create("Mentor.view.IntroductionScreen");
			Ext.Viewport.add({
				xtype: "panel",
				layout: "card",
				itemId: "mainviewport",
				items:[introductionScreen]
			});*/
		}
		
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
