Ext.define('Mentor.store.Mentee', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.Mentee",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idMentee'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});