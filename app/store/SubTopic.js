Ext.define('Mentor.store.SubTopic', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.SubTopic",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idSubTopic'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});