Ext.define('Mentor.store.Skills', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.Skills",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idSkills'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});