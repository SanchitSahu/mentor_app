Ext.define('Mentor.store.MeetingPlace', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.MeetingPlace",
		autoLoad:true,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idMeetingPlace'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});