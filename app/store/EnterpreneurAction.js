Ext.define('Mentor.store.EnterpreneurAction', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.EnterpreneurAction",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idEnterpreneurActionItem'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});