Ext.define('Mentor.store.MentorLoginDetail', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.MentorLoginDetail",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idMentorLoginDetail'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});