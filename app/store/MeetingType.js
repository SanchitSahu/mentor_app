Ext.define('Mentor.store.MeetingType', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.MeetingType",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idMeetingType'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});