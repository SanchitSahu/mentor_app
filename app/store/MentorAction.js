Ext.define('Mentor.store.MentorAction', {
    extend: 'Ext.data.Store',
	config:{
		model: "Mentor.model.MentorAction",
		autoLoad:false,
		autoSync:true,
		proxy:{
			type: 'localstorage',
			id: 'idMentorActionItem'
		},
	},
	/*constructor: function(){
		var me = this;
		me.callParent(arguments);
		me.on("beforeload",me.onBeforeLoad, me);
	},
	
	onBeforeLoad: function(store, operation){
		
	}*/
});