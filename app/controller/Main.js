Ext.define('Mentor.controller.Main', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            
        },
        control: {
            "login button[action=doLogin]": {
            	tap: "doLogin"
            },
            "login button[action=btnRegister]": {
            	tap: "doRegister"
            },
			"login button[action=doForgotPassword]":{
				tap : "doForgotPassword"
			},
			"forgotpassword button[action=btnBackForgotPassword]":{
				tap : "btnBackForgotPassword"
			},
			"forgotpassword button[action=btnResetPassword]":{
				tap : "doResetPassword"
			},
			
			"signup button[action=back_signup]": {
            	tap: "backSignUp"
            },
			"signup button[action=doRegisterSignUp]":{
				tap : "doRegisterSignUp"
			},
			"entrepreneur button[action=btnBackEnterpreneur]":{
				tap: 'btnBackEnterpreneur'
			},
			"entrepreneur button[action=btnNextEnterpreneur]":{
				tap: 'btnNextEnterpreneur'
			},
			
			"meetingdetails button[action=btnBackMeetingDetails]":{
				tap: 'btnBackMeetingDetails'
			},
			"meetingdetails button[action = btnNextMeetingDetails]":{
				tap : 'btnNextMeetingDetails'
			},
			"enterpreneur_action_items button[action = btnNextEntrepreneurItems]":{
				tap: 'btnNextEntrepreneurItems'
			},
			"enterpreneur_action_items button[action = btnBackEnterpreneurActionItems]":{
				tap: 'btnBackEnterpreneurActionItems'
			},
			
			"timing button[action = btnNextTiming]":{
				tap: 'btnNextTiming'
			},
			"timing button[action = btnBackTimingScreen]":{
				tap : 'btnBackTimingScreen'
			},
			"entrepreneur button[action = doStartSession]":{
				tap : 'doStartSession'
			},
			"entrepreneur button[action = doEndSession]":{
				tap : 'doEndSession'
			},
			
			"mentor_action_items button[action = btnNextMentorActionItems]":{
				tap: 'btnNextMentorActionItems'
			},
			"mentor_action_items button[action = btnBackMentorActionItems]":{
				tap: 'btnBackMentorActionItems'
			},
			"meetinghistory button[action = btnSignOutMeetingHistory]":{
				tap: 'btnSignOutMeetingHistory'
			},
			"meetinghistorymentee button[action = btnSignOutMeetingHistoryMentee]":{
				tap: 'btnSignOutMeetingHistoryMentee'
			},
			"signout button[action = btnSignOut]":{
				tap: 'btnSignOut'
			},
			"signout button[action = btnbackSignOut]":{
				tap : 'btnbackSignOut'
			},
			"datepickerfield[itemId=startSessionDatePicker]": {
				change: 'startSessionDatePicker'
			},
			"introductionscreen button[action=btnSkipIntroductionScreen]": {
            	tap: "btnSkipIntroductionScreen"
            },
			
        }
    },
    
    //called when the Application is launched, remove if not needed
    launch: function(app) {
       SERVER_URL : "http://web.mentor.virtualtryon.biz/mentorService";
    },
	
	startSessionDatePicker:function(){
		
	},
	
	doLogin : function(btn){
		me = this;
		
		var Email = Ext.ComponentQuery.query('#emailLogin')[0].getValue();
		var Password = Ext.ComponentQuery.query('#passwordLogin')[0].getValue();
		//var Mentee = Ext.ComponentQuery.query('#radioBtnMenteeLogin')[0].isChecked();
		//var Mentor = Ext.ComponentQuery.query('#radioBtnMentorLogin')[0].isChecked();
		//var UserType;
		if(Email == "")
			Ext.Msg.alert('MELS', 'Please enter user name.', Ext.emptyFn);
		else if( Password == "")
			Ext.Msg.alert('MELS', 'Please enter password.', Ext.emptyFn);
		/*else if(me.ValidateEmail(Email)==false)	
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");*/
		else{
			/*if(Mentee == true)
				UserType = "1" // Mentee
			else if(Mentor == true)
				UserType= "0"  // Mentor*/
			me.LoginUser(Email,Password);
		}
		
		
		
	},
	
	LoginUser : function(strEmail,strPassword){
		var GLOBAL = Mentor.Global;

		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			UserName : strEmail,
			Password : strPassword,
			
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "checkLogin",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.checkLogin.data;
				if(data.checkLogin.Error == 1 || data.checkLogin.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.checkLogin.Message);
					},100);
					return;
				}
				else{
					Ext.getCmp('emailLogin').setValue("");
					Ext.getCmp('passwordLogin').setValue("");
				
					//get the Application Configuation
					me.getApplicationConfiguration();
				
					if(data.checkLogin.data.UserType == "0"){ //Mentor
						localStorage.setItem("idMentorLoginDetail",Ext.encode(data.checkLogin.data));
						var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
						me.getMentee(MentorLoginUser.Id,true);
					}
					else if(data.checkLogin.data.UserType == "1") {//Mentee
						localStorage.setItem("idMenteeLoginDetail",Ext.encode(data.checkLogin.data));
						me.getEnterprenuerAction();
						
					}
						
					/*var viewport = Ext.Viewport,
					mainPanel = viewport.down("#mainviewport");
					var mainMenu = mainPanel.down("entrepreneur");
					if(!mainMenu){
						mainMenu = mainPanel.add({xtype: "entrepreneur"});
					}
					mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});*/
		
				/*	if(strUserType ==  1){ //Mentee
						me.getSkills();
					}
					else
						me.getMentee();*/
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	doRegisterSignUp : function(btn){
		console.log(111)
		me = this;
		var Email = Ext.ComponentQuery.query('#emailSignUp')[0].getValue();
		var Password = Ext.ComponentQuery.query('#passwordSignUp')[0].getValue();
		var AccessToken = Ext.ComponentQuery.query('#accessTokenSignUp')[0].getValue();
		var Mentee = Ext.ComponentQuery.query('#radioBtnMentee')[0].isChecked();
		var Mentor = Ext.ComponentQuery.query('#radioBtnMentor')[0].isChecked();
		var UserType;
		if(Email == "")
			Ext.Msg.alert('MELS', 'Please enter email address.', Ext.emptyFn);
		else if( Password == "")
			Ext.Msg.alert('MELS', 'Please enter password.', Ext.emptyFn);
		else if( AccessToken == "")
			Ext.Msg.alert('MELS', 'Please enter accesstoken that you received in mail.', Ext.emptyFn);
		else if(me.ValidateEmail(Email)==false)	
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");
		else if(Password.length < 6)
			Ext.Msg.alert('MELS',"Password length must be minimum 6 and maximum 8 characters.");
		else{
			if(Mentee == true)
				UserType = "1" // Mentee
			else if(Mentor == true)
				UserType= "0"  // Mentor
			me.RegisterUser(Email,Password,AccessToken,UserType);
		}
		
	},
	RegisterUser:function(strEmail,strPassword,strAccessToken,strUserType){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			Email : strEmail,
			Password : strPassword,
			AccessToken :strAccessToken,
			UserType : strUserType
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "registerUser",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.registerUser.data;
				if(data.registerUser.Error == 1 || data.registerUser.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.registerUser.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.registerUser.data));
					Ext.getCmp('emailSignUp').setValue("");
					Ext.getCmp('passwordSignUp').setValue("");
					Ext.getCmp('accessTokenSignUp').setValue("");
					
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', 'Registered successfully.', function(text) {
							var viewport = Ext.Viewport,
							mainPanel = viewport.down("#mainviewport");
							var mainMenu = mainPanel.down("login");
							if(!mainMenu){
								mainMenu = mainPanel.add({xtype: "login"});
							}
							mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
						});
					},100);
				
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	doForgotPassword : function(btn){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("forgotpassword");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "forgotpassword"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide",direction: 'right'});
	},
	
	doResetPassword:function(btn){
		me = this;
		var GLOBAL = Mentor.Global;
		var Email = Ext.ComponentQuery.query('#emailForgotPassword')[0].getValue();
		//var radionBtnMentee = Ext.ComponentQuery.query('#radioBtnMenteeForgotPassword')[0].isChecked();
		//var radionBtnMentor = Ext.ComponentQuery.query('#radioBtnMentorForgotPassword')[0].isChecked();
		//var UserType;
		if(Email == "")
			Ext.Msg.alert('MELS', 'Please enter email address.', Ext.emptyFn);
		else if(me.ValidateEmail(Email)==false)	
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");
		else{
			/*if(radionBtnMentee == true)
				UserType = "1" // Mentee
			else if(radionBtnMentor == true)
				UserType= "0"  // Mentor
			*/
			Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
			});
			
			var data = {
				Email : Email,
				
			}			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "forgotPassword",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.forgotPassword.data;
					if(data.forgotPassword.Error == 1 || data.forgotPassword.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.forgotPassword.Message);
						},100);
						return;
					}
					else{
						Ext.getCmp('emailForgotPassword').setValue("");
						//Ext.Msg.alert('MELS', data.forgotPassword.Message);
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', "Password reset instructions have been emailed to the address you gave.");
						},100);
						
					}
				
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					},100);
					console.log(responce);
				}
			});
			
			
		}
	},
	
	btnBackForgotPassword : function(btn){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("login");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "login"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide",direction: 'left'});
	},
	
	doRegister : function(btn){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("signup");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "signup"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});
	},
	
	backSignUp : function(){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("login");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "login"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
	},
	
	btnSignOutMeetingHistory : function(){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("signout");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "signout"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
		
		
	},
	btnSignOutMeetingHistoryMentee: function(){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("signout");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "signout"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
		
		
	},
	
	btnSignOut:function(){
	
		localStorage.clear();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		mainPanel.removeAll();
		localStorage.clear();
		Mentor.Global.NavigationStack = [];
		Mentor.Global.MEETING_DETAIL = null;
		var mainMenu = mainPanel.down("login");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "login"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
		
	},
	btnbackSignOut : function(){
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "left"});
	},
	
	// Enterpreneur (Mentee)
	btnNextEnterpreneur : function(btn){
		var me = this;
		/*var MeetingDetailSubTopicStore = Ext.getStore('SubTopic').load();
		var MeetingDetailPanel = Ext.getCmp('idMeetingDetailSubTopic');
		for(i = 0 ; i<MeetingDetailSubTopicStore.data.length;i++){
			var checkBoxField = Ext.create('Ext.field.Checkbox', {
				name: 'recorded_stream',
				value: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicID,
				label: MeetingDetailSubTopicStore.data.getAt(i).data.SubTopicDescription,
				labelCls : 'radio-btn-cls',
				labelWidth: 250,
				listeners: {
					'check': function(radio, e, eOpts) {
						//me.radioHandler(radio.getValue());
					}
				}
			});
			MeetingDetailPanel.add(checkBoxField);
		}*/
		
		//Store Metting Detail (Change for New UI)
		/*var MeetingTypePanel = Ext.getCmp('idMeetingType');
		for(i = 0 ; i <MeetingTypePanel.getItems().length;i++){
			if(MeetingTypePanel.getItems().getAt(i).isChecked())
			{
				var store = Ext.getStore("Mentee");
				var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
				var MeenteeDetail = {"MeetngTypeID" : MeetingTypePanel.getItems().getAt(i).getValue(),
					"MeetingTypeTitle" : MeetingTypePanel.getItems().getAt(i).getLabel(),
					"MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
					"MenteeID" : MenteeSelectField.getValue()}
				
				localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
				//localStorage.setItem("storeMenteedID",Ext.encode(MeetingTypePanel.getItems().getAt(i).getValue()));
			}
		}*/
		//Store Metting Detail (Change for New UI)
		if(Mentor.Global.timer !="")
			clearInterval(Mentor.Global.timer);
		
		var store = Ext.getStore("Mentee");
		var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
		
		var storeMettingType = Ext.getStore("MeetingType");
		var MettingTypeSelectField = Ext.getCmp('idMettingTypeSelectField');
		
		
		/*** Time Screen Move to Enterpreneur Screen***/
		/*var MeenteeDetail = {"MeetngTypeID" : MettingTypeSelectField.getValue(),
			"MeetingTypeTitle" :  storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
			"MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
			"MenteeID" : MenteeSelectField.getValue()}
		
		localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
		
		
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("timing");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "timing"});
		}
		
		Ext.getCmp('startSession').setText('Start Session');
		Ext.getCmp('startSession').setDisabled(false);
		Ext.getCmp('endSession').setText('End Session');
		Ext.getCmp('endSession').setDisabled(true);
		Ext.getCmp('idBtnNextTiming').setDisabled(true);
		
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
		
		*/
		/*** End Time Screen Move to Enterpreneur Screen***/
		
		me.btnNextTiming();
		
		
	},
	btnBackEnterpreneur : function(){
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("meetinghistory");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "meetinghistory"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
		
		localStorage.setItem("storeStartEndSession",null); // Start and End Session
		
		try{
			clearInterval(Mentor.Global.timer);
		}
		catch(e){
			
		}
	},

	
	// Timing Scrren(Topics Screen)
	btnNextTiming : function(){
		
		//Store Timing Detail (Topics)
		var store = Ext.getStore("Topics");
		var TopicsSelectFieldTimingScreen = Ext.getCmp('idTopicsTimingScreen');
		var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		
		var store = Ext.getStore("Mentee");
		var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
		
		var storeMettingType = Ext.getStore("MeetingType");
		var MettingTypeSelectField = Ext.getCmp('idMettingTypeSelectField');
		
		var storeMeetingPlace = Ext.getStore("MeetingPlace");
		var MeetingPlaceSelectField = Ext.getCmp('idMeetingPlaceSelectField');
		
		
		if(MeetingDetail!=null){
			if(isNaN(Date.parse(Ext.getCmp('startSessionEditMettingDetail').getValue())))
				Ext.Msg.alert('MELS',"Start session date is not valid");
			else if(isNaN(Date.parse(Ext.getCmp('endSessionEditMettingDetail').getValue())))
				Ext.Msg.alert('MELS',"End session date is not valid");
			else{
				
				var MeenteeDetail = {"MeetngTypeID" : MettingTypeSelectField.getValue(),
					"MeetingTypeTitle" :  storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
					"MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
					"MenteeID" : MenteeSelectField.getValue(),
					"StartDate" : Ext.getCmp('startSessionEditMettingDetail').getValue(),
					"MeetingEndDatetime" : Ext.getCmp('endSessionEditMettingDetail').getValue(),
					"TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
					"TopicID" : TopicsSelectFieldTimingScreen.getValue(),
					"MeetingElapsedTime" : ElapsedTime.getValue(),
					"MeetingPlaceID" : MeetingPlaceSelectField.getValue(),
					"MeetingPlaceTitle" :  storeMeetingPlace.findRecord(MeetingPlaceSelectField.getValue()).get("MeetingPlaceName")}
				
				localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail))
				
				
				/*var TopicsTimingDetail = {"StartDate" : Ext.getCmp('startSessionEditMettingDetail').getValue(),
								"MeetingEndDatetime" : Ext.getCmp('endSessionEditMettingDetail').getValue(),
								"TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
								"TopicID" : TopicsSelectFieldTimingScreen.getValue(),
								"MeetingElapsedTime" : ElapsedTime.getValue()}
							
					localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/
				
					var SubTopicStore = Ext.getStore('SubTopic');
					SubTopicStore.clearFilter();
					SubTopicStore.filterBy(function(rec) {
						return rec.get('TopicID') ===  TopicsSelectFieldTimingScreen.getValue();
					});
					SubTopicStore.load();
					
					var viewport = Ext.Viewport,
					mainPanel = viewport.down("#mainviewport");
					var mainMenu = mainPanel.down("meetingdetails");
					if(!mainMenu){
						mainMenu = mainPanel.add({xtype: "meetingdetails"});
					}
					Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
					mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
					
					
			}
			
		}
		else{
			if(Ext.getCmp('startSession').getText() == "" || Ext.getCmp('startSession').getText() == "Start Session" )
				Ext.Msg.alert('MELS',"Please click on start session to start meeting");
			else if(Ext.getCmp('endSession').getText() == "" || Ext.getCmp('endSession').getText() == "End Session" )
				Ext.Msg.alert('MELS',"Please click on end session to enter meeting duration.");
			else{
				var startDate = new Date(Ext.getCmp('startSession').getText());
				var endDate = new Date (Ext.getCmp('endSession').getText());
				
				if(endDate - startDate < 0){
					Ext.Msg.alert('MELS',"End Session datetime should be greater than Start Seesion datetime");
				}
				else{
					if(Mentor.Global.timer !="")
						clearInterval(Mentor.Global.timer);
					var MeenteeDetail = {"MeetngTypeID" : MettingTypeSelectField.getValue(),
						"MeetingTypeTitle" :  storeMettingType.findRecord(MettingTypeSelectField.getValue()).get("MeetingTypeName"),
						"MenteeName" : store.findRecord(MenteeSelectField.getValue()).get("MenteeName"),
						"MenteeID" : MenteeSelectField.getValue(),
						"StartDate" : Ext.getCmp('startSession').getText(),
						"MeetingEndDatetime" : Ext.getCmp('endSession').getText(),
						"TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
						"TopicID" : TopicsSelectFieldTimingScreen.getValue(),
						"MeetingElapsedTime" : ElapsedTime.getValue(),
						"MeetingPlaceID" : MeetingPlaceSelectField.getValue(),
						"MeetingPlaceTitle" :  storeMeetingPlace.findRecord(MeetingPlaceSelectField.getValue()).get("MeetingPlaceName")}
					
					localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail))
					
					/*var TopicsTimingDetail = {"StartDate" : Ext.getCmp('startSession').getText(),
								"MeetingEndDatetime" : Ext.getCmp('endSession').getText(),
								"TopicName" : store.findRecord(TopicsSelectFieldTimingScreen.getValue()).get("TopicDescription"),
								"TopicID" : TopicsSelectFieldTimingScreen.getValue(),
								"MeetingElapsedTime" : ElapsedTime.getValue()}
							
					localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/
					
					var MeetingStartEndDetail = {"StartDate" : Ext.getCmp('startSession').getText(),
											"MeetingEndDatetime" : Ext.getCmp('endSession').getText(),
											"MeetingElapsedTime" : Ext.getCmp('MeetingElapsedTime').getValue()}
										
					localStorage.setItem("storeStartEndSession",Ext.encode(MeetingStartEndDetail));
					
				
					var SubTopicStore = Ext.getStore('SubTopic');
					SubTopicStore.clearFilter();
					SubTopicStore.filterBy(function(rec) {
						return rec.get('TopicID') ===  TopicsSelectFieldTimingScreen.getValue();
					});
					SubTopicStore.load();
					
					
					var viewport = Ext.Viewport,
					mainPanel = viewport.down("#mainviewport");
					var mainMenu = mainPanel.down("meetingdetails");
					if(!mainMenu){
						mainMenu = mainPanel.add({xtype: "meetingdetails"});
					}
					Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
					mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
					
					
				}
			}
		}
		
	},
	calculateElapsedTime:function(){
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		if(MeetingDetail!=null){
			var StartSessionDate = new Date(Ext.getCmp('startSessionEditMettingDetail').getValue());
			var EndSessionDate;
				//if(Mentor.Global.timer == "")
					EndSessionDate = new Date(Ext.getCmp('endSessionEditMettingDetail').getValue());
				//else
				//	EndSessionDate = new Date();
			
			var DateDiff= EndSessionDate - StartSessionDate;
			var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
			var diffMins = Math.round(((DateDiff % 86400000) % 3600000) / 60000);
			
		
			Ext.getCmp("min").setHtml("00");
			Ext.getCmp("hrs").setHtml("00");
			
			
			if(diffMins<9)
				Ext.getCmp("min").setHtml("0"+diffMins);
			else
				Ext.getCmp("min").setHtml(diffMins);
			
			if(diffHrs<9)
				Ext.getCmp("hrs").setHtml("0"+diffHrs);
			else
				Ext.getCmp("hrs").setHtml(diffHrs);
			
			
			var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
			if(diffHrs > 0)
				diffMins = diffMins + (diffHrs * 60);
			ElapsedTime.setValue( diffMins + " Minutes");
			
			if(isNaN(diffHrs) || diffHrs<0)
				Ext.getCmp("hrs").setHtml("00");
			if(isNaN(diffMins) || diffMins<0)
				Ext.getCmp("min").setHtml("00");
			
			
			
		}
		else{
			var StartSessionDate = new Date(Ext.getCmp('startSession').getText());
			var EndSessionDate;
				if(Mentor.Global.timer == "")
					EndSessionDate = new Date(Ext.getCmp('endSession').getText());
				else
					EndSessionDate = new Date();
			
			var DateDiff= EndSessionDate - StartSessionDate;
			var diffHrs = parseInt((DateDiff % 86400000) / 3600000); // hours
			var diffMins = Math.round(((DateDiff % 86400000) % 3600000) / 60000);
			
			if(diffMins<9)
				Ext.getCmp("min").setHtml("0"+diffMins);
			else
				Ext.getCmp("min").setHtml(diffMins);
			
			if(diffHrs<9)
				Ext.getCmp("hrs").setHtml("0"+diffHrs);
			else
				Ext.getCmp("hrs").setHtml(diffHrs);
			
			var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
			if(diffHrs > 0)
				diffMins = diffMins + (diffHrs * 60);
			ElapsedTime.setValue( diffMins + " Minutes");
			
			
		}
		
		
	},
	doStartSession:function(){
		me = this;
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		if(MeetingDetail == null){
			
			var currentDate = me.getDate();
			var FullYear = currentDate.getFullYear();
			var Month = currentDate.getMonth() + 1;
			var Date = currentDate.getDate();
			var Hours = currentDate.getHours();
			if(Hours < 9)
				Hours = "0"+Hours;
			var Minutes = currentDate.getMinutes();
			if(Minutes < 9)
				Minutes = "0"+Minutes;
			var StartSessionDateTime = FullYear + "-" + Month + "-" + Date + " " + Hours  + ":" + Minutes;
			
			/*Ext.getCmp('startSession').setText(StartSessionDateTime);
			Ext.getCmp('startSession').setDisabled(true);
			Ext.getCmp('endSession').setDisabled(false);
			
			var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
			ElapsedTime.setValue("0 Minutes");
			
			Mentor.Global.timer = setInterval(function(){
				Mentor.app.application.getController('Main').calculateElapsedTime();
				console.log("a")
			},10000);*/
			
			// 22/02/2016 Move Timing Screen to Enterpreneur Screen
			/*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
			var MeenteeID  = MeetingDetail.MenteeID;
			*/
			var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
			var MeenteeID  = MenteeSelectField.getValue();
			
			me.startEndMeetingSession(1,StartSessionDateTime,MeenteeID);
		}
		
	},
	doEndSession:function(){
		me = this;
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		if(MeetingDetail == null){
		
			if(Ext.getCmp('startSession').getText() == "")
				Ext.Msg.alert('MELS',"Please start session first");
			else{
				
				var currentDate = me.getDate();
				var FullYear = currentDate.getFullYear();
				var Month = currentDate.getMonth() + 1;
				var Date = currentDate.getDate();
				var Hours = currentDate.getHours();
				if(Hours < 9)
					Hours = "0"+Hours;
				var Minutes = currentDate.getMinutes();
				if(Minutes < 9)
					Minutes = "0"+Minutes;
				var StartSessionDateTime = FullYear + "-" + Month + "-" + Date + "-" + Hours  + ":" + Minutes;
			
				/*Ext.getCmp('endSession').setText(StartSessionDateTime);
				Ext.getCmp('endSession').setDisabled(true);
				Ext.getCmp('idBtnNextTiming').setDisabled(false);
				
				if(Mentor.Global.timer !="")
					clearInterval(Mentor.Global.timer);
				*/

				// 22/02/2016 Move Timing Screen to Enterpreneur Screen				
				/*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
				var MeenteeID  = MeetingDetail.MenteeID;
				*/
				
				var MenteeSelectField = Ext.getCmp('idEnterpreneurSelectField');
				var MeenteeID  = MenteeSelectField.getValue();
				me.startEndMeetingSession(2,StartSessionDateTime,MeenteeID);
			}
		}
		
	},
	getDate:function(){
		return new Date();
	},
	
	btnBackTimingScreen : function(btn){
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("entrepreneur");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "entrepreneur"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
			var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		if(MeetingDetail == null)
		{	
			if((Ext.getCmp('startSession').getText() != "" && Ext.getCmp('startSession').getText() != "Start Session") &&
				(Ext.getCmp('endSession').getText() == "" || Ext.getCmp('endSession').getText() == "End Session" ))
			{
				Ext.Msg.confirm("MELS", "This meeting is still in session. Do you wish to end this meeting?", function(btn){
					if (btn == 'yes'){
						var currentDate = me.getDate();
						var FullYear = currentDate.getFullYear();
						var Month = currentDate.getMonth() + 1;
						var Date = currentDate.getDate();
						var Hours = currentDate.getHours();
						if(Hours < 9)
							Hours = "0"+Hours;
						var Minutes = currentDate.getMinutes();
						if(Minutes < 9)
							Minutes = "0"+Minutes;
						var StartSessionDateTime = FullYear + "-" + Month + "-" + Date + "-" + Hours  + ":" + Minutes;
					
						/*Ext.getCmp('endSession').setText(StartSessionDateTime);
						Ext.getCmp('endSession').setDisabled(true);
						Ext.getCmp('idBtnNextTiming').setDisabled(false);
						
						if(Mentor.Global.timer !="")
							clearInterval(Mentor.Global.timer);
						*/	
						var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
						var MeenteeID  = MeetingDetail.MenteeID;
					
						me.startEndMeetingSession(2,StartSessionDateTime,MeenteeID);
					}
				});
					
			}
			else{
			
				if((Ext.getCmp('startSession').getText() != "" && Ext.getCmp('startSession').getText() != "Start Session") &&
					(Ext.getCmp('endSession').getText() != "" && Ext.getCmp('endSession').getText() != "End Session" )){
						Ext.Msg.confirm("MELS", "Do you want to save meeting start and end time details?", function(btn){
						if (btn == 'yes'){
							var startDate = new Date(Ext.getCmp('startSession').getText());
							var endDate = new Date (Ext.getCmp('endSession').getText());
							
							if(endDate - startDate < 0){
								Ext.Msg.alert('MELS',"End Session datetime should be greater than Start Seesion datetime");
							}
							else{
								if(Mentor.Global.timer !="")
									clearInterval(Mentor.Global.timer);
								
								var TopicsTimingDetail = {"StartDate" : Ext.getCmp('startSession').getText(),
											"MeetingEndDatetime" : Ext.getCmp('endSession').getText(),
											"MeetingElapsedTime" : Ext.getCmp('MeetingElapsedTime').getValue()}
										
								localStorage.setItem("storeStartEndSession",Ext.encode(TopicsTimingDetail));
							
								var View = Mentor.Global.NavigationStack.pop();
								var viewport = Ext.Viewport,
								mainPanel = viewport.down("#mainviewport");
							
								mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
							}
						}
						else{
							var View = Mentor.Global.NavigationStack.pop();
							var viewport = Ext.Viewport,
								mainPanel = viewport.down("#mainviewport");
							
							mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
							
							if(typeof Mentor.Global.timer !== 'undefined'){
								if(Mentor.Global.timer !="")
									clearInterval(Mentor.Global.timer);
							}
							localStorage.setItem("storeStartEndSession",null);
						}
					});
				}
				else{
					var View = Mentor.Global.NavigationStack.pop();
					var viewport = Ext.Viewport,
						mainPanel = viewport.down("#mainviewport");
					
					mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
					localStorage.setItem("storeStartEndSession",null);
				}
			}
		}
		else{
			var View = Mentor.Global.NavigationStack.pop();
			var viewport = Ext.Viewport,
				mainPanel = viewport.down("#mainviewport");
			
			mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
			localStorage.setItem("storeStartEndSession",null);
		}
	},
	
	
	// Meeting Detail Screen
	btnNextMeetingDetails : function(){
	
		// Store Metting Detail Screen (Sub Topics)
		var MeetingDetailSubTopicsPanel = Ext.getCmp('idMeetingDetailSubTopic');
		var SubTopicsID = "";
		var SubTopicsName = ""
		var isChecked = 0;
		for(i = 0 ; i <MeetingDetailSubTopicsPanel.getItems().length;i++){
			if(MeetingDetailSubTopicsPanel.getItems().getAt(i).isChecked())
			{
				if(isChecked == 0){
					SubTopicsID = MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
					SubTopicsName = MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
				else{
					SubTopicsID = SubTopicsID + ","+MeetingDetailSubTopicsPanel.getItems().getAt(i).getValue();
					SubTopicsName = SubTopicsName + ","+MeetingDetailSubTopicsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
			}
		}
		//if(isChecked == 0){
			//Ext.Msg.alert('MELS','Please select at least one meeting detail.');
		//}
		//else{
			var MeetingDetailSubTopics = {"SubTopicsID" : SubTopicsID,
							"SubTopicsTitle" : SubTopicsName}
						
			localStorage.setItem("storeMeetingDetailSubTopics",Ext.encode(MeetingDetailSubTopics));
			 
			
			
			var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
			var mainMenu = mainPanel.down("enterpreneur_action_items");
			if(!mainMenu){
				mainMenu = mainPanel.add({xtype: "enterpreneur_action_items"});
			}
			Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
			mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
		//}
	},
	btnBackMeetingDetails : function(btn){

		/*var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("timing");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "timing"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		if(typeof Ext.getCmp('idBtnNextTiming') !== 'undefined')
			Ext.getCmp('idBtnNextTiming').setDisabled(false);
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	
	
	// Entrepreneur Action Items Screen
	btnNextEntrepreneurItems : function(){
		me = this;
		// Store Mentee Action item (Mentee Action Item)
		var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
		var MenteeActionItemID;
		var MenteeActionItemName
		var isChecked = 0;
		for(i = 0 ; i <EntrepreneurItemsPanel.getItems().length;i++){
			if(EntrepreneurItemsPanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					MenteeActionItemID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
					MenteeActionItemName = EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
				else{
					MenteeActionItemID = MenteeActionItemID + ","+EntrepreneurItemsPanel.getItems().getAt(i).getValue();
					MenteeActionItemName = MenteeActionItemName + ","+EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
			}
		}
		
		if(isChecked == 0){
			Ext.Msg.alert('MELS','Please select at least one action.');
		}
		else{
			var EntrepreneurActionItems = {"MenteeActionItemID" : MenteeActionItemID,
							"MenteeActionItemName" : MenteeActionItemName}
						
			localStorage.setItem("storeEntrepreneurActionItems",Ext.encode(EntrepreneurActionItems));
			
			
			if(Ext.getCmp('idBtnNextEntrepreneurItems').getText() == "Update"){
				me.updateMenteeActionItemsWithDone();
			}
			else{
				var viewport = Ext.Viewport,
				mainPanel = viewport.down("#mainviewport");
				var mainMenu = mainPanel.down("mentor_action_items");
				if(!mainMenu){
					mainMenu = mainPanel.add({xtype: "mentor_action_items"});
				}
				Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
				mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
			}
		}
	},
	btnBackEnterpreneurActionItems:function(btn){
		/*var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("meetingdetails");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "meetingdetails"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	
	// Mentor Action items
	btnNextMentorActionItems : function(btn){
		me = this;
		// Store Mentee Action item (Mentee Action Item)
		var MentorActionItemsPanel = Ext.getCmp('idMentorActionItem');
		var MentorActionItemID;
		var MentorActionItemName
		var isChecked = 0;
		var MentorFeedback = Ext.getCmp('idFeedbackSelectFieldMentorActionTaken');
		for(i = 0 ; i <MentorActionItemsPanel.getItems().length;i++){
			if(MentorActionItemsPanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					MentorActionItemID = MentorActionItemsPanel.getItems().getAt(i).getValue();
					MentorActionItemName = MentorActionItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
				else{
					MentorActionItemID = MentorActionItemID + ","+MentorActionItemsPanel.getItems().getAt(i).getValue();
					MentorActionItemName = MentorActionItemName + ","+MentorActionItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
			}
		}
	
		// validation
		if(isChecked == 0){
			Ext.Msg.alert('MELS','Please select at least one action.');
		}
		else{
			var MentorActionItems = {"MentorActionItemID" : MentorActionItemID,
						"MentorActionItemName" : MentorActionItemName,
						"Feedback" : MentorFeedback.getValue()};
					
			localStorage.setItem("storeMentorActionItems",Ext.encode(MentorActionItems));
		
			me.SubmitDetail();
		}
	},
	btnBackMentorActionItems:function(btn){
		/*var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("enterpreneur_action_items");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "enterpreneur_action_items"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	
	//Select Enterpreneur screen (It will be call at the of login also and at the time of New Meeting and Edit Metting-When
	//	Enterpreneur screen calls if isLogin true it means its call from login else it call at the time of Enterprenur screen)
	getMentee : function(MentorID,isLogin){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		var data = {
			MentorID : MentorID,
		}	
		
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMentee",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMentee.data;
				if(data.getMentee.Error == 1){
					Ext.Function.defer(function(){
						Ext.getStore('Mentee').removeAll();
						Ext.Msg.alert('MELS', data.getMentee.Message);
					},100);
					return;
				}
				else if( data.getMentee.Error == 2){
					Ext.getStore('Mentee').removeAll();
					if(isLogin) // If Service at a time Of Login
						me.getTopic();
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getMentee.data));
					Ext.getStore('Mentee').removeAll();
					Ext.getStore('Mentee').add(data.getMentee.data);
					if(isLogin) // If Service at a time Of Login
						me.getTopic();
					else{
						//Check if user comes from meeting detail 
						var MeetingDetail = Mentor.Global.MEETING_DETAIL;
						if(MeetingDetail!=null){
							Ext.getCmp('idEnterpreneurSelectField').setValue(MeetingDetail.MenteeID);
						}
					}
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					Ext.getStore('Mentee').removeAll();
				},100);
				console.log(responce);
			}
		});
	},
	
	getTopic : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getTopic",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getTopic.data;
				if(data.getTopic.Error == 1 || data.getTopic.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getTopic.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					Ext.getStore('Topics').add(data.getTopic.data);
					
					var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
					var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
					if(MentorLoginUser!=null){
						me.getMeetingType();
					}
					else if(MenteeLoginUser!=null){
						me.getSkills();
						
					}
					//me.getMeetingType();
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	getMeetingType : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMeetingType",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMeetingType.data;
				if(data.getMeetingType.Error == 1 || data.getMeetingType.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMeetingType.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					Ext.getStore('MeetingType').add(data.getMeetingType.data);
					me.getMeetingDetailSubTopic();

				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	getMeetingDetailSubTopic : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url :GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getSubTopic",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getSubTopic.data;
				if(data.getSubTopic.Error == 1 || data.getSubTopic.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getSubTopic.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					Ext.getStore('SubTopic').add(data.getSubTopic.data);
					//var MeetingTypeStore = Ext.getStore('SubTopic');

					me.getEnterprenuerAction();
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	getEnterprenuerAction : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMenteeActionTaken",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMenteeActionTaken.data;
				if(data.getMenteeActionTaken.Error == 1 || data.getMenteeActionTaken.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMenteeActionTaken.Message);
					},100);
					return;
				}
				else{
					Ext.getStore('EnterpreneurAction').add(data.getMenteeActionTaken.data);
					
					var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
					var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
					if(MentorLoginUser!=null){
						me.getMentorAction();
					}
					else if(MenteeLoginUser!=null){
						//me.getSkills();
						me.getTopic();
					}
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	getMentorAction : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMentorActionTaken",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMentorActionTaken.data;
				if(data.getMentorActionTaken.Error == 1 || data.getMentorActionTaken.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMentorActionTaken.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					Ext.getStore('MentorAction').add(data.getMentorActionTaken.data);
					//var MeetingTypeStore = Ext.getStore('SubTopic');
						me.getMettingPlace();
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	//Get Metting Place
	getMettingPlace : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMeetingPlace",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMeetingPlace.data;
				if(data.getMeetingPlace.Error == 1 || data.getMeetingPlace.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMeetingPlace.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					Ext.getStore('MeetingPlace').add(data.getMeetingPlace.data);
					//var MeetingTypeStore = Ext.getStore('SubTopic');
					me.getSkills();
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	getSkills : function(){
		var GLOBAL = Mentor.Global;
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getSkills",
				body : "{}"
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getSkills.data;
				if(data.getSkills.Error == 1 || data.getSkills.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getSkills.Message);
					},100);
					return;
				}
				else{
					//localStorage.setItem("idMentor",Ext.encode(data.getTopic.data));
					
					var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
					var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
					Ext.getStore('Skills').add(data.getSkills.data);
					
					if(MentorLoginUser!=null){
						
						var viewport = Ext.Viewport,
							mainPanel = viewport.down("#mainviewport");
						/*var mainMenu = mainPanel.down("meetinghistory");
						if(!mainMenu){
							mainMenu = mainPanel.add({xtype: "meetinghistory"});
						}
						mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});*/
						var mainMenu = mainPanel.down("mentorBottomTabView");
						if(!mainMenu){
							mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
						}
						mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});
						
						Mentor.app.application.getController('AppDetail').getMeetingHistory();
					}
					else if(MenteeLoginUser!=null){
					
						var viewport = Ext.Viewport,
							mainPanel = viewport.down("#mainviewport");
						/*var mainMenu = mainPanel.down("meetinghistorymentee");
						if(!mainMenu){
							mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
						}
						mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});*/
						var mainMenu = mainPanel.down("menteeBottomTabView");
						if(!mainMenu){
							mainMenu = mainPanel.add({xtype: "menteeBottomTabView"});
						}
						mainPanel.animateActiveItem(mainMenu, {type: "slide", duration: 450});
						Mentor.app.application.getController('AppDetail').getMeetingHistory();
					}
					
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	//get the configuration of the application
	getApplicationConfiguration : function(){
		var GLOBAL = Mentor.Global; 
		var data = {};	
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getConfig",
				body : data
			},
			success : function(responce) {
				//Ext.Viewport.setMasked(false); 
				var data = Ext.decode(responce.responseText);
				console.log(data);
				
				if(data.getConfig.Error == 1 || data.getConfig.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getConfig.Message);
					},100);
					return;
				}
				else{
					console.log("Sucess");
					
					appConfigData = data.getConfig.data[0];
					GLOBAL.MENTOR_NAME = appConfigData.mentorName;
					GLOBAL.MENTEE_NAME = appConfigData.menteeName;
					localStorage.setItem(GLOBAL.APPLICATION_CONFIGURATION, Ext.encode(appConfigData));
					
				}
				
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	SubmitDetail : function(){
		var GLOBAL = Mentor.Global;
		
		var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
		//var MeetingTimingDetail = Ext.decode(localStorage.getItem("storeMeetingTiming"));
		var MeetingDetailSubTopics = Ext.decode(localStorage.getItem("storeMeetingDetailSubTopics"));
		var EntrepreneurActionItems = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
		var MentorActionItems = Ext.decode(localStorage.getItem("storeMentorActionItems"));
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		
		//localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
		//localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));
		//localStorage.setItem("storeMeetingDetailSubTopics",Ext.encode(MeetingDetailSubTopics));
		//localStorage.setItem("storeEntrepreneurActionItems",Ext.encode(EntrepreneurActionItems));
		//localStorage.setItem("storeMentorActionItems",Ext.encode(MentorActionItems));
		
		var MeetingID = "0";
		var MentorActionItemDoneID = null;
		if(Ext.getCmp('btnSubmitMentorActionItems').getText() == "Update"){
			MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
			// Done Mentor Action Items
			var MentorActionPanelDoneOnMettingUpdatePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
			
			
			var isChecked = 0;
			for(i = 0 ; i <MentorActionPanelDoneOnMettingUpdatePanel.getItems().length;i++){
				if(MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).isChecked())
				{
					if(isChecked ==0){
						MentorActionItemDoneID = MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
					
						isChecked = isChecked + 1;
					}
					else{
						MentorActionItemDoneID = MentorActionItemDoneID + ","+MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
					
						isChecked = isChecked + 1;
					}
				}
			}
		}
		
		var data = {
			MeetingID : MeetingID,
			MentorID : MentorLoginUser.Id,
			MenteeID : MeetingDetail.MenteeID,
			MeetingStartDatetime : MeetingDetail.StartDate,
			MeetingEndDatetime : MeetingDetail.MeetingEndDatetime,
			MeetingTypeID : MeetingDetail.MeetngTypeID,
			MeetingTopicID : MeetingDetail.TopicID,
			MeetingSubTopicID : MeetingDetailSubTopics.SubTopicsID,
			MeetingPlaceID :  MeetingDetail.MeetingPlaceID,
			MenteeActionIDs : EntrepreneurActionItems.MenteeActionItemID,
			MentorActionIDs :MentorActionItems.MentorActionItemID,
			MeetingFeedback : MentorActionItems.Feedback,
			MeetingElapsedTime : MeetingDetail.MeetingElapsedTime,
			MentorActionItemDone : MentorActionItemDoneID
			
		}	
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "submitMeetingInfo",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.submitMeetingInfo.data;
				if(data.submitMeetingInfo.Error == 1 || data.submitMeetingInfo.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.submitMeetingInfo.Message);
					},100);
					return;
				}
				else{
					console.log("Sucess");
					localStorage.setItem("storeMeetingDetail",null);
					localStorage.setItem("storeMeetingTiming",null);
					localStorage.setItem("storeMeetingDetailSubTopics",null);
					localStorage.setItem("storeEntrepreneurActionItems",null);
					localStorage.setItem("storeMentorActionItems",null);
					
					Ext.Function.defer(function(){
							
						Ext.Msg.alert('MELS', "Meeting details submitted successfully.",function(){
							var viewport = Ext.Viewport,
							mainPanel = viewport.down("#mainviewport");
							var mainMenu = mainPanel.down("mentorBottomTabView");
							if(!mainMenu){
								mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
							}
							Mentor.Global.NavigationStack = [];
							mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
							Mentor.app.application.getController('AppDetail').getMeetingHistory();
						});
					},100);
					
				}
				
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	updateMeetingDetail : function(){
		var GLOBAL = Mentor.Global;
		
		var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
		//var MeetingTimingDetail = Ext.decode(localStorage.getItem("storeMeetingTiming"));
		var MeetingDetailSubTopics = Ext.decode(localStorage.getItem("storeMeetingDetailSubTopics"));
		var EntrepreneurActionItems = Ext.decode(localStorage.getItem("storeEntrepreneurActionItems"));
		var MentorActionItems = Ext.decode(localStorage.getItem("storeMentorActionItems"));
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		
		
		// Done Mentor Action Items
		var MentorActionPanelDoneOnMettingUpdatePanel = Ext.getCmp('idMentorActionItemDoneOnMeetingUpdate');
		var MentorActionItemDoneID;
		
		var isChecked = 0;
		if(MentorActionPanelDoneOnMettingUpdatePanel!=null){
			for(i = 0 ; i <MentorActionPanelDoneOnMettingUpdatePanel.getItems().length;i++){
				if(MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).isChecked())
				{
					if(isChecked ==0){
						MentorActionItemDoneID = MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
					
						isChecked = isChecked + 1;
					}
					else{
						MentorActionItemDoneID = MentorActionItemDoneID + ","+MentorActionPanelDoneOnMettingUpdatePanel.getItems().getAt(i).getValue();
					
						isChecked = isChecked + 1;
					}
				}
			}
		}
		
		
		MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
		
		console.log(MentorActionItemDoneID);
		var data = {
			MeetingID : MeetingID,
			MentorID : MentorLoginUser.Id,
			MenteeID : MeetingDetail.MenteeID,
			MeetingStartDatetime : MeetingDetail.StartDate,
			MeetingEndDatetime : MeetingDetail.MeetingEndDatetime,
			MeetingTypeID : MeetingDetail.MeetngTypeID,
			MeetingTopicID : MeetingDetail.TopicID,
			MeetingSubTopicID : MeetingDetailSubTopics.SubTopicsID,
			MeetingPlaceID :  MeetingDetail.MeetingPlaceID,
			MenteeActionIDs : EntrepreneurActionItems.MenteeActionItemID,
			MentorActionIDs :MentorActionItems.MentorActionItemID,
			MeetingFeedback : MentorActionItems.Feedback,
			MeetingElapsedTime : MeetingDetail.MeetingElapsedTime,
			MentorActionItemDone : MentorActionItemDoneID
			
		}	
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "submitMeetingInfo",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.submitMeetingInfo.data;
				if(data.submitMeetingInfo.Error == 1 || data.submitMeetingInfo.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.submitMeetingInfo.Message);
					},100);
					return;
				}
				else{
					console.log("Sucess");
					localStorage.setItem("storeMeetingDetail",null);
					localStorage.setItem("storeMeetingTiming",null);
					localStorage.setItem("storeMeetingDetailSubTopics",null);
					localStorage.setItem("storeEntrepreneurActionItems",null);
					localStorage.setItem("storeMentorActionItems",null);
					localStorage.setItem("storeStartEndSession",null);
					Mentor.Global.MEETING_DETAIL = null;
					
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', "Meeting details submitted successfully.",function(){
							var viewport = Ext.Viewport,
							mainPanel = viewport.down("#mainviewport");
							var mainMenu = mainPanel.down("mentorBottomTabView");
							if(!mainMenu){
								mainMenu = mainPanel.add({xtype: "mentorBottomTabView"});
							}
							Mentor.Global.NavigationStack = [];
							mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
							Mentor.app.application.getController('AppDetail').getMeetingHistory();
						});
					},100);
					
				}
				
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				console.log(responce);
			}
		});
	},
	
	updateMenteeActionItemsWithDone : function(){
		var EntrepreneurItemsPanel = Ext.getCmp('idEnterpreneurActionItem');
		var EntrepreneurItemsDonePanel = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
		var MenteeActionItemID;
		var MenteeActionItemDoneID;
		var MenteeActionItemName
		var isChecked = 0;
		var isCheckedDone = 0;
		for(i = 0 ; i <EntrepreneurItemsPanel.getItems().length;i++){
			if(EntrepreneurItemsPanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					MenteeActionItemID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
					MenteeActionItemName = EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
				else{
					MenteeActionItemID = MenteeActionItemID + ","+EntrepreneurItemsPanel.getItems().getAt(i).getValue();
					MenteeActionItemName = MenteeActionItemName + ","+EntrepreneurItemsPanel.getItems().getAt(i).getLabel();
					isChecked = isChecked + 1;
				}
			}
			
			
			if(EntrepreneurItemsDonePanel.getItems().getAt(i).isChecked())
			{
				if(isCheckedDone ==0){
					MenteeActionItemDoneID = EntrepreneurItemsPanel.getItems().getAt(i).getValue();
					isCheckedDone = isCheckedDone + 1;
				}
				else{
					MenteeActionItemDoneID = MenteeActionItemDoneID + ","+EntrepreneurItemsDonePanel.getItems().getAt(i).getValue();
					isCheckedDone = isCheckedDone + 1;
				}
			}
		}
		
		if(isChecked == 0){
			Ext.Msg.alert('MELS','Please select at least one action.');
		}
		else{
			var GLOBAL = Mentor.Global;
			var MeetingID = GLOBAL.MEETING_DETAIL.MeetingID;
			var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
			
			
			var data = {
				MenteeID : MenteeLoginUser.Id,
				MeetingID : MeetingID,
				ActionId : MenteeActionItemID,
				MenteeActionItemDone : MenteeActionItemDoneID
			}	
		
			Ext.Viewport.setMasked({
				xtype : "loadmask",
				message : "Please wait"
			});

			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "updateMenteeactionsItemsWithDone",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.updateMenteeactionsItemsWithDone.data;
					if(data.updateMenteeactionsItemsWithDone.Error == 1 || data.updateMenteeactionsItemsWithDone.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.updateMenteeactionsItemsWithDone.Message);
						},100);
						return;
					}
					else{
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', "Meeting details submitted successfully.",function(){
								var viewport = Ext.Viewport,
								mainPanel = viewport.down("#mainviewport");
								var mainMenu = mainPanel.down("meetinghistorymentee");
								if(!mainMenu){
									mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
								}
								Mentor.Global.NavigationStack = [];
								mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});
								Mentor.app.application.getController('AppDetail').getMeetingHistory();
							});
						},100);
						
					}
					
				
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					},100);
					console.log(responce);
				}
			});
		}
	},
	
	
	startEndMeetingSession : function(SesssionStatus,DateTime,MenteeID){
		var GLOBAL = Mentor.Global;
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MenteeID : MenteeID,
			MentorID : MentorLoginUser.Id,
			DateTime : DateTime,
			SessionStatus : SesssionStatus
			
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "startEndMeetingSession",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.startEndMeetingSession.data;
				
				if(data.startEndMeetingSession.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.startEndMeetingSession.Message);
					},100);
					return;
				}
				else if(data.startEndMeetingSession.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', "Not able to start session.");
					},100);
					
				}
				else{
					//Ext.Msg.alert('MELS', "StartSession Sucessfully");
					// Start Metting Session
					if(SesssionStatus == 1){
						var currentDate = me.getDate();
						var FullYear = currentDate.getFullYear();
						var Month = currentDate.getMonth() + 1;
						var Date = currentDate.getDate();
						var Hours = currentDate.getHours();
						if(Hours < 9)
							Hours = "0"+Hours;
						var Minutes = currentDate.getMinutes();
						if(Minutes < 9)
							Minutes = "0"+Minutes;
						var StartSessionDateTime = FullYear + "-" + Month + "-" + Date + " " + Hours  + ":" + Minutes;
						Ext.getCmp('startSession').setText(StartSessionDateTime);
						Ext.getCmp('startSession').setDisabled(true);
						Ext.getCmp('endSession').setDisabled(false);
						
						var ElapsedTime = Ext.getCmp('MeetingElapsedTime');
						ElapsedTime.setValue("0 Minutes");
						
						Mentor.Global.timer = setInterval(function(){
							Mentor.app.application.getController('Main').calculateElapsedTime();
							console.log("a")
						},10000);
						
						/*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
						var MeenteeID  = MeetingDetail.MenteeID;*/
					}
					else{ // End Meeting
						var currentDate = me.getDate();
						var FullYear = currentDate.getFullYear();
						var Month = currentDate.getMonth() + 1;
						var Date = currentDate.getDate();
						var Hours = currentDate.getHours();
						if(Hours < 9)
							Hours = "0"+Hours;
						var Minutes = currentDate.getMinutes();
						if(Minutes < 9)
							Minutes = "0"+Minutes;
						var StartSessionDateTime = FullYear + "-" + Month + "-" + Date + "-" + Hours  + ":" + Minutes;
						Ext.getCmp('endSession').setText(StartSessionDateTime);
						Ext.getCmp('endSession').setDisabled(true);
						
						Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
						
						if(Mentor.Global.timer !="")
							clearInterval(Mentor.Global.timer);
							
						/*var MeetingDetail = Ext.decode(localStorage.getItem("storeMeetingDetail"));
						var MeenteeID  = MeetingDetail.MenteeID;*/
					}
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	
	
	ValidateEmail:function (mail) 
	{
		/*if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
		{
			return (true)
		}
		
		return (false)*/
		return (true);
	},
	
	// Introduction Screen , On Skip Button move to Login Screen
	btnSkipIntroductionScreen : function(){
		var viewport = Ext.Viewport,
		mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("login");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "login"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide",direction: 'left'});
	}


	
});
