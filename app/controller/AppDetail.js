Ext.define('Mentor.controller.AppDetail', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            
        },
        control: {
            "mentorreviewform button[action=btnBackMentorReviewForm]": {
            	tap: "btnBackMentorReviewForm"
            },
			"meetinghistory button[action=btnAddMeetingDetail]": {
            	tap: "btnAddMeetingDetail"
            },
			"meetinghistory button[action=btnProfileMeetingDetail]": {
            	tap: "btnProfileMeetingDetail"
            },
			"mentorPendingRequest button[action=btnGetPendingRequest]": {
            	tap: "btnGetPendingRequest"
            },
			"meetinghistory button[action=btnRefreshMeetingHistory]": {
            	tap: "btnRefreshMeetingHistory"
            },
			
			
			"meetinghistorymentee button[action=btnProfileMeetingDetail]": {
            	tap: "btnProfileMeetingDetail"
            },
			"meetinghistorymentee button[action=btnInviteForMeeting]": {
            	tap: "btnInviteForMeeting"
            },
			"meetinghistorymentee button[action=btnRefreshMeetingHistory]": {
            	tap: "btnRefreshMeetingHistory"
            },
			
			"mentorreviewform button[action=doSubmitMentorReviewForm]": {
            	tap: "doSubmitMentorReviewForm"
            },
			"menteereviewform button[action=doSubmitMenteeReviewForm]": {
            	tap: "doSubmitMenteeReviewForm"
            },
			"menteereviewform button[action=btnBackMenteeReviewForm]": {
            	tap: "btnBackMenteeReviewForm"
            },
			"mentorprofile button[action=btnBackMentorProfile]": {
            	tap: "btnBackMentorProfile"
            },
			"mentorprofile button[action=btnAddSkills]":{
				tap : "btnAddSkills"
			},
			
			"invitementor button[action=btnBackInviteBack]":{
				tap : "btnBackInviteBack"
			},
			"mentorPendingRequest button[action=btnBackMentorPendingRequest]":{
				tap : "btnBackMentorPendingRequest"
			},
			"acceptRejectPendingRequest button[action=doAcceptRequest]":{
				tap : "doAcceptRequest"
			},
			"acceptRejectPendingRequest button[action=doDeclineRequest]":{
				tap : "doDeclineRequest"
			},
			"acceptRejectPendingRequest button[action=btnBackAcceptRejectPendingRequest]":{
				tap : "btnBackAcceptRejectPendingRequest"
			},
			
			"waitscreen button[action=btnBackWaitScreen]":{
				tap : "btnBackWaitScreen"
			},
			"invitementor button[action=btnSendInviteMentorScreen]":{
				tap : "btnInviteMentor"
			},
			"invitementor button[action=btnGotoWaitScreenInviteMentorScreen]":{
				tap : "btnGotoWaitScreenInviteMentorScreen"
			},
			"waitscreen button[action=btnRefreshWaitScreen]":{
				tap : "getMettingWaitScreenInfo"
			},
			"waitscreen button[action=btnHistoryScreenWaitScreen]":{
				tap : "btnHistoryScreenWaitScreen"
			},
			"meetinghistorymentee button[action=btnInviteStatusMeetingHistory]":{
				tap : "btnGotoWaitScreenInviteMentorScreen"
			},
			
			'meetinghistory searchfield[itemId=searchBoxMentorMettingHistory]' : {
				clearicontap : 'onClearSearchMentorMettingHistory',
				keyup: 'onSearchKeyUpMentorMettingHistory'
			},
			'meetinghistorymentee searchfield[itemId=searchBoxMenteeMettingHistory]' : {
				clearicontap : 'onClearSearchMenteeMettingHistory',
				keyup: 'onSearchKeyUpMenteeMettingHistory'
			}
			
			
			
        }
    },
	
	//called when the Application is launched, remove if not needed
    launch: function(app) {
       
    },
	
	getMeetingHistory:function(){
		me = this;
		var GLOBAL = Mentor.Global;
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
			
		if(MentorLoginUser!=null){
			UserID = MentorLoginUser.Id;
			UserType= "0";
			Ext.getCmp('idUserNameMentorMeetingHistory').setHtml(MentorLoginUser.MentorName);
			Ext.getCmp('idVersionMeetingHistory').setHtml(GLOBAL.VERSION_NAME);
		}
		else if(MenteeLoginUser!=null){
			UserID = MenteeLoginUser.Id;
			UserType= "1";
			Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);
			Ext.getCmp('idVersionMenteeMeetingHistory').setHtml(GLOBAL.VERSION_NAME);
		}
		

		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			UserId : UserID,
			UserType : UserType,
			CurrentPage : "1",
			PageSize : "10"
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMeetingHistory",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMeetingHistory.data;
				Ext.getStore('MeetingHistory').clearData();
				if(data.getMeetingHistory.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMeetingHistory.Message);
					},100);
					return;
				}
				else if(data.getMeetingHistory.Error == 2){
					if(UserType== "1" )
					{
						Ext.Msg.alert('MELS', "Welcome to your new MELS account. Click Invites tab to issue meeting invite.",function(){
							Ext.getCmp('idMenteeBottomTabView').setActiveItem(1);
						});
						me.btnInviteForMeeting();
						
					}
					else{
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', "Welcome to your new MELS account. Click OK to start a new meeting.");
						},100);
					}
						
				}
				else{
					Ext.getStore('MeetingHistory').clearData();
					Ext.getStore('MeetingHistory').add(data.getMeetingHistory.data);
					
					if(UserType== "0" ){
						if(data.getMeetingHistory.pending == "0")
							Ext.getCmp("idBtnPendingInvitesMeetingHistoryMentorScreen").setText("No Pending Invites");
						else
							Ext.getCmp("idBtnPendingInvitesMeetingHistoryMentorScreen").setText(data.getMeetingHistory.pending+" Pending Invites");
					}
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	displayDetail:function(view, index, item, e){
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
			
		if(MentorLoginUser!=null){
			UserID = MentorLoginUser.Id;
			UserType= "0"
		}
		else if(MenteeLoginUser!=null){
			UserID = MenteeLoginUser.Id;
			UserType= "1"
		}
		
		if(UserType == "0"){ // mentor
			var viewport = Ext.Viewport,
				mainPanel = viewport.down("#mainviewport");
			var mainMenu = mainPanel.down("mentorreviewform");
			if(!mainMenu){
				mainMenu = mainPanel.add({xtype: "mentorreviewform"});
			}
			//mainPanel.remove({xtype: "mentorreviewform"});
			//mainMenu = mainPanel.add({xtype: "mentorreviewform"});
			
			Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
			mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

			record = view.getStore().getAt(index).data;
			Mentor.Global.MEETING_DETAIL = record;
			
			Ext.getCmp('meetingWithWhomMentorReviewForm').setValue(record.MenteeName);
			Ext.getCmp('meetingTypeMentorReviewForm').setValue(record.MeetingTypeName);
			Ext.getCmp('meetingPlaceMentorReviewForm').setValue(record.MeetingPlaceName);
			Ext.getCmp('meetingSubjectMentorReviewForm').setValue(record.TopicDescription);
			//Ext.getCmp('meetingmeetingDateMentorReviewForm').setValue("");
			Ext.getCmp('meetingStartTimeMentorReviewForm').setValue(record.MeetingStartDatetime);
			Ext.getCmp('meetingEndTimeReviewForm').setValue(record.MeetingEndDatetime);
			//Ext.getCmp('meetingPlaceMentorReviewForm').setValue("");
			//Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue(record.SubTopicDescription);
			Ext.getCmp('meetingElapsedTimeMentorReviewForm').setValue(record.MeetingElapsedTime);
			
			var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
			Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
			Ext.getCmp('idMentorReviewFormTitle').setTitle(record.MeetingStartDatetime.split(" ")[0]+" Meeting Edit Form");
		
			/*if(record.SatisfactionIndex == "")
				Ext.getCmp('idSatisfactionWithMeeting').setValue(null);
			else
				Ext.getCmp('idSatisfactionWithMeeting').setValue(record.SatisfactionIndex);*/
			var SatificationValuePanel = Ext.getCmp('idSatisfactionWithMeeting');
			/*var SatificationValue;
			var isChecked = 0;
			for(i = 0 ; i <SatificationValuePanel.getItems().length;i++){
				if(record.SatisfactionIndex  == SatificationValuePanel.getItems().getAt(i).getValue()){
					SatificationValuePanel.getItems().getAt(i).setChecked(true);
				}
			}*/
			SatificationValuePanel.setValue(record.SatisfactionIndex);
				
			Ext.getCmp('mentorCommentMentorReviewForm').setValue(record.MentorComment);
			
			//Set Value for MentorActionItems
			if(record.MentorActionDetail!=""){
				var MentorActionItems = record.MentorActionDetail.split(',');
				var MentorActionName;
				for(i = 0;i<MentorActionItems.length;i++){
					var MentorAction = MentorActionItems[i].split('~');
					if( i == 0)
						MentorActionName = MentorAction[1];
					else
						MentorActionName = MentorActionName + ",\n"+MentorAction[1];
				}
				Ext.getCmp('actionItemsByMentorMentorReviewForm').setValue(MentorActionName);
				Ext.getCmp('actionItemsByMentorMentorReviewForm').setMaxRows(MentorActionItems.length);
			}
			

			//Set Value for MenteeActionItems
			if(record.MenteeActionDetail!=""){
				var MenteeActionItems = record.MenteeActionDetail.split(',');
				var MenteeActionName;
				for(i = 0;i<MenteeActionItems.length;i++){
					var MenteeAction = MenteeActionItems[i].split('~')
					if( i == 0)
						MenteeActionName = MenteeAction[1];
					else
						MenteeActionName = MenteeActionName + ",\n"+MenteeAction[1];
				}
				Ext.getCmp('actionItemsByMenteeMentorReviewForm').setValue(MenteeActionName);
				Ext.getCmp('actionItemsByMenteeMentorReviewForm').setMaxRows(MenteeActionItems.length);
			}
			
			//Set Value for Meeting Detail (Sub Topics)
			if(record.SubTopicDescription!=""){
				var SubTopics = record.SubTopicDescription.split(',');
				var SubTopicsName;
				for(i = 0;i<SubTopics.length;i++){
					var subTopicItems = SubTopics[i].split('~');
					if( i == 0)
						SubTopicsName = subTopicItems[1];
					else
						SubTopicsName = SubTopicsName + ",\n"+subTopicItems[1];
				}
				Ext.getCmp('mainIssueDiscussedMentorReviewForm').setValue(SubTopicsName);
				Ext.getCmp('mainIssueDiscussedMentorReviewForm').setMaxRows(SubTopics.length);
			}

		}
		else if(UserType == "1"){ //mentee
			var viewport = Ext.Viewport,
				mainPanel = viewport.down("#mainviewport");
			var mainMenu = mainPanel.down("menteereviewform");
			if(!mainMenu){
				mainMenu = mainPanel.add({xtype: "menteereviewform"});
			}
			Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
			mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

			record = view.getStore().getAt(index).data;
			Mentor.Global.MEETING_DETAIL = record;
			
			Ext.getCmp('meetingWithWhomMenteeReviewForm').setValue(record.MentorName);
			Ext.getCmp('meetingTypeMenteeReviewForm').setValue(record.MeetingTypeName);
			Ext.getCmp('meetingPlaceMenteeReviewForm').setValue(record.MeetingPlaceName);
			Ext.getCmp('meetingSubjectMenteeReviewForm').setValue(record.TopicDescription);
			//Ext.getCmp('meetingmeetingDateMentorReviewForm').setValue("");
			Ext.getCmp('meetingStartTimeMenteeReviewForm').setValue(record.MeetingStartDatetime);
			Ext.getCmp('meetingEndTimeMenteeReviewForm').setValue(record.MeetingEndDatetime);
			//Ext.getCmp('meetingPlaceMentorReviewForm').setValue("");
			//Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setValue(record.SubTopicDescription);
			Ext.getCmp('meetingElapsedTimeMenteeReviewForm').setValue(record.MeetingElapsedTime);
			Ext.getCmp('idMenteeReviewFormTitle').setTitle(record.MeetingStartDatetime.split(" ")[0]+" Meeting Edit Form");
			
			//meetingElapsedTimeMenteeReviewForm
			var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
			Ext.getCmp('idUserNameEnterpreneur').setHtml(MenteeLoginDetail.MenteeName);
			
			/*if(record.CareC == "")
				Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').setValue(null);
			else
				Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').setValue(record.CareC);
			
			if(record.CareA == "")
				Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').setValue(null);
			else
				Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').setValue(record.CareA);
			
			if(record.CareR == "")
				Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').setValue(null);
			else
				Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').setValue(record.CareR);
			
			if(record.CareE == "")
				Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').setValue(null);
			else
				Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').setValue(record.CareE);*/
				
			//Care C
			var SatisfactionClearAndUnderstandablePanel = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
			/*var SatisfactionClearAndUnderstandableValue;
			var isChecked = 0;
			for(i = 0 ; i <SatisfactionClearAndUnderstandablePanel.getItems().length;i++){
				if(record.CareC  == SatisfactionClearAndUnderstandablePanel.getItems().getAt(i).getValue()){
					SatisfactionClearAndUnderstandablePanel.getItems().getAt(i).setChecked(true);
				}
			}*/	
			SatisfactionClearAndUnderstandablePanel.setValue(record.CareC);
			
			//Care A
			var SatisfactionOverallSubjectPanel = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
			/*var SatisfactionOverallSubjectValue;
			var isChecked = 0;
			for(i = 0 ; i <SatisfactionOverallSubjectPanel.getItems().length;i++){
				if(record.CareA  == SatisfactionOverallSubjectPanel.getItems().getAt(i).getValue()){
					SatisfactionOverallSubjectPanel.getItems().getAt(i).setChecked(true);
				}
			}	*/
			SatisfactionOverallSubjectPanel.setValue(record.CareA);
			
			//Care R
			var SatisfactionReleveantProblemPanel = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
			/*var SatisfactionReleveantProblemValue;
			var isChecked = 0;
			for(i = 0 ; i <SatisfactionReleveantProblemPanel.getItems().length;i++){
				if(record.CareR  == SatisfactionReleveantProblemPanel.getItems().getAt(i).getValue()){
					SatisfactionReleveantProblemPanel.getItems().getAt(i).setChecked(true);
				}
			}	*/
			SatisfactionReleveantProblemPanel.setValue(record.CareR);
			
			//Care E
			var SatisfactionAdviceAndFeedbackPanel = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
			/*var SatisfactionAdviceAndFeedbackValue;
			var isChecked = 0;
			for(i = 0 ; i <SatisfactionAdviceAndFeedbackPanel.getItems().length;i++){
				if(record.CareE  == SatisfactionAdviceAndFeedbackPanel.getItems().getAt(i).getValue()){
					SatisfactionAdviceAndFeedbackPanel.getItems().getAt(i).setChecked(true);
				}
			}	*/
			SatisfactionAdviceAndFeedbackPanel.setValue(record.CareE);
		
			Ext.getCmp('menteeCommentMenteeReviewForm').setValue(record.MenteeComment);
			
			if(record.MentorActionDetail!=""){
				var MentorActionItems = record.MentorActionDetail.split(',');
				var MentorActionName;
				for(i = 0;i<MentorActionItems.length;i++){
					var MentorAction = MentorActionItems[i].split('~');
					if( i == 0)
						MentorActionName = MentorAction[1];
					else
						MentorActionName = MentorActionName + ",\n"+MentorAction[1];
				}
				Ext.getCmp('actionItemsByMentorMenteeReviewForm').setValue(MentorActionName);
				Ext.getCmp('actionItemsByMentorMenteeReviewForm').setMaxRows(MentorActionItems.length);
			}

			if(record.MenteeActionDetail!=""){
				var MenteeActionItems = record.MenteeActionDetail.split(',');
				var MenteeActionName;
				for(i = 0;i<MenteeActionItems.length;i++){
					var MenteeAction = MenteeActionItems[i].split('~')
					if( i == 0)
						MenteeActionName = MenteeAction[1];
					else
						MenteeActionName = MenteeActionName + ",\n"+MenteeAction[1];
				}
				Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setValue(MenteeActionName);
				Ext.getCmp('actionItemsByMenteeMenteeReviewForm').setMaxRows(MenteeActionItems.length);
				
			}
			
			//Set Value for Meeting Detail (Sub Topics)
			if(record.SubTopicDescription!=""){
				var SubTopics = record.SubTopicDescription.split(',');
				var SubTopicsName;
				for(i = 0;i<SubTopics.length;i++){
					var subTopicItems = SubTopics[i].split('~');
					if( i == 0)
						SubTopicsName = subTopicItems[1];
					else
						SubTopicsName = SubTopicsName + ",\n"+subTopicItems[1];
				}
				Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setValue(SubTopicsName);
				Ext.getCmp('mainIssueDiscussedMenteeReviewForm').setMaxRows(SubTopics.length);
			}
		}
		
	},
	doSubmitMentorReviewForm:function(btn){
		console.log(Mentor.Global.MEETING_DETAIL);
		
		me = this;
		var GLOBAL = Mentor.Global;
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
		
		/*var SatificationValuePanel = Ext.getCmp('idSatisfactionWithMeeting');
		var SatificationValue;
		var isChecked = 0;
		var isCheckedStatus = false;
		for(i = 0 ; i <SatificationValuePanel.getItems().length;i++){
			if(SatificationValuePanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					SatificationValue = SatificationValuePanel.getItems().getAt(i).getValue();
					isCheckedStatus = true;
				}
				else{
					
				}
			}
		}
		if(isCheckedStatus == false)
		{
			Ext.Msg.alert('MELS', "Please select satisfaction rating.");
			return;
		}*/
		var SatificationRating = Ext.getCmp('idSatisfactionWithMeeting');
		if(SatificationRating.getValue() == "" || SatificationRating.getValue() == "0")
		{
			Ext.Msg.alert('MELS', "Please select satisfaction rating.");
			return;
		}
		else
			SatificationValue = SatificationRating.getValue();
		
		var MentorComment = Ext.ComponentQuery.query('#mentorCommentMentorReviewForm')[0].getValue();
			
		if(MentorLoginUser!=null){
			UserID = MentorLoginUser.Id;
			UserType= "0"
		}
		else if(MenteeLoginUser!=null){
			UserID = MenteeLoginUser.Id;
			UserType= "1"
		}
		
		Ext.Viewport.setMasked({
				xtype : "loadmask",
				message : "Please wait"
			});
			
			var data = {
				MentorID : GLOBAL.MEETING_DETAIL.MentorID,
				MenteeID : GLOBAL.MEETING_DETAIL.MenteeID,
				MeetingID : GLOBAL.MEETING_DETAIL.MeetingID,
				MentorComment : MentorComment,
				SatisfactionIndex : SatificationValue
			}			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "mentorComment",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.mentorComment.data;
					
					if(data.mentorComment.Error == 1 || data.mentorComment.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.mentorComment.Message);
						},100);
						return;
					}
					else{
						
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.mentorComment.Message,function(){
									/*var viewport = Ext.Viewport,
										mainPanel = viewport.down("#entorBottomTabView");
									var mainMenu = mainPanel.down("meetinghistory");
									if(!mainMenu){
										mainMenu = mainPanel.add({xtype: "meetinghistory"});
									}
									mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
									var View = Mentor.Global.NavigationStack.pop();
									var viewport = Ext.Viewport,
										mainPanel = viewport.down("#mainviewport");
									
									mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
		
									Mentor.Global.MEETING_DETAIL = null;
									
									me.getMeetingHistory();
							});
						},100);
						
						
						
					}
				
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					},100);
					
				}
			});
		
	},
	doSubmitMenteeReviewForm : function(btn){
	
		var GLOBAL = Mentor.Global;
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
		
		/*var SatificationCareCValue = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm').getValue();
		var SatificationCareAValue = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm').getValue();
		var SatificationCareRValue = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm').getValue();
		var SatificationCareEValue = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm').getValue();*/
		
		//Care C
		/*var SatificationCareCValuePanel = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
		var SatificationCareCValue;
		var isChecked = 0;
		var isCheckedStatus = false;
		for(i = 0 ; i <SatificationCareCValuePanel.getItems().length;i++){
			if(SatificationCareCValuePanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					SatificationCareCValue = SatificationCareCValuePanel.getItems().getAt(i).getValue();
					isCheckedStatus = true;
				}
			}
		}
		if(isCheckedStatus == false)
		{
			Ext.Msg.alert('MELS', "Please select clear and understandable feedback and advice rating.");
			return;
		}*/
		
		var SatificationCareCValueRatting = Ext.getCmp('idSatisfactionClearAndUnderstandableMenteeReviewForm');
		var SatificationCareCValue;
		if(SatificationCareCValueRatting.getValue() == "" || SatificationCareCValueRatting.getValue() == "0")
		{
			Ext.Msg.alert('MELS', "Please select clear and understandable feedback and advice rating.");
			return;
		}
		else
			SatificationCareCValue = SatificationCareCValueRatting.getValue();
		
		
		
		//Care A
		/*var SatificationCareAValuePanel = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
		var SatificationCareAValue;
		var isChecked = 0;
		isCheckedStatus = false;
		for(i = 0 ; i <SatificationCareAValuePanel.getItems().length;i++){
			if(SatificationCareAValuePanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					SatificationCareAValue = SatificationCareAValuePanel.getItems().getAt(i).getValue();
					isCheckedStatus = true;
				}
			}
		}
		if(isCheckedStatus == false)
		{
			Ext.Msg.alert('MELS', "Please select applicable to overall subject area rating.");
			return;
		}*/
		var SatificationCareAValueRatting = Ext.getCmp('idSatisfactionOverallSubjectMenteeReviewForm');
		var SatificationCareAValue;
		if(SatificationCareAValueRatting.getValue() == "" || SatificationCareAValueRatting.getValue() == "0")
		{
			Ext.Msg.alert('MELS', "Please select applicable to overall subject area rating.");
			return;
		}
		else
			SatificationCareAValue = SatificationCareAValueRatting.getValue();
		
		
		//Care R
		/*var SatificationCareRValuePanel = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
		var SatificationCareRValue;
		var isChecked = 0;
		isCheckedStatus = false;
		for(i = 0 ; i <SatificationCareRValuePanel.getItems().length;i++){
			if(SatificationCareRValuePanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					SatificationCareRValue = SatificationCareRValuePanel.getItems().getAt(i).getValue();
					isCheckedStatus = true;
				}
			}
		}
		if(isCheckedStatus == false)
		{
			Ext.Msg.alert('MELS', "Please select relevant to my particular problem rating.");
			return;
		}*/
		var SatificationCareRValueRatting = Ext.getCmp('idSatisfactionReleveantProblemMenteeReviewForm');
		var SatificationCareRValue;
		if(SatificationCareRValueRatting.getValue() == "" || SatificationCareRValueRatting.getValue() == "0")
		{
			Ext.Msg.alert('MELS', "Please select relevant to my particular problem rating.");
			return;
		}
		else
			SatificationCareRValue = SatificationCareRValueRatting.getValue();
		
		
		//Care E
		/*var SatificationCareEValuePanel = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
		var SatificationCareEValue;
		var isChecked = 0;
		isCheckedStatus = false;
		for(i = 0 ; i <SatificationCareEValuePanel.getItems().length;i++){
			if(SatificationCareEValuePanel.getItems().getAt(i).isChecked())
			{
				if(isChecked ==0){
					SatificationCareEValue = SatificationCareEValuePanel.getItems().getAt(i).getValue();
					isCheckedStatus = true;
				}
			}
		}
		if(isCheckedStatus == false)
		{
			Ext.Msg.alert('MELS', "Please select Executable/Actionable practical advice and feedback rating.");
			return;
		}*/
		var SatificationCareEValueRatting = Ext.getCmp('idSatisfactionAdviceAndFeedbackMenteeReviewForm');
		var SatificationCareEValue;
		if(SatificationCareEValueRatting.getValue() == "" || SatificationCareEValueRatting.getValue() == "0")
		{
			Ext.Msg.alert('MELS', "Please select Executable/Actionable practical advice and feedback rating.");
			return;
		}
		else
			SatificationCareEValue = SatificationCareEValueRatting.getValue();
		
		
		var MenteeComment = Ext.ComponentQuery.query('#menteeCommentMenteeReviewForm')[0].getValue();
		
		Ext.Viewport.setMasked({
				xtype : "loadmask",
				message : "Please wait"
			});
			
			var data = {
				MentorID : GLOBAL.MEETING_DETAIL.MentorID,
				MenteeID : GLOBAL.MEETING_DETAIL.MenteeID,
				MeetingID : GLOBAL.MEETING_DETAIL.MeetingID,
				MenteeComment : MenteeComment,
				CareC : SatificationCareCValue,
				CareA : SatificationCareAValue,
				CareR : SatificationCareRValue,
				CareE : SatificationCareEValue
			}			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "menteeComment",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.menteeComment.data;
					
					if(data.menteeComment.Error == 1 || data.menteeComment.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.menteeComment.Message);
						},100);
						return;
					}
					else{
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.menteeComment.Message,function(){
									/*var viewport = Ext.Viewport,
										mainPanel = viewport.down("#mainviewport");
									var mainMenu = mainPanel.down("meetinghistorymentee");
									if(!mainMenu){
										mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
									}
									mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
									var View = Mentor.Global.NavigationStack.pop();
									var viewport = Ext.Viewport,
										mainPanel = viewport.down("#mainviewport");
									
									mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
									Mentor.Global.MEETING_DETAIL = null;
									
									me.getMeetingHistory();
							});
						},100);
						
						
						
					}
				
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					},100);
					
				}
			});
	},
	
	btnBackMentorReviewForm : function(btn){
		
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
		
		
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("meetinghistory");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "meetinghistory"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		
		Mentor.Global.MEETING_DETAIL = null;
	},
	btnBackMenteeReviewForm: function(btn){
		
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
		
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("meetinghistorymentee");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "meetinghistorymentee"});
		}
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'right'});*/
		
		Mentor.Global.MEETING_DETAIL = null;
	},
	
	btnAddMeetingDetail : function(btn){
		me = this;
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("entrepreneur");
		
		//mainMenu = mainPanel.remove({xtype: "entrepreneur"});
		//mainMenu = mainPanel.add({xtype: "entrepreneur"});
		
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "entrepreneur"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
		Mentor.Global.MEETING_DETAIL = null;
		me.clearLocalStoragePreviousMeetingDetail();
		
		MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		Mentor.app.application.getController('Main').getMentee(MentorLoginDetail.Id,false);
		
		localStorage.setItem("storeStartEndSession",null); //Clear Previous Meeting Start and End Session
	},
	
	changeMenteeDetail : function(){
		me = this;
		me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("entrepreneur");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "entrepreneur"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	changeTimingDetail : function(){
		me = this;
		me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("timing");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "timing"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	changeMeetingDetail : function(){
		me = this;
		me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
		
		//Clear the store and Dispaly according to the Metting MainTopics
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		var SubTopicStore = Ext.getStore('SubTopic');
		SubTopicStore.clearFilter();
		SubTopicStore.filterBy(function(rec) {
			return rec.get('TopicID') ===  MeetingDetail.MeetingTopicID;
		});
		SubTopicStore.load();
					
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("meetingdetails");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "meetingdetails"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	changeMenteeActionItemsDetail : function(){
		me = this;
		me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
	
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("enterpreneur_action_items");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "enterpreneur_action_items"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	changeMentorActionItemsDetail : function(){
		me = this;
		me.storeMeetingDetailForUpdate(); // Store the Meeting Detail for Update;
		
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("mentor_action_items");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "mentor_action_items"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	clearLocalStoragePreviousMeetingDetail : function(){
		localStorage.setItem("storeMeetingDetail",null);
		localStorage.setItem("storeMeetingTiming",null);
		localStorage.setItem("storeMeetingDetailSubTopics",null);
		localStorage.setItem("storeEntrepreneurActionItems",null);
		localStorage.setItem("storeMentorActionItems",null);
	},
	
	btnProfileMeetingDetail : function(btn){
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("mentorprofile");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "mentorprofile"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
	},
	
	btnBackMentorProfile : function(btn){
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	storeMeetingDetailForUpdate : function(){
	
		var MEETING_DETAIL = Mentor.Global.MEETING_DETAIL;
		localStorage.setItem("storeMeetingDetail",null);
		//localStorage.setItem("storeMeetingTiming",null);
		localStorage.setItem("storeMeetingDetailSubTopics",null);
		localStorage.setItem("storeEntrepreneurActionItems",null);
		localStorage.setItem("storeMentorActionItems",null);
	
		//EnterpreneurScreen (Select Mentee and Meeting Type)
		var MeenteeDetail = {"MeetngTypeID" : MEETING_DETAIL.MeetingTypeID,
					"MeetingTypeTitle" : MEETING_DETAIL.MeetingTypeName,
					"MenteeName" : MEETING_DETAIL.MenteeName,
					"MenteeID" : MEETING_DETAIL.MenteeID,
					"StartDate" : MEETING_DETAIL.MeetingStartDatetime,
					"MeetingEndDatetime" : MEETING_DETAIL.MeetingEndDatetime,
					"TopicName" : MEETING_DETAIL.TopicDescription,
					"TopicID" : MEETING_DETAIL.MeetingTopicID,
					"MeetingElapsedTime" : MEETING_DETAIL.MeetingElapsedTime,
					"MeetingPlaceID" : MEETING_DETAIL.MeetingPlaceID,
					"MeetingPlaceTitle" :  MEETING_DETAIL.MeetingPlaceName}
		localStorage.setItem("storeMeetingDetail",Ext.encode(MeenteeDetail));
		
		
		/*** 22/02/2016 Move Timing Screen to Enterpreneur Screen***/
		//Timing Screen (Topics)
		/*var TopicsTimingDetail = {"StartDate" : MEETING_DETAIL.MeetingStartDatetime,
					"MeetingEndDatetime" : MEETING_DETAIL.MeetingEndDatetime,
					"TopicName" : MEETING_DETAIL.TopicDescription,
					"TopicID" : MEETING_DETAIL.MeetingTopicID,
					"MeetingElapsedTime" : MEETING_DETAIL.MeetingElapsedTime}
				
		localStorage.setItem("storeMeetingTiming",Ext.encode(TopicsTimingDetail));*/
		/** End 22/02/2016 Move Timing Screen to Enterpreneur Screen***/
		
		
		// Store Metting Detail Screen (Sub Topics)
		var MeetingDetailSubTopics = {"SubTopicsID" : MEETING_DETAIL.MeetingSubTopicID,
						"SubTopicsTitle" : MEETING_DETAIL.SubTopicDescription}
					
		localStorage.setItem("storeMeetingDetailSubTopics",Ext.encode(MeetingDetailSubTopics));
		 
		 
		 
		 // Store Mentee Action item (Mentee Action Item)
		var MenteeActionItems = MEETING_DETAIL.MenteeActionDetail.split(',');
		var MenteeActionName;
		var MenteeActionID;
		for(j = 0;j<MenteeActionItems.length;j++){
			var MenteeAction = MenteeActionItems[j].split('~');
			if(j == 0){
				MenteeActionName = MenteeAction[1];
				MenteeActionID = MenteeAction[0];
			}
			else{
				MenteeActionName =  MenteeActionName + ","+MenteeAction[1];
				MenteeActionID = MenteeActionID + ","+MenteeAction[0];
			}
			
		}
			var EntrepreneurActionItems = {"MenteeActionItemID" : MenteeActionID,
							"MenteeActionItemName" : MenteeActionName}
						
			localStorage.setItem("storeEntrepreneurActionItems",Ext.encode(EntrepreneurActionItems));
			
			
			
			
		
		// Store Menor Action item (Mentee Action Item)
		var MentorActionItems = MEETING_DETAIL.MentorActionDetail.split(',');
		var MentorActionName;
		var MentorActionID;
		for(j = 0;j<MentorActionItems.length;j++){
			var MentorAction = MentorActionItems[j].split('~');
			if(j == 0){
				MentorActionName = MentorAction[1];
				MentorActionID = MentorAction[0];
			}
			else{
				MentorActionName =  MentorActionName + ","+MentorAction[1];
				MentorActionID = MentorActionID + ","+MentorAction[0];
			}
			
		}
			var MentorActionItems = {"MentorActionItemID" : MentorActionID,
						"MentorActionItemName" : MentorActionName,
						"Feedback" : MEETING_DETAIL.MeetingFeedback,
						"MeetingEndDatetime" : MEETING_DETAIL.MeetingEndDatetime};
					
			localStorage.setItem("storeMentorActionItems",Ext.encode(MentorActionItems));
	},
	
	btnAddSkills : function(btn){
		var GLOBAL = Mentor.Global;
		var SkillsPanel = Ext.getCmp('idSkillsProfile');
		var SkillID;
		var SkillName
		var isChecked = 0;
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
			
		var Email = Ext.getCmp('email').getValue();
		var Phone = Ext.getCmp('phone').getValue();
		var UserName = Ext.getCmp('userName').getValue();
		var AccessToken = Ext.getCmp('password').getValue();
		
		if(Email == ""){
			Ext.Msg.alert('MELS', "Please enter email address");
			return;
		}
		else if(Phone == ""){
			Ext.Msg.alert('MELS', "Please enter phone number");
			return;
		}
		else if(UserName == ""){
			Ext.Msg.alert('MELS', "Please enter username");
			return;
		}
		else if(AccessToken == ""){
			Ext.Msg.alert('MELS', "Please enter password");
			return;
		}
		else if(me.ValidateEmail(Email)==false){
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");
			return;
		}
		else if(AccessToken.length < 4){
			Ext.Msg.alert('MELS',"Password length must be four characters.");
			return;
		}
		
		if(MentorLoginUser!=null){
			UserID = MentorLoginUser.Id;
			UserType= "0"
		}
		else if(MenteeLoginUser!=null){
			UserID = MenteeLoginUser.Id;
			UserType= "1"
		}
		
		
		for(i = 0 ; i <SkillsPanel.getItems().length;i++){
			if(SkillsPanel.getItems().getAt(i).getCls() == 'profileSkillsActiveCLS')
			{
				if(isChecked ==0){
					SkillID = SkillsPanel.getItems().getAt(i).getId();
					SkillName = SkillsPanel.getItems().getAt(i).getText();
					isChecked = isChecked + 1;
				}
				else{
					SkillID = SkillID + ","+SkillsPanel.getItems().getAt(i).getId();
					MenteeActionItemName = SkillName + ","+SkillsPanel.getItems().getAt(i).getText();
					isChecked = isChecked + 1;
				}
			}
		}
		
		if(isChecked<=0){
			Ext.Msg.alert('MELS', "Please select at least one skills");
		}
		else{
		
			var data = {
				UserId :UserID,
				UserType : UserType,
				SkillID : SkillID,
				UserName : UserName,
				PhoneNumber : Phone,
				Pass : AccessToken,
				Email : Email
			}	
			Ext.Viewport.setMasked({
				xtype : "loadmask",
				message : "Please wait"
			});
			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "addSkill",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.addSkill.data;
					if(data.addSkill.Error == 1 || data.addSkill.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.addSkill.Message);
						},100);
						return;
					}
					else{
						if(UserType == "0") //Mentor
							localStorage.setItem("idMentorLoginDetail",Ext.encode(data.addSkill.data));
						else if(UserType == "1") //Mentee
							localStorage.setItem("idMenteeLoginDetail",Ext.encode(data.addSkill.data));
						
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS',"Profile updated sucessfully.",function(){
								if(MentorLoginUser!=null)
									Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
								else
									Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
							});
						},100);
					
					}
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
					},100);
					console.log(responce);
				}
			});
		}
	},
	
	btnInviteForMeeting : function(){
		var GLOBAL = Mentor.Global;
		
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("invitementor");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "invitementor"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});*/
		
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MenteeID : MenteeLoginUser.Id
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMentorForInvitation",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMentorForInvitation.data;
				
				if(data.getMentorForInvitation.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMentorForInvitation.Message);
					},100);
					return;
				}
				else if(data.getMentorForInvitation.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', "No mentor found");
					},100);
				
				}
				else{
					Ext.getStore('InviteMentor').clearData();
					Ext.getStore('InviteMentor').add(data.getMentorForInvitation.data);

				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	btnInviteMentor : function(){
		var GLOBAL = Mentor.Global;
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var MentorSelectField = Ext.getCmp('idMentorSelectFieldInviteMentorScreen');
		var TopicsSelectField = Ext.getCmp('idTopicsSelectFieldInviteMentorScreen');
		var MenteeCommentReasonForMeeting = Ext.getCmp('menteeCommentInviteMentorScreen');
		if(MenteeCommentReasonForMeeting.getValue() == "")
		{
			Ext.Msg.alert('MELS', "Reason for meeting is empty.");
			return;
		}
		
		
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("invitementor");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "invitementor"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
		*/
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MentorID : MentorSelectField.getValue(),
			MenteeID : MenteeLoginUser.Id,
			TopicID : TopicsSelectField.getValue(),
			MenteeComments : MenteeCommentReasonForMeeting.getValue()
			
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "mentorInvitation",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.mentorInvitation.data;
				MenteeCommentReasonForMeeting.setValue('');
				if(data.mentorInvitation.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorInvitation.Message,function(){
							Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
						});
					},100);
					return;
				}
				else if(data.mentorInvitation.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorInvitation.Message,function(){
							Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
						});
					},100);
					
				}
				else{
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorInvitation.Message,function(){
							//me.btnGotoWaitScreenInviteMentorScreen();
							Ext.getCmp('idMenteeInvitesTabView').setActiveItem(1);
						});
					},100);
					
				}
				
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	btnBackInviteBack : function(btn){
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	
	btnGetPendingRequest : function(btn){
		var GLOBAL = Mentor.Global;
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		
		/*var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("mentorPendingRequest");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "mentorPendingRequest"});
		}
		//Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});*/
		
		Ext.getCmp('idUserNameMentorPendingRequest').setHtml(MentorLoginUser.MentorName);
		
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MentorID : MentorLoginUser.Id
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMentorPendingRequest",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMentorPendingRequest.data;
				
				if(data.getMentorPendingRequest.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMentorPendingRequest.Message);
					},100);
					return;
				}
				else if(data.getMentorPendingRequest.Error == 2){
					Ext.getStore('PendingRequest').clearData();
					Ext.getStore('PendingRequest').load();
					Ext.getStore('PendingRequest').sync();
					
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', "There are no outstanding invitations at this time.",function(){
							/*var View = Mentor.Global.NavigationStack.pop();
							var viewport = Ext.Viewport,
								mainPanel = viewport.down("#mainviewport");
							
							mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});*/
							Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
						});
					},100);
					
				}
				else{
					Ext.getStore('PendingRequest').clearData();
					Ext.getStore('PendingRequest').load();
					Ext.getStore('PendingRequest').sync();
					Ext.getStore('PendingRequest').add(data.getMentorPendingRequest.data);
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	btnBackMentorPendingRequest : function(btn){
		/*var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});*/
		Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
	},
	
	respondPendingRequest :function(view, index, item, e){
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("acceptRejectPendingRequest");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "acceptRejectPendingRequest"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

		record = view.getStore().getAt(index).data;
		Mentor.Global.MENTOR_PENDING_REQUEST = record;
		
		Ext.getCmp('idMeetingFromReject').setHtml("Meenting Request from "+record.MenteeName);
		Ext.getCmp('idMeetingTopicAcceptRejectPendingRequest').setValue(record.TopicDescription);
		Ext.getCmp('menteeCommentAcceptRejectPendingRequest').setValue(record.ManteeComments);
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		
		Ext.getCmp('idVersionAcceptRejectPendingRequest').setHtml(Mentor.Global.VERSION_NAME);
		if(MentorLoginUser!=null){
			Ext.getCmp('idUserName').setHtml(MentorLoginUser.MentorName);
			
		}
		else if(MenteeLoginUser!=null){
			Ext.getCmp('idUserName').setHtml(MenteeLoginUser.MenteeName);
			
		}
		
		
	},
	
	doAcceptRequest : function(){
		
		
		var GLOBAL = Mentor.Global;
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		
		var Record = Mentor.Global.MENTOR_PENDING_REQUEST;
		var Comment = Ext.getCmp('commentsAcceptRejectPendingRequest').getValue();
		var Status = "Accepted";
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			InvitationId : Record.InvitationId,
			Status : Status,
			Comments : Comment,
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "mentorResponse",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.mentorResponse.data;
				
				if(data.mentorResponse.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorResponse.Message);
					},100);
					return;
				}
				else if(data.mentorResponse.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorResponse.Message);
					},100);
					
				}
				else{
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorResponse.Message,function(){
								var View = Mentor.Global.NavigationStack.pop();
								var viewport = Ext.Viewport,
									mainPanel = viewport.down("#mainviewport");
								
								mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
								
								me.btnGetPendingRequest();
						});
					},100);
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	doDeclineRequest : function(){
		var GLOBAL = Mentor.Global;
		var Record = Mentor.Global.MENTOR_PENDING_REQUEST;
		var Comment = Ext.getCmp('commentsAcceptRejectPendingRequest').getValue();
		var Status = "Rejected";
		
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			InvitationId : Record.InvitationId,
			Status : Status,
			Comments : Comment,
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "mentorResponse",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.mentorResponse.data;
				
				if(data.mentorResponse.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorResponse.Message);
					},100);
					return;
				}
				else if(data.mentorResponse.Error == 2){
					Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.mentorResponse.Message);
					},100);
				
				}
				else{
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.mentorResponse.Message,function(){
								var View = Mentor.Global.NavigationStack.pop();
								var viewport = Ext.Viewport,
									mainPanel = viewport.down("#mainviewport");
								
								mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
								
								me.btnGetPendingRequest();
						});
					},100);
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
		
	},
	
	btnBackAcceptRejectPendingRequest : function(){
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	btnBackWaitScreen:function(){
		var View = Mentor.Global.NavigationStack.pop();
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		
		mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
	},
	
	btnGotoWaitScreenInviteMentorScreen: function(){
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("waitscreen");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "waitscreen"});
		}
			Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});
		
		me.getMentorForWaitScreen();
	},
	
	getMentorForWaitScreen : function(){
		var GLOBAL = Mentor.Global;
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MenteeID : MenteeLoginUser.Id,
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMentorWaitScreen",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMentorWaitScreen.data;
				
				if(data.getMentorWaitScreen.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMentorWaitScreen.Message,function(){
							Ext.getCmp('idMenteeInvitesTabView').setActiveItem(0);
						});
					},100);
					return;
				}
				else if(data.getMentorWaitScreen.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMentorWaitScreen.Message);
					},100);
					
					var View = Mentor.Global.NavigationStack.pop();
					var viewport = Ext.Viewport,
						mainPanel = viewport.down("#mainviewport");
					
					mainPanel.animateActiveItem(View, {type: "slide",direction: "right"});
					
					me.btnGetPendingRequest();
				}
				else{
					Ext.getStore('WaitScreenMentor').clearData();
					Ext.getStore('WaitScreenMentor').add(data.getMentorWaitScreen.data);
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	getMettingWaitScreenInfo : function(){
		var GLOBAL = Mentor.Global;
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var MentorID = Ext.getCmp('idMentorSelectField').getValue();
		Ext.Viewport.setMasked({
			xtype : "loadmask",
			message : "Please wait"
		});
		
		var data = {
			MenteeID : MenteeLoginUser.Id,
			MentorID : MentorID
		}			
		Ext.Ajax.request({
			url : GLOBAL.SERVER_URL,
			method : "POST",
			dataType : 'json',
			headers : {
				'Content-Type' : 'application/json'
			},
			xhr2 : true,
			disableCaching : false,
			jsonData :{ 
				method: "getMettingWaitScreenInfo",
				body : data
			},
			success : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				var data = Ext.decode(responce.responseText);
				console.log(data);
				d = data.getMettingWaitScreenInfo.data;
				
				if(data.getMettingWaitScreenInfo.Error == 1 ){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMettingWaitScreenInfo.Message);
					},100);
					return;
				}
				else if(data.getMettingWaitScreenInfo.Error == 2){
					Ext.Function.defer(function(){
						Ext.Msg.alert('MELS', data.getMettingWaitScreenInfo.Message);
					},100);
					return;
				}
				else{
					var record = data.getMettingWaitScreenInfo.data[0];
					if(record.Status ==  "Accepted"){
						Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
						Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
						Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has accepted by mentor.');
						
					}
					else if(record.Status ==  "Rejected"){
						Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
						Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
						Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has declined by mentor.');
					}
					else if(record.Status ==  "Pending"){
						Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
						Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
						Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has not accepted by mentor yet.');
					}
				
					if(record.Meeting_status == 1){
							Ext.getCmp('waitScreenMeetingStatus').setValue('Meeting has started.');
					}
					else if(record.Meeting_status == 3){
							Ext.getCmp('waitScreenMeetingStatus').setValue('Meeting has ended.');
					}
					
				}
			
			},
			failure : function(responce) {
				Ext.Viewport.setMasked(false); //Avinash
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS','Something went wrong. Please try again later.');
				},100);
				
			}
		});
	},
	
	btnHistoryScreenWaitScreen : function(){
		
	},
	
	btnRefreshMeetingHistory : function(){
		this.getMeetingHistory();
	},
	
	
	//On Search Mentor Metting History
	onSearchKeyUpMentorMettingHistory: function(searchField) {
		queryString = searchField.getValue();
		console.log(this,'Please search by: ' + queryString);

		var store = Ext.getStore('MeetingHistory');
		store.clearFilter();

		if(queryString){
			var thisRegEx = new RegExp(queryString, "i");
			store.filterBy(function(record) {
				if (thisRegEx.test(record.get('MenteeName')) ||
					thisRegEx.test(record.get('SubTopicName')) ||
					thisRegEx.test(record.get('TopicDescription'))) {
					return true;
				};
				return false;
			});
		}

	},
	
	//On Search Mentee Metting History
	onSearchKeyUpMenteeMettingHistory: function(searchField) {
		queryString = searchField.getValue();
		console.log(this,'Please search by: ' + queryString);

		var store = Ext.getStore('MeetingHistory');
		store.clearFilter();

		if(queryString){
			var thisRegEx = new RegExp(queryString, "i");
			store.filterBy(function(record) {
				if (thisRegEx.test(record.get('MentorName')) ||
					thisRegEx.test(record.get('SubTopicName')) ||
					thisRegEx.test(record.get('TopicDescription'))) {
					return true;
				};
				return false;
			});
		}

	},
	
	// From Invite List from Mentee Login go to Wait Screen
	displayInvitationSendDetailInWaitScreen : function(view, index, item, e){
		
		
		var viewport = Ext.Viewport,
			mainPanel = viewport.down("#mainviewport");
		var mainMenu = mainPanel.down("waitscreen");
		if(!mainMenu){
			mainMenu = mainPanel.add({xtype: "waitscreen"});
		}
		Mentor.Global.NavigationStack.push(mainPanel.getActiveItem());
		mainPanel.animateActiveItem(mainMenu, {type: "slide", direction: 'left'});

		var WaitScreenRecord = view.getStore().getAt(index).data;
			
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		
		if(MentorLoginDetail!=null){
			Ext.getCmp('idUserNameWaitScreen').setHtml(MentorLoginDetail.MentorName);
		}
		else if(MenteeLoginDetail!=null){
			Ext.getCmp('idUserNameWaitScreen').setHtml(MenteeLoginDetail.MenteeName);
		}
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelMentorCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
		Ext.getCmp('idLabelMentorNameListWaitScreen').setTitle(Mentor.Global.MENTOR_NAME + ' Name');
			
		Ext.getCmp('idMentorSelectField').setValue(WaitScreenRecord.InvitationId);
		
		Ext.getCmp('mentorCommentWaitScreen').setValue(WaitScreenRecord.MentorComments);
		Ext.getCmp('idBtnRefreshWaitScreen').setHidden(false);
		if(WaitScreenRecord.Status ==  "Accepted"){
			Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
			Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
			Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has accepted by '+WaitScreenRecord.MentorName + '.');
			
		}
		else if(WaitScreenRecord.Status ==  "Rejected"){
			Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
			Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
			Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has declined by '+WaitScreenRecord.MentorName + '.' );
			Ext.getCmp('idBtnRefreshWaitScreen').setHidden(true);
			
		}
		else if(WaitScreenRecord.Status ==  "Pending"){
			Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
			Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
			Ext.getCmp('waitScreenMeetingStatus').setValue(WaitScreenRecord.MentorName+" has not yet responded.");
		}
	},
	
	//Clear Searach Mentor Metting History
	onClearSearchMentorMettingHistory : function()
	{
		var store = Ext.getStore('MeetingHistory');
		store.clearFilter();
	},
	
	//Clear Searach Mentee Metting History
	onClearSearchMenteeMettingHistory : function()
	{
		var store = Ext.getStore('MeetingHistory');
		store.clearFilter();
	},
	
		
	ValidateEmail:function (mail) 
	{
		if (/^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
		{
			return (true)
		}
		
		return (false)
		//return (true);
	}
	
	
});