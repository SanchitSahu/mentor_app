Ext.define('Mentor.controller.UpdateProfilePicture', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            
        },
        control: {
            "mentorprofile button[action=takeProfilePhoto]": {
            	tap: "takeProfilePhoto"
            },
			"mentorprofile button[action=btnUpdateProfile]":{
				tap : "editProfileWithPhoto"
			},
			"mentorprofile button[action=btnSaveProfile]":{
				tap : "btnSaveProfile"
			},
			
			
        }
    },
	
	//called when the Application is launched, remove if not needed
    launch: function(app) {
       
    },
	
	takeProfilePhoto: function(btnCmp){
		var me = this;
		Ext.Msg.show({
			title: "Profile Image", 
			message: "Select an option",
			buttons: [{
				itemId: "0",
				text: "Gallery", 
				ui:"action"
			},{
				itemId:"1",
				text:"Camera", 
				ui:"action"
			}, {
				itemId: "cancel",
				text: "Cancel",
				ui: "decline"
			}],
			promptConfig: false,
			scope: me,
			fn: function(btn){
				if(btn == "cancel"){
					return;
				}
				this.openCordovaPicker.call(this, btnCmp, Number(btn));
			}
		});
	},
	
	openCordovaPicker: function(component, sourceType){
		var me = this,
			width = (Ext.getBody().getWidth() * 2 * sourceType),
			height = (Ext.getBody().getHeight() * 2 * sourceType);
		navigator.camera.getPicture(function(fileURL){
			component.element.setStyle("backgroundImage", 'url("' + fileURL + '")');
			//component.up('mentorprofile').setProfilePickImageUrl(fileURL);
			//me.uploadProfilePhoto(fileURL);
			Mentor.Global.PROFILE_IMAGE = fileURL;
			me.editProfileWithPhoto(fileURL);
		}, function(error){
			console.log(Ext.encode(error));
			Ext.Msg.alert("Error", Ext.encode(error));
			// alert("Fail " + Ext.encode(error));
		}, {
			quality : 50,
			destinationType : 1,
			/*  DATA_URL : 0,      // Return image as base64-encoded string
			    FILE_URI : 1,      // Return image file URI
			    NATIVE_URI : 2     // Return image native URI (e.g., assets-library:// on iOS or content:// on Android)	*/
			sourceType : sourceType,	//	0,
			/*	PHOTOLIBRARY : 0,
			    CAMERA : 1,
			    SAVEDPHOTOALBUM : 2	*/
			//allowEdit : true,
			correctOrientation: true,
			targetWidth: width ,
			targetHeight: height ,
			encodingType: 1,
			/* JPEG : 0,               // Return JPEG encoded image
			    PNG : 1	*/
			saveToPhotoAlbum: true,
			mediaType: 0
		});
	},
	
	//uploadProfilePhoto: function(btn){
	/*uploadProfilePhoto: function(url){
		var me = this,
			//formPanel = btn.up("mentorprofile"),
			fileURL = formPanel.getProfilePickImageUrl();
		if(Ext.isEmpty(fileURL)){
			Ext.Msg.alert("Message", "Tap the image to select photo then click on upload button");
			return;
		}
		var options = new FileUploadOptions(),
			viewport = Ext.Viewport;
		options.fileKey = "files[]";
		options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
		options.mimeType = "image/png";

		var params = {};
		params.method = "mentorIamge";
		params.MentorID = "1";			
		options.params = params;

		var ft = new FileTransfer(),
			viewport = Ext.Viewport,
			ftURL =  GLOBAL.SERVER_URL;
		ft.onprogress = function(progressEvent) {
			if (progressEvent.lengthComputable) {
				var mask = viewport.getMasked();
				mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),1) * 100 + "%");
			  //mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),2) * 100 + "%");
			}
		};				
		viewport.setMasked({
			xtype: "loadmask",
			message: "Uploading..."
		});
		ft.upload(fileURL, encodeURI(ftURL), function(obj){
			viewport.setMasked(false);
			console.log("Success: "+ Ext.encode(obj));
			var response = Ext.decode(obj.response),
				data = Ext.isArray(response) ? response[0]: response,
				url = data && data.name; 
			formPanel.setProfilePickImageUrl(null);
			me.updateProfilePicture("http://images.allinworldsportsapp.com/"+url);
			// component.element.setStyle("backgroundImage", 'url("http://images.allinworldsportsapp.com/' + url + '")');
		}, function(obj){
			viewport.setMasked(false);
			console.log("Fail: "+ Ext.encode(obj));
			Ext.Msg.alert("Error", "Image upload failed");
		}, options);
	},*/
	
	
	editProfileWithPhoto: function(url){
		var me = this,
			//formPanel = btn.up("mentorprofile"),
			fileURL = Mentor.Global.PROFILE_IMAGE;
			
		/*var Email = Ext.getCmp('email').getValue();
		var Phone = Ext.getCmp('phone').getValue();
		var UserName = Ext.getCmp('userName').getValue();
		var AccessToken = Ext.getCmp('password').getValue();

		if(Email == ""){
			Ext.Msg.alert('MELS', "Please enter email address");
			return;
		}
		else if(Phone == ""){
			Ext.Msg.alert('MELS', "Please enter phone number");
			return;
		}
		else if(UserName == ""){
			Ext.Msg.alert('MELS', "Please enter username");
			return;
		}
		else if(AccessToken == ""){
			Ext.Msg.alert('MELS', "Please enter password");
			return;
		}
		else if(me.ValidateEmail(Email)==false){
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");
			return;
		}
		else if(AccessToken.length < 4){
			Ext.Msg.alert('MELS',"Password length must be four characters.");
			return;
		}*/
		/*if(Ext.isEmpty(fileURL)){
			Ext.Msg.alert("Message", "Tap the image to select photo then click on upload button");
			return;
		}*/
		
		var GLOBAL = Mentor.Global;
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var UserID;
		var UserType;
			
		if(MentorLoginUser!=null){
			UserID = MentorLoginUser.Id;
			UserType= "0"
		}
		else if(MenteeLoginUser!=null){
			UserID = MenteeLoginUser.Id;
			UserType= "1"
		}
		
		var data = {
			UserId :UserID,
			UserType : UserType,
			/*UserName : UserName,
			PhoneNumber : Phone,
			Pass : AccessToken,
			Email : Email*/
			
		}	
		
		
		var options = new FileUploadOptions(),
			viewport = Ext.Viewport;
		options.fileKey = "files[]";
		options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
		options.mimeType = "image/png";

		var params = {};
		params.method = "editProfile";
		params.UserId = UserID;	
		params.UserType = UserType;
		/*params.UserName = UserName;
		params.PhoneNumber = Phone;
		params.Pass = AccessToken;
		params.Email = Email;*/
		options.params = params;

		var ft = new FileTransfer(),
			viewport = Ext.Viewport,
			ftURL =  GLOBAL.SERVER_URL;
		ft.onprogress = function(progressEvent) {
			if (progressEvent.lengthComputable) {
				var mask = viewport.getMasked();
				mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),1) * 100 + "%");
			  //mask.setMessage("Uploading..." + Ext.Number.toFixed((progressEvent.loaded / progressEvent.total),2) * 100 + "%");
			}
		};				
		viewport.setMasked({
			xtype: "loadmask",
			message: "Uploading..."
		});
		ft.upload(fileURL, encodeURI(ftURL), function(obj){
			viewport.setMasked(false);
			console.log("Success: "+ Ext.encode(obj));
			var response = Ext.decode(obj.response),
				data = Ext.isArray(response) ? response[0]: response,
				url = data && data.name; 
			//formPanel.setProfilePickImageUrl(null);
			
			//me.updateProfilePicture("http://images.allinworldsportsapp.com/"+url);
			
			//var data = Ext.decode(obj.responseText);
			console.log(data);
			d = data.editProfile.data;
			if(data.editProfile.Error == 1 || data.editProfile.Error == 2){
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS', data.editProfile.Message);
				},100);
				return;
			}
			else{
				if(UserType == "0") //Mentor
					localStorage.setItem("idMentorLoginDetail",Ext.encode(data.editProfile.data));
				else if(UserType == "1") //Mentee
					localStorage.setItem("idMenteeLoginDetail",Ext.encode(data.editProfile.data));
				
				Ext.Function.defer(function(){
					Ext.Msg.alert('MELS',"Profile Image updated sucessfully.",function(){
						if(MentorLoginUser!=null)
							Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
						else
							Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
					});
				},100);
				
			}
			
			
			// component.element.setStyle("backgroundImage", 'url("http://images.allinworldsportsapp.com/' + url + '")');
		}, function(obj){
			viewport.setMasked(false);
			console.log("Fail: "+ Ext.encode(obj));
			Ext.Msg.alert("Error", "Profile Image not update please try later.");
		}, options);
	},
	
	btnUpdateProfile : function(btn){
		var Email = Ext.getCmp('email').getValue();
		var Phone = Ext.getCmp('phone').getValue();
		var UserName = Ext.getCmp('userName').getValue();
		var AccessToken = Ext.getCmp('password').getValue();

		if(Email == "")
			Ext.Msg.alert('MELS', "Please enter email address");
		else if(Phone == "")
			Ext.Msg.alert('MELS', "Please enter phone number");
		else if(UserName == "")
			Ext.Msg.alert('MELS', "Please enter username");
		else if(AccessToken == "")
			Ext.Msg.alert('MELS', "Please enter password");
		else if(me.ValidateEmail(Email)==false)	
			Ext.Msg.alert('MELS',"You have entered an invalid email address.");
		else if(AccessToken.length < 4)
			Ext.Msg.alert('MELS',"Password length must be four characters.");
		else{
			
			var GLOBAL = Mentor.Global;
			
			var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
			var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
			var UserID;
			var UserType;
				
			if(MentorLoginUser!=null){
				UserID = MentorLoginUser.Id;
				UserType= "0"
			}
			else if(MenteeLoginUser!=null){
				UserID = MenteeLoginUser.Id;
				UserType= "1"
			}
			
			var data = {
				UserId :UserID,
				UserType : UserType,
				UserName : UserName,
				PhoneNumber : Phone,
				Pass : AccessToken,
				Email : Email
				
			}	
			Ext.Viewport.setMasked({
				xtype : "loadmask",
				message : "Please wait"
			});
			
			Ext.Ajax.request({
				url : GLOBAL.SERVER_URL,
				method : "POST",
				dataType : 'json',
				headers : {
					'Content-Type' : 'application/json'
				},
				xhr2 : true,
				disableCaching : false,
				jsonData :{ 
					method: "editProfile",
					body : data
				},
				success : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					var data = Ext.decode(responce.responseText);
					console.log(data);
					d = data.editProfile.data;
					if(data.editProfile.Error == 1 || data.editProfile.Error == 2){
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS', data.editProfile.Message);
						},100);
						return;
					}
					else{
						if(UserType == "0") //Mentor
							localStorage.setItem("idMentorLoginDetail",Ext.encode(data.editProfile.data));
						else if(UserType == "1") //Mentee
							localStorage.setItem("idMenteeLoginDetail",Ext.encode(data.editProfile.data));
						
						Ext.Function.defer(function(){
							Ext.Msg.alert('MELS',"Profile updated sucessfully.");
						},100);
						
					}
				},
				failure : function(responce) {
					Ext.Viewport.setMasked(false); //Avinash
					Ext.Function.defer(function(){
						Ext.Msg.alert('Something went wrong. Please try again later.');
					},100);
					console.log(responce);
				}
			});
		
		}
	
	},
	ValidateEmail:function (mail) 
	{
		if (/^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
		{
			return (true)
		}
		
		return (false)
		//return (true);
	},
	
	btnSaveProfile : function(){
		
	}
	
});