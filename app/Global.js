Ext.define('Mentor.Global',{
	singleton: true,
	SESSION: null,
	CURRENT_LOGIN_USER: null,
	//SERVER_URL: "http://web.mentor.virtualtryon.biz/mentorService",
	//SERVER_URL: "http://rajatlala.com/mentor/webservices/index.php/mentorService",
	SERVER_URL: "http://mobileapp.melstm.net/mentor_app_v1/webservices/index.php/mentorService",
	VERSION_NAME : "MELS v1.0 16/02/25",
	NavigationStack : [],
	TabNavigationStack : [],
	MEETING_DETAIL:null,
	MENTOR_PENDING_REQUEST:null,
	
	// Local Storage ID
	APPLICATION_CONFIGURATION :"applicationConfiguration",
    MENTOR_NAME : "",
	MENTEE_NAME : "",
	PROFILE_IMAGE : "",
	clearNavigationStack:function(){
		NavigationStack=[];
	},
	Timer : "",
});
