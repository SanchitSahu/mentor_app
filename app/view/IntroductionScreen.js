Ext.define('Mentor.view.IntroductionScreen', {
    extend: 'Ext.Carousel',
    xtype: 'introductionscreen',
    requires: [
    ],
    config: {
		cls: 'forgotpassword',
		fullscreen: true,
		direction: 'horizontal',
		/*height: 'Ext.getBody().getSize().height+\'px\'',
        width: 'Ext.getBody().getSize().width+\'px\'',
        layout: {
            type: 'fit'
        },*/
		items:[ /*{
			docked: 'top',
			xtype: 'toolbar',
			title: 'Introduction',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{
					xtype : 'spacer'
				},
				{ 
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP'
				},
			]
		},*/
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'View Past Meetings And Edit Meeting Details As Appropriate!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen1.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
				
			]	
		},
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'Specify Entrepreneur and Meeting Type!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen3.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
			]
		},
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'Accurately Log Meeting Duration By Leveraging Start Session And End Session Buttons!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen4.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
			]
		},
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'Log New Meeting And View Pending Invites From Entrepreneur!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen2.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
			]
		},
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'Specify Mentor Action Items And Rate Meeting Session!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen5.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
			]
		
		},
		{
			xtype : 'panel',
			layout: {
				type: 'vbox',
				pack: 'center',
			},
			items:[
				{
					xtype:'label',
					html: 'Respond to Pending Meeting Request From Fellow Entrepreneurs!',
					style : 'font-size:16px;color:#000000;margin:10px;text-align: center;',
					flex : 0.7,

				},
				{
					xtype: 'image',
					src: 'resources/images/Screen6.png',
					flex : 10,
				},
				{
					xtype: 'button',
					action : 'btnSkipIntroductionScreen',
					text: 'SKIP',
					flex : 0.5,
					margin : 30,
					ui : 'plain'
				},
			]
			
		},

		
		
		]
	}
            
       
});
