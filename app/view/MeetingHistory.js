Ext.define('Mentor.view.MeetingHistory', {
    extend: 'Ext.dataview.List',
    xtype: 'meetinghistory',
	itemId:'itemIdMeetinghistory',
    requires: [
    ],
    config: {
		title: 'Meeting History',
		cls: 'meetinghistory',
		store : 'MeetingHistory',
			itemTpl: '<div class="myContent" style = "display: flex;">'+ 
						'<div style="width:20%; float:left; ">'+ 
							'<img src={MenteeImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">'+ 
						'</div>'+ 
						'<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">'+
							
							'<div style="width:100%"><div style="width:50%; float:left;font-size: 14px;"><b>{MenteeName}</b></div>' +
							'<div style=" float:right;">'+
								'<span style="vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style = "vertical-align: text-top;font-size: 11px;">{MeetingStartDatetime}</span>'+
							'</div></div>' +
							
							//Ratting
							'<div style="width:100%;float:left;"><span style="font-size:11px;background:#373737;padding: 1px 10px;color: white;">{averageText}</span>'+
								'<tpl for=".">'+
									'<img style = "margin-left: 2px;margin-right: 2px;"src="./resources/images/{average}.png" alt="Smiley face" height="10px" >'+
									
								'</tpl>'+
							'</div>'+
							
							//Main Topics
							'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
							
							//Sub Topics Topics
							'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Sub Topics : {SubTopicName}</span>'+
						'<div>'+
					'</div>',
		listeners:{
			itemtap: function(view, index, item, e)
			{
				Mentor.app.application.getController('AppDetail').displayDetail(view, index, item, e);
           
			},
		
		},
		items:[ 
			{
				docked: 'top',
				xtype: 'panel',
				//title: 'Meeting History',
				cls:'mettingHistoryToolBar',
				defaults: {
					iconMask: true
				},
				items:[
					
					{
						xtype : 'toolbar',
						docked: 'top',
						cls:'my-titlebar',
						title: 'Meeting History',
						defaults: {
							iconMask: true
						},
						items:[
							
							{
								iconCls: 'refreshIcon',
								action : 'btnRefreshMeetingHistory',
								docked: 'left',
							},
							{
								iconCls: 'addMettingMentorMettingHistory',
								action : 'btnAddMeetingDetail',
								docked: 'right',
							},
							/*{
								xtype : 'textfield',
								
							}*/
						]
					},
					
					
					/*{
						iconCls: 'home',
						action : 'btnProfileMeetingDetail',
						docked: 'left',
					},*/
					{
						xtype : 'panel',
						cls : 'searchFieldCls',
						items : [
							{
								xtype : 'searchfield',
								placeHolder : 'Search meeting history',
								itemId: 'searchBoxMentorMettingHistory',
								//style : 'background: transparent !important;',
								
							}
						]
					}
					
				]
			},
			{
				xtype : 'label',
				id : 'idUserNameMentorMeetingHistory',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
				hidden : true,
				
			},
			{
				xtype : 'label',
				id : 'idVersionMeetingHistory',
				style : 'text-align: right;padding-right: 10px;padding: 10px;',
				docked: 'bottom',
				hidden : true,
				
			},
			{
				xtype : 'container',
				hidden : true,	
				layout: {
					type: 'hbox',
					pack: 'center',
					
				},
				docked: 'top',
				items:[{
					xtype : 'button',
					label : 'Add New Meeting',
					cls : 'signin-btn-cls',
					text : 'New Meeting',
					style : 'width:45%;margin-bottom:10px;font-size: 15px;',
					action : 'btnAddMeetingDetail',
					
				},
				{
					xtype : 'button',
					label : 'Add New Meeting',
					cls : 'signin-btn-cls',
					text : 'Pending Invites',
					style : 'width:45%;margin-bottom:10px;font-size: 15px;',
					action : 'btnGetPendingRequest',
					id : 'idBtnPendingInvitesMeetingHistoryMentorScreen'
				}
				]
				
			}
		],
		
		/*listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]*/
			
	},
	onPageActivate: function(){
		Mentor.Global.TabNavigationStack = 0;
	}
            
       
});
