Ext.define('Mentor.view.WaitScreen', {
    extend: 'Ext.Panel',
    xtype: 'waitscreen',
    requires: [
    ],
    config: {
		cls: 'enterpreneur_action_items',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
            indicators: {
				y: {
                    autoHide: false
                }
            }
        },
		defaults:{
			xtype:'container',
        },
		items:[ 
		{
			docked: 'top',
			xtype: 'toolbar',
			title: 'Invite Status',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					iconCls: 'btnBackCLS',
					action : 'btnBackWaitScreen',
					docked: 'left',
					
				},
				{ 
					action : 'btnRefreshWaitScreen',
					/*xtype : 'button',
					text : 'Refresh',
					cls : 'signin-btn-cls',*/
					docked: 'right',
					iconCls: 'refreshIcon',
					id : 'idBtnRefreshWaitScreen'
					
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameWaitScreen',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
					
		},
		{
			xtype : 'panel',
			layout :{ type: 'vbox',},
			items:[
				{
			
					xtype: 'fieldset',
					title: 'Mentor Name',
					id:'idLabelMentorNameListWaitScreen',
					items: [
						{
							xtype: 'selectfield',
							id:	'idMentorSelectField',
							store: 'WaitScreenMentor',
							displayField: 'MentorName',
							valueField : 'InvitationId',
							listeners: {
								change: function(field, value) {
									if (value instanceof Ext.data.Model) {
										value = value.get(field.getValueField());
									}
									console.log(value);
									var Store = Ext.getStore('WaitScreenMentor');
									var record = Store.findRecord('InvitationId', value);
									if(record!=null){
										Ext.getCmp('mentorCommentWaitScreen').setValue(record.data.MentorComments);
										Ext.getCmp('idBtnRefreshWaitScreen').setHidden(false);
										if(record.data.Status ==  "Accepted"){
											Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(true);
											Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
											Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has accepted by '+record.data.MentorName + '.');
											
										}
										else if(record.data.Status ==  "Rejected"){
											Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(true);
											Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
											Ext.getCmp('waitScreenMeetingStatus').setValue('Invitation has declined by '+record.data.MentorName + '.' );
											Ext.getCmp('idBtnRefreshWaitScreen').setHidden(true);
											
										}
										else if(record.data.Status ==  "Pending"){
											Ext.getCmp('radioBtnDeclinedWaitScreen').setChecked(false);
											Ext.getCmp('radioBtnAcceptedWaitScreen').setChecked(false);
											Ext.getCmp('waitScreenMeetingStatus').setValue(record.data.MentorName+" has not yet responded.");
										}
									}
									
									//Pass value parameter to the 2nd select field's store
								}
							}

						}
					]
					
				},
				{
					xtype : 'label',
					html : 'Status',
					style : 'font-weight: bold;',
					margin : '0 10 0 20',
					
				},
				{
					xtype: 'textfield',
					name: 'waitScreenMeetingStatus',
					id: 'waitScreenMeetingStatus',
					disabled :true,
					disabledCls : 'textFieldDisableCLS',
					margin : '0 10 0 10',
					/*disabled :false,
					listeners : {
						element : 'element',
						tap : function() {
							Mentor.app.application.getController('AppDetail').changeMenteeDetail();
						}
					}*/
					
				},
				{
					xtype : 'label',
					html : 'Mentor Invitation Response :',
					style : 'font-weight: bold;',
					margin : '20 10 0 20',
					hidden : true,
					
				},
				{
					xtype : 'panel',
					hidden : true,
					layout: {
						type: 'hbox',
						align : 'center'
					},
					items: [
						{
							xtype: 'radiofield',
							name : 'color',
							value: '0',
							label: 'Accepted',
							labelWidth: 100,
							flex :1,
							checked: false,
							labelCls : 'radio-btn-cls',
							cls : 'statisfactionCLS',
							id : 'radioBtnAcceptedWaitScreen',
							disabled :true,
						},
						{
							xtype: 'radiofield',
							name : 'color',
							value: '1',
							labelWidth: 100,
							flex :1,
							label: 'Declined',
							labelCls : 'radio-btn-cls',
							cls : 'statisfactionCLS',
							id : 'radioBtnDeclinedWaitScreen',
							disabled :true,
						}
					]
				},
				{
					xtype : 'label',
					html : 'Mentor Comment',
					margin : '20 10 0 20',
					style : 'font-weight: bold;',
					id:'idLabelMentorCommentsWaitScreen'
					

				},
				{
					xtype: 'textareafield',
					name: 'mentorComment',
					id: 'mentorCommentWaitScreen',
					disabledCls : 'textFieldDisableCLS',
					maxRows: 2,
					disabled :true,
					margin : '0 10 0 10',
					
					
				},
			]
		},
		{
			xtype : 'container',
			layout :{ type: 'hbox'},
			items:[
				/*{
					xtype : 'button',
					text : 'History',
					cls : 'signin-btn-cls',
					action : 'btnHistoryScreenWaitScreen',
					flex:1
				},*/
				{
					xtype : 'button',
					text : 'Check Current Status',
					cls : 'signin-btn-cls',
					flex:1,
					action : 'btnRefreshWaitScreen',
					hidden : true,
					
				}
			]
		},
		
		
		
		
		],
		/*listeners:[{
			event: "activate",
			fn: "onPageActivate"
		},
		{
			event: "painted",
			fn: "onPagePainted"
		},
		]*/
	},
	onPageActivate: function(){
		
		//Check The Login User and display the name on the top header
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		
		if(MentorLoginDetail!=null){
			Ext.getCmp('idUserNameWaitScreen').setHtml(MentorLoginDetail.MentorName);
		}
		else if(MenteeLoginDetail!=null){
			Ext.getCmp('idUserNameWaitScreen').setHtml(MenteeLoginDetail.MenteeName);
		}
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelMentorCommentsWaitScreen').setHtml(Mentor.Global.MENTOR_NAME + ' Comment');
		Ext.getCmp('idLabelMentorNameListWaitScreen').setTitle(Mentor.Global.MENTOR_NAME + ' Name');
		
	},
	
	onPagePainted : function(){
		//Mentor.app.application.getController('AppDetail').getMentorForWaitScreen();
	}
            
       
});
