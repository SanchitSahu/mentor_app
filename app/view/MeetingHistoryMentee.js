Ext.define('Mentor.view.MeetingHistoryMentee', {
    extend: 'Ext.dataview.List',
    xtype: 'meetinghistorymentee',
    requires: [
    ],
    config: {
		cls: 'meetinghistory',
		store : 'MeetingHistory',
		/*plugins: [
			{
				xclass: 'Ext.plugin.PullRefresh',
				pullText: 'Pull down for more new Tweets!'
			}
		],*/
		/*itemTpl: '<div class="myContent">'+ 
				'<div><b>{MeetingStartDatetime}</b></div>' +
				'<div>Meeting notes with <b>{MentorName}</b></div>' +
				'</div>',*/
		itemTpl: '<div class="myContent" style = "display: flex;">'+ 
						'<div style="width:20%; float:left; ">'+ 
							'<img src={MentorImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">'+ 
						'</div>'+ 
						'<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">'+
							
							'<div style="width:100%"><div style="width:50%; float:left;font-size: 14px;"><b>{MentorName}</b></div>' +
							//'<div style="width:50%; float:right;">'+
							'<div style="float:right;">'+
								'<span style="vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style = "vertical-align: text-top;font-size: 11px;">{MeetingStartDatetime}</span>'+
							'</div></div>' +
							
							//Ratting
							'<div style="width:100%;float:left;"><span style="font-size:11px;background:#373737;padding: 1px 10px;color: white;">Average</span>'+
								'<tpl for=".">'+
									'<img style = "margin-left: 2px;margin-right: 2px;"src="./resources/images/{average}.png" alt="Smiley face" height="10px" >'+
								'</tpl>'+
							'</div>'+
							
							//Main Topics
							'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
							
							//Sub Topics Topics
							'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Sub Topics : {SubTopicName}</span>'+
						'<div>'+
					'</div>',
		listeners:{
		itemtap: function(view, index, item, e)
		{
			Mentor.app.application.getController('AppDetail').displayDetail(view, index, item, e);
           
		}
	},
		items:[ 
			/*{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Meeting History',
				cls:'my-titlebar',
				defaults: {
					iconMask: true
				},
				items:[
					{
						iconCls: 'refreshIcon',
						action : 'btnRefreshMeetingHistory',
						docked: 'right',

					},
					{
						iconCls: 'home',
						action : 'btnProfileMeetingDetail',
						docked: 'left',
						style : 'visibility: hidden;',
					}
					/*{
						iconCls: 'reply',
						action : 'btnAddMeetingDetail',
						docked: 'left',
					}*/
			/*	]
			},*/
			{
				docked: 'top',
				xtype: 'panel',
				//title: 'Meeting History',
				cls:'mettingHistoryToolBar',
				defaults: {
					iconMask: true
				},
				items:[
					
					{
						docked: 'top',
						xtype: 'toolbar',
						title: 'Meeting History',
						cls:'my-titlebar',
						defaults: {
							iconMask: true
						},
						items:[
							
							{
								iconCls: 'refreshIcon',
								action : 'btnRefreshMeetingHistory',
								docked: 'right',

							},
							{
								iconCls: 'home',
								action : 'btnProfileMeetingDetail',
								docked: 'left',
								style : 'visibility: hidden;',
							}
							/*{
								iconCls: 'reply',
								action : 'btnAddMeetingDetail',
								docked: 'left',
							}*/
						]
					},
					
					
					/*{
						iconCls: 'home',
						action : 'btnProfileMeetingDetail',
						docked: 'left',
					},*/
					{
						xtype : 'panel',
						cls : 'searchFieldCls',
						//style : 'background: black !important;',
						items : [
							{
								xtype : 'searchfield',
								placeHolder : 'Search meeting history',
								itemId: 'searchBoxMenteeMettingHistory',
								//style : 'background: black !important;',
							}
						]
					}
					
				]
			},
			{
				xtype : 'label',
				id : 'idUserName',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
				hidden : true,
			},
			{
				xtype : 'label',
				id : 'idVersionMenteeMeetingHistory',
				style : 'text-align: right;padding-right: 10px;padding: 10px;',
				docked: 'bottom',
				hidden : true,
			},
			{
				xtype : 'container',
				hidden : true,	
				layout: {
					type: 'hbox',
					pack: 'center',
					
				},
				docked: 'top',
				items:[{
					xtype : 'button',
					label : 'Add New Meeting',
					cls : 'signin-btn-cls',
					text : 'Issue Invite',
					style : 'width:45%;margin-bottom:10px;',
					action : 'btnInviteForMeeting',
				},
				{
					xtype : 'button',
					label : '',
					cls : 'signin-btn-cls',
					text : 'Invite Status',
					style : 'width:45%;margin-bottom:10px;',
					action : 'btnInviteStatusMeetingHistory',
				}
				]
				
			}
		],
			
	}
            
       
});
