Ext.define('Mentor.view.InviteMentor', {
    extend: 'Ext.Panel',
    xtype: 'invitementor',
    requires: [
    ],
    config: {
		cls: 'meetinghistory',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
            indicators: {
				y: {
                    autoHide: false
                }
            }
        },
		defaults:{
			xtype:'container',
        },
		items:[ 
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Invite Screen',
				cls:'my-titlebar',
				hidden : true,
				defaults: {
					iconMask: true
				},
				items:[
					{
						iconCls: 'arrow_left',
						action : 'btnBackInviteBack',
						docked: 'left',
					},
					{
						iconCls: 'arrow_right',
						action : 'btnGotoWaitScreenInviteMentorScreen',
						docked: 'right',
					}
				]
			},
			{
				xtype : 'label',
				id : 'idUserName',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
				hidden : true,
			},
			{
				xtype : 'label',
				id : 'idVersionInviteMentor',
				style : 'text-align: right;padding-right: 10px;padding: 10px;',
				docked: 'bottom',
				hidden : true,
			},
			{
			
				xtype: 'fieldset',
				title: 'Select Mentor',
				id:'idLabelSelectMentorInvireMentor',
				items: [
					{
						xtype: 'selectfield',
						id:	'idMentorSelectFieldInviteMentorScreen',
						store: 'InviteMentor',
						displayField: 'MentorName',
						valueField : 'MentorID',
						listeners: {
							change: function(field, value) {
								if (value instanceof Ext.data.Model) {
									value = value.get(field.getValueField());
								}
								console.log(value);
								var Store = Ext.getStore('InviteMentor');
								var record = Store.findRecord('MentorID', value);
								if(record!=null){
									if(record.data.rStatus == "TRUE"){
										Ext.getCmp('idSendBtnInviteMentorScreen').setDisabled(true);
										Ext.getCmp('idSendBtnInviteMentorScreen').setText("Invitation Already Exists");
										Ext.getCmp('idTopicsSelectFieldInviteMentorScreen').setDisabled(true);
										Ext.getCmp('menteeCommentInviteMentorScreen').setDisabled(true);
									}
									else{
										Ext.getCmp('idSendBtnInviteMentorScreen').setDisabled(false);
										Ext.getCmp('idSendBtnInviteMentorScreen').setText("Send Invitation");
										Ext.getCmp('idTopicsSelectFieldInviteMentorScreen').setDisabled(false);
										Ext.getCmp('menteeCommentInviteMentorScreen').setDisabled(false);
										
									}
								}
								//Pass value parameter to the 2nd select field's store
							}
						}

					}
				]
			
			},
			
			{
			
				xtype: 'fieldset',
				title: 'Select Main Topic',
				margin : '-20 10 0 10',
				items:[
					{
						xtype: 'selectfield',
						id : 'idTopicsSelectFieldInviteMentorScreen',
						store: 'Topics',
						displayField: 'TopicDescription',
						valueField : 'TopicID',
						
						
					}
				]
			
			},
			
			{
				xtype : 'label',
				html : 'Reason for Meeting Request',
				margin : '20 0 0 20',
				style : 'font-weight: bold;'
				

			},
			{
				xtype: 'textareafield',
				name: 'mentorComment',
				id: 'menteeCommentInviteMentorScreen',
				cls : 'textFieldDisableCLS',
				maxRows: 2,
				disabled :false,
				margin : '0 10 0 10',
				inputCls : 'inputCLS',
				 clearIcon : false,
				
				
			},
			
			{
				xtype : 'container',
				layout :{ type: 'hbox'},
				items:[
					{
						xtype : 'button',
						text : 'Send Invitation',
						cls : 'signin-btn-cls',
						action: "btnSendInviteMentorScreen",
						id : 'idSendBtnInviteMentorScreen',
						flex:1
					},
					/*{
						xtype : 'button',
						text : 'NEXT',
						cls : 'signin-btn-cls',
						flex:1,
						
					}*/
				]
			},
			
		],
		
		
		
		listeners:[
		{
			event: "activate",
			fn: "onPageActivate"
		},
		{
			event: "painted",
			fn: "onPagePainted"
		},
		]	
	},
	
	onPageActivate: function(){
		console.log("asaasdsadasf");
		var TopicsStore = Ext.getStore('Topics').load();
		 Ext.getStore('InviteMentor').load();
		Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);
		
		//Ext.getStore('InviteMentor').load();
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelSelectMentorInvireMentor').setTitle('Select '+Mentor.Global.MENTOR_NAME);
		
	},
	onPagePainted : function(){
		console.log("asaasdsadasf");

		var TopicsStore = Ext.getStore('Topics').load();
		Ext.getStore('InviteMentor').load();
		Ext.getCmp('idVersionInviteMentor').setHtml(Mentor.Global.VERSION_NAME);
		
		//Ext.getStore('InviteMentor').load();
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelSelectMentorInvireMentor').setTitle('Select '+Mentor.Global.MENTOR_NAME);
		Mentor.app.application.getController('AppDetail').btnInviteForMeeting();
	}

   
       
});
