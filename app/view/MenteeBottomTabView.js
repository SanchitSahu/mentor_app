Ext.define('Mentor.view.MenteeBottomTabView', {
    extend: 'Ext.TabPanel',
	xtype : 'menteeBottomTabView',
	id  : 'idMenteeBottomTabView',
    requires: [
      'Mentor.view.MeetingHistory'  
    ],

    config: {
		ui : 'light',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
			defaults: {
				flex: 1
			},
        },
        items: [
			{
				xtype : 'meetinghistorymentee',
				iconCls : 'mettingHistoryIconMentorBottomTabView',
				iconMask: true,
				Cls : 'mettingHistoryMentorBottomTabView',
				id : 'mettingHistoryMentorBottomTabView',
				title : 'Meeting History',
				
			},
			{
				xtype : 'menteeInvitesTabView',
				iconCls : 'pendingInvitesIconMentorBottomTabView',
				iconMask: true,
				title : 'Invites',
				/*listeners : {
					element : 'element',
					painted : function() {
						Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
					}
				}*/
			
			},
			{
				xtype : 'mentorprofile',
				iconCls : 'myProfileIconMentorBottomTabView',
				iconMask: true,
				title : 'My Profile',
				
			},
			
        ]
    }
});