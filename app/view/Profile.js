Ext.define('Mentor.view.Profile', {
    extend: 'Ext.Panel',
    xtype: 'mentorprofile',
	itemId:'itemIdProfile',
    requires: [
    ],
    config: {
		cls: 'mentorprofile',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
			indicators: {
				y: {
					autoHide: false
				}
			}
		},
		defaults:{
			xtype:'container',
        },
		items:[ 
		{
			docked: 'top',
			xtype: 'panel',
			cls:'mettingHistoryToolBar',
			defaults: {
				iconMask: true
			},
			layout:{
				type : 'vbox',
				pack : 'center',
				align : 'middle',
			},
			items:[
	
				{
					docked: 'top',
					xtype: 'toolbar',
					title: 'Profile',
					cls:'my-titlebar',
					defaults: {
						iconMask: true
					},
					items : [
						{
							iconCls: 'btnBackCLS',
							docked: 'left',
							//style : 'visibility: hidden;',
							handler : function(){
								var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
								if(MentorLoginUser!=null)
									Ext.getCmp('idMentorBottomTabView').setActiveItem(0);
								else
									Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
							}
						},
						{
							iconCls: 'saveProileIconCLS',
							docked: 'right',
							//action : 'btnSaveProfile',
							action: "btnAddSkills"
						},
						
						
					]
				},
				
				{
					xtype: "button",
					action: "takeProfilePhoto",
					id:'id_ProfilePicture',
					cls: "photo-preview-cls",
					margin : 10,
					
				},
				{
					xtype : 'label',
					id : 'idUserNameProfileScreen',
					style : 'color: white;margin-bottom: 10px;',
							
				},
					
				
			]
		},
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
		},
		
		{
			xtype : 'panel',
			layout :{ type: 'vbox',},
			items:[
				{
					xtype : 'panel',
					margin : 10,
					items : [
						{
							xtype : 'label',
							html : 'Email',
							cls : 'userProfileLabelCLS',
						},
						{
							xtype : 'textfield',
							//label : 'Email',
							id : 'email',
							disabled :false,
							clearIcon  : false,
							inputCls : 'inputCLS',
						},
						{
							xtype : 'image',
							style : 'border-bottom: 1px #cacaca solid;'
						},
						
						
						{
							xtype : 'label',
							html : 'Username',
							cls : 'userProfileLabelCLS',
							margin: '10 0 0 0'
						},
						
						{
							xtype : 'textfield',
							//label : 'Username',
							id : 'userName',
							disabled :false,
							clearIcon  : false,
							inputCls : 'inputCLS',
						},
						{
							xtype : 'image',
							style : 'border-bottom: 1px #cacaca solid;'
						},
						
						
						{
							xtype : 'label',
							html : 'Password',
							cls : 'userProfileLabelCLS',
							margin: '10 0 0 0'
						},
						{
							xtype : 'textfield',
							//label : 'Password',
							id : 'password',
							disabled :false,
							clearIcon  : false,
							inputCls : 'inputCLS',
						},
						{
							xtype : 'image',
							style : 'border-bottom: 1px #cacaca solid;'
						},
						
						
						{
							xtype : 'label',
							html : 'Phone',
							cls : 'userProfileLabelCLS',
							margin: '10 0 0 0'
						},
						{
							xtype : 'textfield',
							//label : 'Phone',
							id : 'phone',
							disabled :false,
							clearIcon  : false,
							inputCls : 'inputCLS',
						},
						{
							xtype : 'image',
							style : 'border-bottom: 1px #cacaca solid;'
						},
						
						
					]
				},
				{
					xtype: 'panel',
					style : 'background : #f2f2f2;padding: 10px 30px;',
					margin: '10 2 0 2',
					items:[
						{
							xtype : 'label',
							html : 'Add Interests',
							cls : 'profileAddInterestCLS',
							id : 'addInterestProfile'
						},
						{
							xtype : 'label',
							html : 'Tap on any interests to add them to your profile.',
							cls : 'profileAddInterestDetailCLS'
						},
					]
				},
				{
					xtype : 'button',
					text : 'Save Contact Info',
					id : 'idButtonUpdateProfile',
					cls : 'signin-btn-cls',
					action: "btnUpdateProfile",
					margin: '10 0 0 0',
					hidden : true,
				},
				/*{
					xtype: 'fieldset',
					title: 'Add Mentee Skills',
					items: [
						{
							xtype : 'panel',
							id : 'idSkillsProfile',
							margin : '10 30 0 30',
							defaults: {
								style: {
									float: 'left',
									pack : 'center',
								}
							},
					
						},
						
					]
				},*/
				{
					xtype:'label',
					html : 'Add Skills',
					id : 'idLabelAddSkillProfile',
					style : 'margin-left:10px;margin-right:10px;margin-top:20px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
					hidden : true,
				},
				{
					xtype : 'panel',
					id : 'idSkillsProfile',
					margin : '10 30 5 30',
					defaults: {
						style: {
							float: 'left',
							pack : 'center',
						}
					},
					control: {
						'button[action="addInterest"]': {
							tap: function (element) {
								if(element.getCls() == 'profileSkillsActiveCLS')
									element.setCls('profileSkillsInActiveCLS');
								else
									element.setCls('profileSkillsActiveCLS');
								/*element.toggleCls('profileSkillsActiveCLS');
								var buttonCls = element.getCls();*/
								
							}
						}
					},
					
				},
				{
					xtype : 'button',
					text : 'Add Skills',
					id : 'idButtonAddSkillProfile',
					cls : 'signin-btn-cls',
					action: "btnAddSkills",
					hidden:true,
				}
				
		
			]
		}],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		var SkillsStore = Ext.getStore('Skills').load();
		var SkillsPanel = Ext.getCmp('idSkillsProfile');
		var me = this;
		SkillsPanel.removeAll();
		
		var MentorLoginUser = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginUser = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		var Skills;
		if(MentorLoginUser!=null){
			
			Ext.getCmp('idUserNameProfileScreen').setHtml(MentorLoginUser.MentorName);
			Ext.getCmp('userName').setValue(MentorLoginUser.MentorName);
			Ext.getCmp('email').setValue(MentorLoginUser.MentorEmail);
			Ext.getCmp('phone').setValue(MentorLoginUser.MentorPhone);
			Ext.getCmp('password').setValue(MentorLoginUser.Password);
			Skills = MentorLoginUser.Skills;
			if(MentorLoginUser.MentorImage!=""){
				me.down("button[action=takeProfilePhoto]").element
						.setStyle("backgroundImage", 'url("' + MentorLoginUser.MentorImage + '")');
			}
			
			Ext.getCmp('addInterestProfile').setHtml('Skills Possessed');
		}
		else if(MenteeLoginUser!=null){
			Ext.getCmp('idUserNameProfileScreen').setHtml(MenteeLoginUser.MenteeName);
			Ext.getCmp('userName').setValue(MenteeLoginUser.MenteeName);
			Ext.getCmp('email').setValue(MenteeLoginUser.MenteeEmail);
			Ext.getCmp('phone').setValue(MenteeLoginUser.MenteePhone);
			Ext.getCmp('password').setValue(MenteeLoginUser.Password);
			
			Ext.getCmp('idLabelAddSkillProfile').setHtml("Add Interests");
			Ext.getCmp('idButtonAddSkillProfile').setText("Add Interests");
			Ext.getCmp('addInterestProfile').setHtml('Skills Needed');
			
			Skills = MenteeLoginUser.Skills;
			if(MenteeLoginUser.MenteeImage!=""){
				me.down("button[action=takeProfilePhoto]").element
						.setStyle("backgroundImage", 'url("' + MenteeLoginUser.MenteeImage + '")');
			}
		}
		
		for(i = 0 ; i<SkillsStore.data.length;i++){
			var checked = false;
			var avtiveSkillCLS = 'profileSkillsInActiveCLS';
			
			var SkillsArray = Skills.split(',');
			for(j = 0;j<SkillsArray.length;j++){
				var SkillsID = SkillsArray[j].split('~')
				if(SkillsID[0] == SkillsStore.data.getAt(i).data.SkillID)
				{
					checked = true;
					avtiveSkillCLS = 'profileSkillsActiveCLS';
					break;
				}
			}
			
			
			var radioField = Ext.create('Ext.Button', {
				//name: 'recorded_stream',
				id: SkillsStore.data.getAt(i).data.SkillID,
				text: SkillsStore.data.getAt(i).data.SkillName,
				//labelCls : 'profileSkillsCLS',
				//labelWidth: 250,
				//checked : checked,
				cls : avtiveSkillCLS,
				//margin : 2,
				action : 'addInterest',
				
			});
			SkillsPanel.add(radioField);
		}
		
		
		
		
	}
            
       
});
