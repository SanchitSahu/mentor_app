Ext.define('Mentor.view.SignUp', {
    extend: 'Ext.Panel',
    xtype: 'signup',
    requires: [
    ],
    config: {
		cls: 'signUp',
		defaults:{
			xtype:'formpanel',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Register',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					iconCls: 'arrow_left',
					action : 'back_signup'
				},
			]
		},
		{
			xtype: 'panel',
			cls : 'login-top',
			paddingTop:10,
			layout: {
				type: 'hbox',
				pack: 'center',
				
			},
			items:[
				{
					xtype: 'container',
					layout: {
						type: 'vbox',
						pack: 'center',
						
					},
					items:[
						{
							xtype:'label',
							html: 'Mentor',
							style : 'font-size:30px;color:#cf3229;'

						},
						
					]
				}
			]
		},
		{
			xtype : 'panel',
			flex :1,
			style : 'margin-top:50px;',
			items:[
				{
					xtype: 'fieldset',
					items: [
						{
							xtype: 'emailfield',
							name: 'email',
							id : 'emailSignUp',
							placeHolder : 'Email'
						},
						{
							xtype: 'passwordfield',
							name: 'password',
							id : 'passwordSignUp',
							placeHolder : 'Password',
							maxLength : 8,
						},
						{
							xtype: 'textfield',
							name: 'accesstoken',
							id : 'accessTokenSignUp',
							placeHolder : 'AccessToken'
						}
					]
				},
				{
					xtype : 'panel',
					layout: {
						type: 'hbox',
						align : 'stretch'
					},
					items: [
						{
							xtype: 'radiofield',
							name : 'color',
							value: '0',
							label: 'Mentor',
							labelWidth: 100,
							flex :1,
							checked: true,
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMentor'
						},
						{
							xtype: 'radiofield',
							name : 'color',
							value: '1',
							labelWidth: 100,
							flex :1,
							label: 'Mentee',
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMentee',
						}
					]
				},
				{
					xtype : 'button',
					text : 'REGISTER',
					id : 'btn_singin',
					cls : 'signin-btn-cls',
					action : 'doRegisterSignUp'
				},
				
				
			]
		},
		
		]
	}
            
       
});
