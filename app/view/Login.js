Ext.define('Mentor.view.Login', {
    extend: 'Ext.Panel',
    xtype: 'login',
    requires: [
    ],
    config: {
		cls: 'loginCLS',
		defaults:{
			xtype:'formpanel',
        },
		items:[ 
		{
			docked: 'top',
			xtype: 'toolbar',
			title: 'Sign In',
			cls:'my-titlebar',
			hidden : true,
		},
		{
			xtype: 'panel',
			cls : 'login-top',
			paddingTop:10,
			layout: {
				type: 'hbox',
				pack: 'center',
				
			},
			/*items:[
				{
					xtype: 'container',
					layout: {
						type: 'vbox',
						pack: 'center',
						
					},
					items:[
						{
							xtype:'label',
							html: 'Mentor',
							style : 'font-size:30px;color:#cf3229;'

						},
						
					]
				}
			]*/
		},
		{
			xtype : 'panel',
			flex :1,
			style : 'margin-top:50px;',
			layout: {
				type: 'vbox',
				pack: 'center',
				
			},
			items:[
				//{
					//xtype: 'fieldset',
					//items: [
						{
							xtype : 'label',
							html : 'Sign In',
							cls : 'signInLabelLoginCLS',
							
						},
						{
							xtype: 'emailfield',
							name: 'email',
							id: 'emailLogin',
							placeHolder : 'UserName',
							cls : 'userNameLoginCLS',
							margin : '20 20 0 20',
						},
						{
							xtype: 'passwordfield',
							name: 'password',
							id: 'passwordLogin',
							placeHolder : 'Password',
							cls : 'passwordLoginCLS',
							margin : '20 20 0 20',
						},
					//]
				//},
				/*{
					xtype : 'panel',
					layout: {
						type: 'hbox',
						align : 'stretch'
					},
					items: [
						{
							xtype: 'radiofield',
							name : 'color',
							value: '0',
							label: 'Mentor',
							labelWidth: 100,
							flex :1,
							checked: true,
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMentorLogin'
						},
						{
							xtype: 'radiofield',
							name : 'color',
							value: '1',
							labelWidth: 100,
							flex :1,
							label: 'Mentee',
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMenteeLogin'
						}
					]
				},*/
				
				
				
			]
		},
		
		{
		
			xtype : 'panel',
			layout: {
				pack: 'center'
			},
			items:[
				{
					xtype : 'button',
					text : 'SIGN IN',
					id : 'id_btn_singin',
					cls : 'signin-btn-cls',
					action: "doLogin",
					margin : '20 100 0 100',
				},
				{
					xtype : 'button',
					text : 'Forgot Password',
					ui : 'plain',
					//docked: 'middle',
					cls : 'forgotpassword-btn-cls',
					action : 'doForgotPassword'
				},
				
			]
			
		
		},
		
		/*{
			xtype : 'button',
			text : 'REGISTER',
			id : 'id_btn_register',
			cls : 'signin-register-btn-cls',
			action: "btnRegister",
			style : 'margin-top:60px;',
		},*/
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;margin-bottom:10px;margin-left:10px;font-size:15px;',
			docked: 'bottom',
			
					
		},
		
		]
	}
            
       
});
