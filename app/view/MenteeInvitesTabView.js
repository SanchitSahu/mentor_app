Ext.define('Mentor.view.MenteeInvitesTabView', {
    extend: 'Ext.TabPanel',
    xtype: 'menteeInvitesTabView',
	id : 'idMenteeInvitesTabView',
    config: {
		tabBar: {
			docked: 'top',
			layout: {
				pack: 'center'
			},
			defaults: {
				flex: 1
			},
		},
       
        items: [
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Invites',
				cls:'mettingHistoryToolBar',
				id: 'invitesTab',
				cls : 'invitesTab',
				defaults: {
					iconMask: true
				},
				items : [
					{ 
						iconCls: 'btnBackCLS',
						//iconCls: 'addMettingMentorMettingHistory',
						//action : 'btnBackMenteeReviewForm',
						docked : 'left',
						//style : 'visibility: hidden;',
						handler:function(){
							Ext.getCmp('idMenteeBottomTabView').setActiveItem(0);
						}
					},
					{ 
						//iconCls: 'arrow_left',
						iconCls: 'addMettingMentorMettingHistory',
						action : 'btnBackMenteeReviewForm',
						docked : 'right',
						style : 'visibility: hidden;',
					},
				]
			},
			{
				xtype : 'invitementor',
				title: 'Issue Invite',
				
				
			},
			{
				title: 'Invite Status',
				//xtype : 'waitscreen',
				xtype : 'inviteStatusList'
			}
			/*{
				xtype : 'panel',
				//iconCls: 'home',
				title: 'home',
				
				 tabBar: {
					docked: 'top',
					layout: {
						pack: 'center'
					},
					defaults: {
						flex: 1
					},
				},
				items : [
					{
						xtype : 'invitementor',
						title: 'Issue Invite',
						listeners : {
							element : 'element',
							painted : function() {
								Mentor.app.application.getController('AppDetail').btnInviteForMeeting();
							}
						}
						
					},{
						title: 'Invite Status',
						
					}
				]
			},*/
			
		]
    }
});