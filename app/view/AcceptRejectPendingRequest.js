Ext.define('Mentor.view.AcceptRejectPendingRequest', {
    extend: 'Ext.Panel',
    xtype: 'acceptRejectPendingRequest',
    requires: [
    ],
    config: {
		cls: 'forgotpassword',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
            indicators: {
				y: {
                    autoHide: false
                }
            }
        },
		defaults:{
			xtype:'formpanel',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Respond Pending Request',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					//iconCls: 'arrow_left',
					iconCls: 'btnBackCLS',
					action : 'btnBackAcceptRejectPendingRequest'
				},
			]
		},
		{
				xtype : 'label',
				id : 'idUserName',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
				hidden : true,
		},
		{
			xtype : 'label',
			id : 'idVersionAcceptRejectPendingRequest',
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
		},
		{
			xtype: 'panel',
			cls : 'login-top',
			paddingTop:10,
			layout: {
				type: 'hbox',
				pack: 'center',
				
			},
			items:[
				{
					xtype: 'container',
					layout: {
						type: 'vbox',
						pack: 'center',
						
					},
					items:[
						{
							xtype:'label',
							id:'idMeetingFromReject',
							style : 'font-size:16px;color:#000000;margin:10px;'

						},
						
					]
				}
			]
		},
		{
			xtype : 'label',
			html : 'Meeting Topic',
			style : 'font-weight: bold;',
			margin : '10 10 0 20',
			
		},
		{
			xtype: 'textfield',
			name: 'acceptRejectPendingRequestMeetingTopic',
			id: 'idMeetingTopicAcceptRejectPendingRequest',
			disabled :true,
			disabledCls : 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
			margin : '0 10 0 10',
			/*disabled :false,
			listeners : {
				element : 'element',
				tap : function() {
					Mentor.app.application.getController('AppDetail').changeMenteeDetail();
				}
			}*/
			
		},
		{
			xtype : 'label',
			html : 'Mentee Comments',
			style : 'font-weight: bold;',
			margin : '20 10 0 20',
			id:'idLabelMenteeCommentsAcceptRejectPendingRequest'
			
		},
		{
			xtype: 'textareafield',
			name: 'menteeCommentAcceptRejectPendingRequest',
			id: 'menteeCommentAcceptRejectPendingRequest',
			disabledCls : 'textFieldDisableAcceptAndRejectMenteeRequestCLS',
			maxRows: 2,
			disabled :true,
			margin : '0 10 0 10',
			clearIcon : false,
			
		},
		{
			xtype : 'panel',
			flex :1,
			style : 'margin-top:10px;',
			items:[
				{
					xtype: 'fieldset',
					title : 'Comments',
					items: [
						{
							xtype: 'textareafield',
							id: 'commentsAcceptRejectPendingRequest',
							placeHolder : 'Comments',
							inputCls : 'inputCLS',
							clearIcon : false,
						}
						
					]
				},
				
				
			]
		},
		{
			xtype: 'container',
			layout: {
				type: 'hbox',
				pack: 'center',
				
			},
			items:[
				{
					xtype:'button',
					//text : 'ACCEPT',
					cls : 'tick',
					action : 'doAcceptRequest'

				},
				{
					xtype:'button',
					//text : 'DECLINE',
					cls : 'cancel',
					action : 'doDeclineRequest'

				},
				
			]
		}
		
		
		],
		
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelMenteeCommentsAcceptRejectPendingRequest').setHtml(Mentor.Global.MENTEE_NAME + ' Comments');
		
	}
	
            
       
});
