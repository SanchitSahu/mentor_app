Ext.define('Mentor.view.MenteeReviewForm', {
    extend: 'Ext.Panel',
    xtype: 'menteereviewform',
    requires: [
    ],
    config: {
		cls: 'menteereviewform',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
			indicators: {
				y: {
					autoHide: false
				}
			}
		},
		defaults:{
			xtype:'formpanel',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Meeting Edit Form',
			cls:'my-titlebar-review_form',
			id :'idMenteeReviewFormTitle',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					//iconCls: 'arrow_left',
					iconCls: 'btnBackCLS',
					action : 'btnBackMenteeReviewForm'
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
					
		},
		{
					xtype: 'panel',
					margin : '10',
					layout: {
						type: 'vbox',
					},
					items:[
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting With Whom',
									cls : 'reviewFormLabelCLS',
									flex : 2
									 
								},
								{
									xtype: 'textfield',
									name: 'meetingWithWhom',
									id: 'meetingWithWhomMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*action : 'btnAddMeetingDetail',
									disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting Type',
									cls : 'reviewFormLabelCLS',
									flex : 2,
								},
								{
									xtype: 'textfield',
									name: 'meetingType',
									id: 'meetingTypeMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting Place',
									cls : 'reviewFormLabelCLS',
									flex : 2,
								},
								{
									xtype: 'textfield',
									name: 'meetingType',
									id: 'meetingPlaceMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting Subject',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									
								},
								{
									xtype: 'textfield',
									name: 'meetingSubject',
									id: 'meetingSubjectMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeTimingDetail();
										}
									}*/
									
								},
							]
						},
						/*{
							xtype : 'label',
							html : 'Meeting Date',
							margin : '10 0 0 0'
						},
						{
							xtype: 'textfield',
							name: 'meetingDate',
							id: 'meetingmeetingDateMentorReviewForm',
							
						},*/
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting Start Time',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									
								},
								{
									xtype: 'textfield',
									name: 'meetingStartTime',
									id: 'meetingStartTimeMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeTimingDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Meeting End Time',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textfield',
									name: 'meetingEndTime',
									id: 'meetingEndTimeMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeTimingDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Elapsed Time (Minutes)',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textfield',
									name: 'meetingElapsedTime',
									id: 'meetingElapsedTimeMenteeReviewForm',
									disabled :true,
									cls : 'textFieldDisableCLS',
									flex : 3,
									/*clearIcon  : false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeTimingDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Subtopics Discussed',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textareafield',
									name: 'mainIssueDiscussed',
									id: 'mainIssueDiscussedMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									maxRows: 2,
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMeetingDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Action Item By Mentor',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									id: 'idLabelActionItemByMentor'


								},
								{
									xtype: 'textareafield',
									name: 'actionItemsByMentor',
									id: 'actionItemsByMentorMenteeReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									maxRows: 2,
									flex : 3,
									/*disabled :false,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMentorActionItemsDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
								{
									xtype : 'label',
									html : 'Action Items By Mentee',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									id: 'idLabelActionItemByMentee'
									

								},
								{
									xtype: 'textareafield',
									name: 'actionItemsByMentee',
									id: 'actionItemsByMenteeMenteeReviewForm',
									//cls : 'textFieldDisableCLS',
									//maxRows: 2,
									//disabled :false,
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									maxRows: 2,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeActionItemsDetail();
										}
									}
									
								},
							]
						},
						
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[	
						
								{
									xtype : 'label',
									html : 'Mentee Comment',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									id: 'idLabelMenteeComment'
									

								},
								{
									xtype: 'textareafield',
									name: 'mentorComment',
									id: 'menteeCommentMenteeReviewForm',
									cls : 'textFieldDisableCLS',
									maxRows: 2,
									disabled :false,
									clearIcon : false,
									flex : 3,
									
									
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							style : 'background-color: #f2f2f2;',
							items:[
								{
									xtype : 'label',
									html : 'Satisfaction with meeting: 1 being very poor and 5 being very good.(This anonymous rating is for use only by administrator)',
									margin : '20 0 0 0',
									cls : 'reviewFormLabelCLS',
									style : 'text-align: -webkit-center;'
								},
								{
									xtype : 'label',
									html : '{1-5}  Clear and understandable feedback and advice.',
									margin : '10 0 0 0',
									style : 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
									//style : 'font-weight: bold;'
								},
							
								{
									xtype: 'ratingfield',
									name : 'usefull',
									label: ' ',
									labelWidth : '0%',
									value: 0,
									id:'idSatisfactionClearAndUnderstandableMenteeReviewForm',
									labelCls : 'background:red',
									//style: 'background:red'
									style : 'background-color: #f2f2f2;'
								},
								
								
								{
									xtype : 'label',
									html : '{1-5} Applicable to overall subject area.',
									margin : '10 0 0 0',
									//style : 'font-weight: bold;'
									style : 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
								},
							
								{
									xtype: 'ratingfield',
									name : 'usefull',
									label: ' ',
									labelWidth : '0%',
									value: 0,
									id:'idSatisfactionOverallSubjectMenteeReviewForm',
									labelCls : 'background:red',
									//style: 'background:red'
									style : 'background-color: #f2f2f2;'
								},
								
								
								{
									xtype : 'label',
									html : '{1-5} Relevant to my particular problem.',
									margin : '10 0 0 0',
									//style : 'font-weight: bold;'
									style : 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
								},
								
								{
									xtype: 'ratingfield',
									name : 'usefull',
									label: ' ',
									labelWidth : '0%',
									value: 0,
									id:'idSatisfactionReleveantProblemMenteeReviewForm',
									labelCls : 'background:red',
									//style: 'background:red'
									style : 'background-color: #f2f2f2;'
								},
								
								
								{
									xtype : 'label',
									html : '{1-5} Executable/Actionable practical advice and feedback.',
									margin : '10 0 0 0',
									//style : 'font-weight: bold;'
									style : 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'
								},
								
								{
									xtype: 'ratingfield',
									name : 'usefull',
									label: ' ',
									labelWidth : '0%',
									value: 0,
									id:'idSatisfactionAdviceAndFeedbackMenteeReviewForm',
									labelCls : 'background:red',
									//style: 'background:red'
									style : 'background-color: #f2f2f2;'
								},
								
							]
						}
						
					]
		},
		{
			xtype : 'button',
			text : 'Submit',
			id : 'idBtnSubmitMenteeReviewForm',
			cls : 'signin-btn-cls',
			action: "doSubmitMenteeReviewForm"
		},
				
			
		
		],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelActionItemByMentor').setHtml('Action Item By '+Mentor.Global.MENTOR_NAME);
		Ext.getCmp('idLabelActionItemByMentee').setHtml('Action Items By '+Mentor.Global.MENTEE_NAME);
		Ext.getCmp('idLabelMenteeComment').setHtml(Mentor.Global.MENTEE_NAME+ ' Comment' );
		
		
	}
            
       
});
