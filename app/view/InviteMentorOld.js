Ext.define('Mentor.view.InviteMentorOld', {
    extend: 'Ext.dataview.List',
    xtype: 'invitementor',
    requires: [
    ],
    config: {
		cls: 'meetinghistory',
		store : 'InviteMentor',
			itemTpl: '<div class="myContent" style="height: 40px;">'+ 
				'<div style="width:49%;float: left;padding-top: 10px;"><b>{MentorName}</b></div>' +
				'<div style = "width:20%;float: right;text-align:-webkit-center;"> '+
				'<div class=x-button type="button" style=border: none;>Invite</div></div>'+
				'</div>',
		listeners:{
			/*itemtap: function(view, index, item, e)
			{
				//Mentor.app.application.getController('AppDetail').displayDetail(view, index, item, e);
			   
			},*/
			itemtap: function(view, index, target, record, event) {
				 console.log('Item was tapped on the Data View');
				 console.log(view, index, target, record, event);
				 if(event.target.type == "button"){
					Mentor.app.application.getController('AppDetail').inviteMentor(record.data.MentorID);
				 }
				 else {
					Mentor.app.application.getController('AppDetail').inviteMentor(record.data.MentorID);
				 }
			},
		},
		items:[ 
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Issue Invitations',
				cls:'my-titlebar',
				defaults: {
					iconMask: true
				},
				items:[
					{
						iconCls: 'arrow_left',
						action : 'btnBackInviteBack',
						docked: 'left',
					}
				]
			},
			{
				xtype : 'label',
				id : 'idUserName',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
			},
			
		],
		
			
	},
	/*onItemTap: function(view, index, target, record, event) {
		 console.log('Item was tapped on the Data View');
		 console.log(view, index, target, record, event);
		var btnType = target.getTarget('div.x-button').getAttribute('type');
		if(btnType){
		  Ext.Msg.alert('You clicked on the Button ...', 
		  'The country code selected is: ' + event.target.name);
		 }
		 else {
		  Ext.Msg.alert('You clicked on the Row ...', 
		  'The country code selected is: ' + record.get('code'));
		 }
    },*/
 

            
       
});
