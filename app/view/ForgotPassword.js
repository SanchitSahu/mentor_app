Ext.define('Mentor.view.ForgotPassword', {
    extend: 'Ext.Panel',
    xtype: 'forgotpassword',
    requires: [
    ],
    config: {
		cls: 'forgotpassword',
		defaults:{
			xtype:'formpanel',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Forgot Password',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					iconCls: 'btnBackCLS',
					action : 'btnBackForgotPassword'
				},
			]
		},
		{
			xtype: 'panel',
			cls : 'login-top',
			paddingTop:10,
			layout: {
				type: 'hbox',
				pack: 'center',
				
			},
			items:[
				{
					xtype: 'container',
					layout: {
						type: 'vbox',
						pack: 'center',
						
					},
					items:[
						{
							xtype:'label',
							html: 'Plese provide your registered email here, we will send your password to your registered email address.',
							style : 'font-size:16px;color:#BBBBBB;margin:10px;'

						},
						
					]
				}
			]
		},
		{
			xtype : 'panel',
			flex :1,
			style : 'margin-top:50px;',
			items:[
				{
					xtype: 'fieldset',
					items: [
						{
							xtype: 'emailfield',
							name: 'email',
							id: 'emailForgotPassword',
							placeHolder : 'Email'
						}
						
					]
				},
				/*{
					xtype : 'panel',
					layout: {
						type: 'hbox',
						align : 'stretch'
					},
					items: [
						{
							xtype: 'radiofield',
							name : 'color',
							value: '0',
							label: 'Mentor',
							labelWidth: 100,
							flex :1,
							checked: true,
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMentorForgotPassword'
						},
						{
							xtype: 'radiofield',
							name : 'color',
							value: '1',
							labelWidth: 100,
							flex :1,
							label: 'Mentee',
							labelCls : 'radio-btn-cls',
							id : 'radioBtnMenteeForgotPassword'
						}
					]
				},*/
				{
					xtype : 'button',
					text : 'RESET PASSWORD',
					action : 'btnResetPassword',
					cls : 'signin-btn-cls',
				},
				
			]
		},
		
		]
	}
            
       
});
