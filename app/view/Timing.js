Ext.define('Mentor.view.Timing', {
    extend: 'Ext.Panel',
    xtype: 'timing',
    requires: [
    ],
    config: {
		cls: 'timing',
		defaults:{
			xtype:'container',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Timings',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					//iconCls: 'arrow_left',
					iconCls: 'btnBackCLS',
					action : 'btnBackTimingScreen'
					
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
					
		},
		{
			xtype : 'panel',
			margin : 10,
			layout :{ type: 'vbox',},
			items:[
			
				{
					xtype: 'fieldset',
					title: 'Select Main Topics',
					//style : 'margin-top: -20px;',
					items:[{
						xtype: 'selectfield',
						id : 'idTopicsTimingScreen',
						store: 'Topics',
						displayField: 'TopicDescription',
						valueField : 'TopicID'
					}]
				},
				{
					xtype : 'panel',
					layout :{ type: 'hbox',},
					items : [
						{
							xtype : 'button',
							text : 'Start Session',
							cls : 'startSession-btn-cls',
							id  : 'startSession',
							style : 'margin-right: 0.5em; margin-left: 0.5em;',
							flex : 1,
							handler : function(){
								Mentor.app.application.getController('Main').doStartSession();
							}
							
						},
						{
							xtype : 'textfield',
							disabled :false,
							id  : 'startSessionEditMettingDetail',
							style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
							cls : 'textFieldDisableCLS',
							hidden: true,
							flex : 1,
							listeners: {
								keyup: function(form, e) {
									Mentor.app.application.getController('Main').calculateElapsedTime();
								}
							}
							
						},
						{
							xtype : 'button',
							text : 'End Session',
							cls : 'endSession-btn-cls',
							id  : 'endSession',
							style : 'margin-right: 0.5em; margin-left: 0.5em;',
							flex : 1,
							handler : function(){
								Mentor.app.application.getController('Main').doEndSession();
							}
							
						},
						{
							xtype : 'textfield',
							disabled :false,
							id  : 'endSessionEditMettingDetail',
							style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
							cls : 'textFieldDisableCLS',
							hidden: true,
							flex : 1,
							listeners: {
								keyup: function(form, e) {
									Mentor.app.application.getController('Main').calculateElapsedTime();
								}
							}
						},
					]
				},
				
				/*{
					xtype: 'fieldset',
					title: 'End Session',
					style : 'margin-top: -20px;',
					items:[{
						/*xtype : 'datetimepickerfield',
						placeHolder : 'End Session',
						id : 'endSession',
						dateTimeFormat  : 'Y-m-d H:i:s',
						minHours:1, 
						maxHours: 24,
						//value: new Date(),
						picker :{
							value: (new Date()),
							listeners : {
								element : 'element',
								tap : function() {
									clearInterval(timer);
									timer = "";
									Mentor.app.application.getController('Main').calculateElapsedTime();
								}
							}
						},
						listeners : {
							element : 'element',
							tap : function() {
								Ext.getCmp('endSession').setValue(new Date());
							}
						}*/
						/*xtype : 'textfield',
						disabled :true,
						id  : 'endSession',
						listeners : {
								element : 'element',
								tap : function() {
									Mentor.app.application.getController('Main').doEndSession();
								}
							}
						
						
					}]*/
				//},
				
				{
					xtype: 'fieldset',
					title: 'Elapsed Time (Rounded)',
					style : 'margin-top: 10px;',
					items: [
						{
			
							xtype : 'textfield',
							id : 'MeetingElapsedTime',
							placeHolder : 'Session Length',
							disabled: true,
			
						},
					]
				},
				{
					xtype : 'container',
					layout :{ type: 'hbox'},
					items:[
						{
							xtype : 'button',
							text : 'Next',
							cls : 'signin-btn-cls',
							action: "btnNextTiming",
							id : 'idBtnNextTiming',
							flex:1,
						},
						{
							xtype : 'button',
							text : 'Update',
							cls : 'signin-btn-cls',
							flex:1,
							hidden : true,
							id : 'idBtnUpdateTimingScreen',
							action : 'btnUpdateTiming', //UpdateMeetingDetail Controller
							
						}
					]
				}
		
			]
		}
	
		],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		console.log('safasf');
		var TopicsStore = Ext.getStore('Topics').load();
		
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		var TopicsSelectField = Ext.getCmp('idTopicsTimingScreen');
		//Ext.getCmp('startSession').setValue(new Date());
		//Ext.getCmp('endSession').setValue('');
		Ext.getCmp('MeetingElapsedTime').setValue('');
		
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
		
		if(MeetingDetail!=null){
			Ext.getCmp('idTopicsTimingScreen').setValue(MeetingDetail.MeetingTopicID);
			Ext.getCmp('MeetingElapsedTime').setValue(MeetingDetail.MeetingElapsedTime);
			
			Ext.getCmp('startSessionEditMettingDetail').setHidden(false);
			Ext.getCmp('endSessionEditMettingDetail').setHidden(false);
			//Ext.getCmp('MeetingElapsedTime').setDisabled(false);
			
			Ext.getCmp('startSessionEditMettingDetail').setValue(MeetingDetail.MeetingStartDatetime);
			Ext.getCmp('endSessionEditMettingDetail').setValue(MeetingDetail.MeetingEndDatetime);
			
			
			//Ext.getCmp('startSession').setText(MeetingDetail.MeetingStartDatetime);
			//Ext.getCmp('endSession').setText(MeetingDetail.MeetingEndDatetime);
			Ext.getCmp('startSession').setHidden(true);
			Ext.getCmp('endSession').setHidden(true);
			
			timer = "";
			
			Ext.getCmp('idBtnUpdateTimingScreen').setHidden(false);
			Ext.getCmp('idBtnNextTiming').setDisabled(false);
		}
		else{
			//
			/*timer = setInterval(function(){
				Mentor.app.application.getController('Main').calculateElapsedTime();
				console.log("a")
			},10000);*/
			
			Ext.getCmp('startSessionEditMettingDetail').setHidden(true);
			Ext.getCmp('endSessionEditMettingDetail').setHidden(true);
			Ext.getCmp('startSession').setHidden(false);
			Ext.getCmp('endSession').setHidden(false);
			
			Ext.getCmp('idBtnUpdateTimingScreen').setHidden(true);
			
			// If user store the Start and End Session display back
			var MeetingTimingStartEndSession= Ext.decode(localStorage.getItem("storeStartEndSession"));
			if(MeetingTimingStartEndSession!=null){
				MeetingStartDatetime = MeetingTimingStartEndSession.StartDate;
				MeetingEndDatetime =  MeetingTimingStartEndSession.MeetingEndDatetime;
				MeetingElapsedTime =  MeetingTimingStartEndSession.MeetingElapsedTime;
				
				Ext.getCmp('startSession').setText(MeetingStartDatetime);
				Ext.getCmp('endSession').setText(MeetingEndDatetime);
				Ext.getCmp('MeetingElapsedTime').setValue( MeetingElapsedTime);
				
				Ext.getCmp('startSession').setDisabled(true);
				Ext.getCmp('endSession').setDisabled(true);
				Ext.getCmp('idBtnNextTiming').setDisabled(false);
				
			
			}
			
		}
	}
            
       
});
