Ext.define('Mentor.view.MentorBottomTabView', {
    extend: 'Ext.TabPanel',
	xtype : 'mentorBottomTabView',
	id : 'idMentorBottomTabView',
	requires: [
      'Mentor.view.MeetingHistory'  
    ],

    config: {
		ui : 'light',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
			defaults: {
				flex: 1
			},
        },
        items: [
			{
				xtype : 'meetinghistory',
				iconCls : 'mettingHistoryIconMentorBottomTabView',
				iconMask: true,
				Cls : 'mettingHistoryMentorBottomTabView',
				id : 'mettingHistoryMentorBottomTabView',
				
			},
			{
				xtype : 'mentorPendingRequest',
				iconCls : 'pendingInvitesIconMentorBottomTabView',
				iconMask: true,
				title : 'Pending Invites',
				/*listeners : {
					element : 'element',
					painted : function() {
						Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
					}
				}*/
			
			},
			{
				xtype : 'mentorprofile',
				iconCls : 'myProfileIconMentorBottomTabView',
				iconMask: true,
				title : 'My Profile',
				
			},
			
        ],
		listeners:[{
			event: "activeitemchange",
			fn: "activeitemchange"
			//function : bottomchange( this, value, oldValue, eOpts ){alert(1)}
		}]
    },
	activeitemchange: function( a, value, oldValue, eOpts ){
		/*if("itemIdMeetinghistory" == oldValue._itemId)
			Mentor.Global.TabNavigationStack.push(0);
		else if("itemIdmentorPendingRequest" == oldValue._itemId)
			Mentor.Global.TabNavigationStack.push(1);
		else if("itemIdProfile" == oldValue._itemId)
			Mentor.Global.TabNavigationStack.push(2);*/
	}
});