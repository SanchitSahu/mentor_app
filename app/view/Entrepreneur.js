Ext.define('Mentor.view.Entrepreneur', {
    extend: 'Ext.Panel',
    xtype: 'entrepreneur',
    requires: [
    ],
    config: {
		cls: 'entrepreneur',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
            indicators: {
				y: {
                    autoHide: false
                }
            }
        },
		defaults:{
			xtype:'container',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'New Meeting',
			cls:'my-titlebar',
			id: 'idTitleEntrepreneur',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					//iconCls: 'arrow_left',
					iconCls: 'btnBackCLS',
					action : 'btnBackEnterpreneur'
					
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			id : 'idVersionEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
		},
		{
			
			xtype: 'fieldset',
			title: 'Select Entrepreneur',
			id:'idLabelSelectMenteeEnterpreneurScreen',
			cls : 'fieldSetCLS',
			labelCls : 'fieldSetLabelCLS',
			items: [
				{
					xtype: 'selectfield',
					id:	'idEnterpreneurSelectField',
					store: 'Mentee',
					displayField: 'MenteeName',
					valueField : 'MenteeID',
					listeners: {
						change: function(field, value) {
							if (value instanceof Ext.data.Model) {
								value = value.get(field.getValueField());
								
							}
							console.log(value);
							var Store = Ext.getStore('Mentee');
							var record = Store.findRecord('MenteeID', value);
							if(record!=null){
								Ext.getCmp('idTopicsTimingScreen').setValue(record.data.TopicID);
								Ext.getCmp('idMenteeCommentEnterpreneurScreen').setValue(record.data.MenteeComments);
							}
							else{
								Ext.getCmp('idMenteeCommentEnterpreneurScreen').setValue("");
							}
						}
					}
				},
				
			]
			
		},
		{
			
			xtype: 'fieldset',
			title: 'Meeting Type',
			id:'idLabelSelectMeetingTypeEnterpreneurScreen',
			items: [
				{
					xtype: 'selectfield',
					id:	'idMettingTypeSelectField',
					store: 'MeetingType',
					displayField: 'MeetingTypeName',
					valueField : 'MeetingTypeID'

				}
			]
			
		},
		{
			
			xtype: 'fieldset',
			title: 'Meeting Place',
			id:'idLabelSelectMeetingPlaceEnterpreneurScreen',
			items: [
				{
					xtype: 'selectfield',
					id:	'idMeetingPlaceSelectField',
					store: 'MeetingPlace',
					displayField: 'MeetingPlaceName',
					valueField : 'MeetingPlaceID'

				}
			]
			
		},
		//Timing Screen
		{
			xtype: 'fieldset',
			title: 'Select Main Topic',
			id:'idLabelSelectMainTopicEnterpreneurScreen',
			items:[{
				xtype: 'selectfield',
				id : 'idTopicsTimingScreen',
				store: 'Topics',
				displayField: 'TopicDescription',
				valueField : 'TopicID',
				disabled : true,
			}]
		},
		{
			
			xtype: 'fieldset',
			title: 'Mentee Comment',
			id:'idLabelMenteeCommentEnterpreneurScreen',
			items: [
				{
					xtype: 'textareafield',
					//name: 'mainIssueDiscussed',
					id: 'idMenteeCommentEnterpreneurScreen',
					disabled :true,
					maxRows: 2,
					flex : 3,

				},
			]
			
		},
		
		{
			xtype : 'panel',
			//layout :{ type: 'hbox',},
			items : [
				{
					xtype : 'button',
					text : 'Start Session',
					cls : 'startSession-btn-cls',
					id  : 'startSession',
					style : 'margin-right: 0.5em; margin-left: 0.5em;',
					flex : 1,
					action : 'doStartSession',
					/*handler : function(){
						Mentor.app.application.getController('Main').doStartSession();
					}*/
					
				},
				{
					xtype : 'textfield',
					disabled :false,
					id  : 'startSessionEditMettingDetail',
					style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
					cls : 'textFieldDisableCLS',
					hidden: true,
					flex : 1,
					listeners: {
						keyup: function(form, e) {
							Mentor.app.application.getController('Main').calculateElapsedTime();
						}
					}
					
				},
				{
					xtype : 'button',
					text : 'End Session',
					cls : 'endSession-btn-cls',
					id  : 'endSession',
					style : 'margin-right: 0.5em; margin-left: 0.5em;',
					flex : 1,
					action : 'doEndSession'
					/*handler : function(){
						Mentor.app.application.getController('Main').doEndSession();
					}*/
					
				},
				{
					xtype : 'textfield',
					disabled :false,
					id  : 'endSessionEditMettingDetail',
					style : 'margin-top: 10px;margin-right: 0.5em; margin-left: 0.5em;',
					cls : 'textFieldDisableCLS',
					hidden: true,
					flex : 1,
					listeners: {
						keyup: function(form, e) {
							Mentor.app.application.getController('Main').calculateElapsedTime();
						}
					}
				},
				
			]
		},
		{
			xtype: 'fieldset',
			title: 'Elapsed Time (Rounded up to the next minute)',
			style : 'margin-top: 20px;',
			hidden:true,
			items: [
				{
	
					xtype : 'textfield',
					id : 'MeetingElapsedTime',
					placeHolder : 'Session Length',
					disabled: true,
					hidden:true,
				},
			]
		},
		
		{
			xtype: 'container',
			layout: {
				type: 'vbox',
				pack: 'center',
				align: 'center',
				
			},
			
			items: [
				{
					xtype : 'label',
					html :  'Elapsed Time (Rounded up to the next minute)',
					style : 'margin-top: 10px;',
				},
				{
					//flex: 5,
					items: [
						{
							xtype: 'container',
							id: 'stopWatchPanel',
							cls: 'stopWatchPanel',
							html: '00:00',
							hidden: true
						},
						{
							xtype: 'container',
							flex: 1,
							layout: 'hbox',
							defaults: {
								xtype: 'label',
								//ui: 'round',
								action: 'stopWatchButton',
								cls: 'stopWatchButton',
							},
							items: [
								
								{
									xtype : 'panel',
									pack: 'center',
									layout:
									{
										type: 'vbox',
										pack: 'center',
										xtype: 'label',
									},
									items:[
										{html: '00', id: 'hrs', cls: 'timeBlock'},
										{
											html: 'Hours',
											cls: 'labeltime',
										},
									]
								},
								{
									xtype : 'panel',
									pack: 'center',
									layout:
									{
										type: 'vbox',
										pack: 'center',
										xtype: 'label',
									},
									items:[
										{
											html: ':', id: 'seperator', cls: 'timeBlock'
										},
										
									]
								},
								{
									xtype : 'panel',
									pack: 'center',
									layout:
									{
										type: 'vbox',
										pack: 'center',
										xtype: 'label',
									},
									items:[
										{
											html: '00', id: 'min', cls: 'timeBlock'
										},
										{
											html: 'Minutes',
											cls: 'labeltime'
										},
									]
								},
							],
						},
						/*{
							xtype: 'container',
							pack: 'center',
							//flex: 1,
							layout:
									{
										type: 'hbox',
										pack: 'center',
										xtype: 'label',
									},
							items: [
								{
									html: 'Hours',
									cls: 'labeltime',
								},
								{
									html: 'Minutes',
									cls: 'labeltime'
								},
							]
						},*/
					]
				},
			]

		},
		{
			xtype : 'panel',
			layout :{ type: 'vbox',},
			items:[
				/*{
					xtype:'label',
					html : 'Meeting Type',
					style : 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;'
				},*/
				/*{
					xtype : 'panel',
					id : 'idMeetingType',
					hidden : true,

				},*/
				/*{
					xtype: 'radiofield',
					name : 'color',
					value: '0',
					label: 'In Person',
					checked: true,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'radiofield',
					name : 'color',
					value: '1',
					labelWidth: 100,
					label: 'Phone',
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'radiofield',
					name : 'color',
					value: '0',
					label: 'Email',
					checked: true,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'radiofield',
					name : 'color',
					value: '1',
					labelWidth: 100,
					label: 'SMS',
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'radiofield',
					name : 'color',
					value: '1',
					labelWidth: 100,
					label: 'Webinar',
					labelCls : 'radio-btn-cls'
				},*/
				{
					xtype : 'container',
					layout :{ type: 'hbox',},
					items:[
						{
							xtype : 'button',
							text : 'Next',
							cls : 'signin-btn-cls',
							action: "btnNextEnterpreneur",
							flex:1,
							//width : '45%',
							id : 'idBtnNextEnterpreneur'
						},
						{
							xtype : 'button',
							text : 'Update',
							cls : 'signin-btn-cls',
							flex:1,
							hidden : true,
							id : 'idBtnUpdateEnterpreneur',
							action : 'btnUpdateEnterpreneur', //UpdateMeetingDetail Controller
							//width : '45%'
						}
					]
				},
			]
		}
		
		],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		},
		{
			event: "painted",
			fn: "onPagePainted"
		},
		]
	},
	
	onPageActivate: function(){
		console.log("asaasdsadasf");
		
		Ext.getCmp('startSession').setText('Start Session');
		Ext.getCmp('startSession').setDisabled(false);
		Ext.getCmp('endSession').setText('End Session');
		Ext.getCmp('endSession').setDisabled(true);
		Ext.getCmp('idBtnNextEnterpreneur').setDisabled(true)
		
		
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		//Mentor.app.application.getController('Main').getMentee(MentorLoginDetail.Id,false);
		
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		
		
		Ext.getCmp('idEnterpreneurSelectField').getStore().load();
		var MeetingTypeStore = Ext.getStore('MeetingType').load();
		var MeenteeStore = Ext.getStore('Mentee').load();
		var MeetingPlaceStore = Ext.getStore('MeetingPlace').load();
		
		Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
		Ext.getCmp('idVersionEnterpreneur').setHtml(Mentor.Global.VERSION_NAME);
		
		
		/*** Timing Screen***/
		var TopicsStore = Ext.getStore('Topics').load();
			var TopicsStore = Ext.getStore('Topics').load();
		
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		var TopicsSelectField = Ext.getCmp('idTopicsTimingScreen');
		//Ext.getCmp('startSession').setValue(new Date());
		//Ext.getCmp('endSession').setValue('');
		Ext.getCmp('MeetingElapsedTime').setValue('');
		
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
		
		if(MeetingDetail!=null){
			Ext.getCmp('idTitleEntrepreneur').setTitle('Update Meeting');
			Ext.getCmp('idTopicsTimingScreen').setValue(MeetingDetail.MeetingTopicID);
			Ext.getCmp('MeetingElapsedTime').setValue(MeetingDetail.MeetingElapsedTime);
			
			Ext.getCmp('startSessionEditMettingDetail').setHidden(false);
			Ext.getCmp('endSessionEditMettingDetail').setHidden(false);
			//Ext.getCmp('MeetingElapsedTime').setDisabled(false);
			
			Ext.getCmp('startSessionEditMettingDetail').setValue(MeetingDetail.MeetingStartDatetime);
			Ext.getCmp('endSessionEditMettingDetail').setValue(MeetingDetail.MeetingEndDatetime);
			
			
			//Ext.getCmp('startSession').setText(MeetingDetail.MeetingStartDatetime);
			//Ext.getCmp('endSession').setText(MeetingDetail.MeetingEndDatetime);
			Ext.getCmp('startSession').setHidden(true);
			Ext.getCmp('endSession').setHidden(true);
			
			timer = "";
			
			//Ext.getCmp('idBtnUpdateTimingScreen').setHidden(false);
			Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
			
			
			var Hours = parseInt(MeetingDetail.MeetingElapsedTime/60);
			var Minutes = MeetingDetail.MeetingElapsedTime%60;
			if(Minutes<9)
				Ext.getCmp("min").setHtml("0"+Minutes);
			else
				Ext.getCmp("min").setHtml(Minutes);
			
			if(Hours<9)
				Ext.getCmp("hrs").setHtml("0"+Hours);
			else
				Ext.getCmp("hrs").setHtml(Hours);
			
			if(isNaN(Hours) || Hours<0)
				Ext.getCmp("hrs").setHtml("00");
			if(isNaN(Minutes) || Minutes<0)
				Ext.getCmp("min").setHtml("00");
			
			//Mentor.app.application.getController('Main').calculateElapsedTime();
		}
		else{
			Ext.getCmp('idTitleEntrepreneur').setTitle('New Meeting');
			//
			/*timer = setInterval(function(){
				Mentor.app.application.getController('Main').calculateElapsedTime();
				console.log("a")
			},10000);*/
			
			Ext.getCmp('startSessionEditMettingDetail').setHidden(true);
			Ext.getCmp('endSessionEditMettingDetail').setHidden(true);
			Ext.getCmp('startSession').setHidden(false);
			Ext.getCmp('endSession').setHidden(false);
			
			//Ext.getCmp('idBtnUpdateTimingScreen').setHidden(true);
			
			
			/******* 22/02/2016 Timing Screen Move to Enterprenuer Screen***/
			// If user store the Start and End Session display back
			/*var MeetingTimingStartEndSession= Ext.decode(localStorage.getItem("storeStartEndSession"));
			if(MeetingTimingStartEndSession!=null){
				MeetingStartDatetime = MeetingTimingStartEndSession.StartDate;
				MeetingEndDatetime =  MeetingTimingStartEndSession.MeetingEndDatetime;
				MeetingElapsedTime =  MeetingTimingStartEndSession.MeetingElapsedTime;
				
				Ext.getCmp('startSession').setText(MeetingStartDatetime);
				Ext.getCmp('endSession').setText(MeetingEndDatetime);
				Ext.getCmp('MeetingElapsedTime').setValue( MeetingElapsedTime);
				
				Ext.getCmp('startSession').setDisabled(true);
				Ext.getCmp('endSession').setDisabled(true);
				Ext.getCmp('idBtnNextEnterpreneur').setDisabled(false);
			}*/
			/******* End 22/02/2016 Timing Screen Move to Enterprenuer Screen***/
			
		}
		/*** End ***/
		
		/*var MeetingTypePanel = Ext.getCmp('idMeetingType');
		MeetingTypePanel.removeAll();
		for(i = 0 ; i<MeetingTypeStore.data.length;i++){
			var checked = false;
			
			if(MeetingDetail == null){
				if(i == 0)
					checked = true;
				Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(true);
			}
			else{
				Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(false);
				// from Meeting History Screen
				if(MeetingDetail.MeetingTypeID == MeetingTypeStore.data.getAt(i).data.MeetingTypeID)
				{
						checked = true;
				}
			}
				
			var radioField = Ext.create('Ext.field.Radio', {
				name: 'recorded_stream',
				value: MeetingTypeStore.data.getAt(i).data.MeetingTypeID,
				label: MeetingTypeStore.data.getAt(i).data.MeetingTypeName,
				labelCls : 'radio-btn-cls',
				labelWidth: 250,
				checked : checked,
				listeners: {
					'check': function(radio, e, eOpts) {
						//me.radioHandler(radio.getValue());
					}
				}
			});
			MeetingTypePanel.add(radioField);
			
		}*/
		
		
		if(MeetingDetail!=null){
			Ext.getStore('Mentee').removeAll();
			var MenteeStore  = Ext.getStore('Mentee');
			MenteeStore.insert(0,{
				"MenteeName" : MeetingDetail.MenteeName,
				"MenteeID" : MeetingDetail.MenteeID,
				"TopicID" : MeetingDetail.MeetingTopicID,
				"MenteeComments" : MeetingDetail.MenteeInvitationComment
				});
			
			Ext.getCmp('idEnterpreneurSelectField').setDisabled(true);
			
			Ext.getCmp('idEnterpreneurSelectField').setValue(MeetingDetail.MenteeID);
			
			Ext.getCmp('idMettingTypeSelectField').setValue(MeetingDetail.MeetingTypeID);
			Ext.getCmp('idMeetingPlaceSelectField').setValue(MeetingDetail.MeetingPlaceID);
			Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(false);
		}
		else{
			Ext.getCmp('idBtnUpdateEnterpreneur').setHidden(true);
			Ext.getCmp('idEnterpreneurSelectField').setDisabled(false);
			Ext.getCmp("min").setHtml("00");
			Ext.getCmp("hrs").setHtml("00");
			
			//Ext.getCmp('idMettingTypeSelectField').setValue(MeetingTypeStore.getAt(0));
			//Ext.getCmp('idEnterpreneurSelectField').setValue(MeenteeStore.getAt(0));
		}
		
		/*var storeMeetingDetail = localStorage.getItem("storeMeetingDetail",Ext.decode(MeenteeDetail));
		if(storeMeetingDetail!=null){
			console.log(storeMeetingDetail);
		}
		else{
			console.log("asfsafsafsadfsadfs");
		}*/
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelSelectMenteeEnterpreneurScreen').setTitle('Select '+Mentor.Global.MENTEE_NAME);
		Ext.getCmp('idLabelMenteeCommentEnterpreneurScreen').setTitle(Mentor.Global.MENTEE_NAME + ' Comment');
		
	},
	onPagePainted : function(){
		
	}
            
       
});
