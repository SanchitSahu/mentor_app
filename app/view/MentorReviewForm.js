Ext.define('Mentor.view.MentorReviewForm', {
    extend: 'Ext.Panel',
    xtype: 'mentorreviewform',
    requires: [
    ],
    config: {
		cls: 'mentorreviewform',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
			indicators: {
				y: {
					autoHide: false
				}
			}
		},
		defaults:{
			xtype:'formpanel',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Mentor Review Form',
			cls:'my-titlebar-review_form',
			id :'idMentorReviewFormTitle',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					iconCls: 'btnBackCLS',
					action : 'btnBackMentorReviewForm'
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			html : Mentor.Global.VERSION_NAME,
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
		},
		{
					xtype: 'panel',
					margin : '10',
					layout: {
						type: 'vbox',
					},
					items:[
						
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting With Whom',
									cls : 'reviewFormLabelCLS',
									flex : 2
								},
								{
									xtype: 'textfield',
									name: 'meetingWithWhom',
									id: 'meetingWithWhomMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting Type',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textfield',
									name: 'meetingType',
									id: 'meetingTypeMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting Place',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textfield',
									name: 'meetingType',
									id: 'meetingPlaceMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting Subject',
									cls : 'reviewFormLabelCLS',
									flex : 2,
								},
								{
									xtype: 'textfield',
									name: 'meetingSubject',
									id: 'meetingSubjectMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						
						/*{
							xtype : 'label',
							html : 'Meeting Date',
							margin : '10 0 0 0'
						},
						{
							xtype: 'textfield',
							name: 'meetingDate',
							id: 'meetingmeetingDateMentorReviewForm',
							
						},*/
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting Start Time',
									cls : 'reviewFormLabelCLS',
									flex : 2,
									
								},
								{
									xtype: 'textfield',
									name: 'meetingStartTime',
									id: 'meetingStartTimeMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Meeting End Time',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textfield',
									name: 'meetingEndTime',
									id: 'meetingEndTimeReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Elapsed Time (Minutes)',
									cls : 'reviewFormLabelCLS',
									flex : 2,
								},
								{
									xtype: 'textfield',
									name: 'meetingElapsedTime',
									id: 'meetingElapsedTimeMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Subtopics Discussed',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textareafield',
									name: 'mainIssueDiscussed',
									id: 'mainIssueDiscussedMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									inputCls : 'inputCLS',
									maxRows: 2,
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMeetingDetail();
										}
									}
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Action Item By Mentor',
									id: 'idLabelActionItemByMentor',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textareafield',
									name: 'actionItemsByMentor',
									id: 'actionItemsByMentorMentorReviewForm',
									disabled :false,
									cls : 'textFieldDisableCLS',
									clearIcon  : false,
									maxRows: 2,
									inputCls : 'inputCLS',
									flex : 3,
									listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMentorActionItemsDetail();
										}
									}
									
								},
							]
						},
						
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Action Items By Mentee',
									id: 'idLabelActionItemByMentee',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textareafield',
									name: 'actionItemsByMentee',
									id: 'actionItemsByMenteeMentorReviewForm',
									disabled :true,
									disabledCls : 'textFieldDisableCLS',
									maxRows: 2,
									flex : 3,
									/*listeners : {
										element : 'element',
										tap : function() {
											Mentor.app.application.getController('AppDetail').changeMenteeActionItemsDetail();
										}
									}*/
									
								},
							]
						},
						{
							xtype: 'panel',
								margin : '10 0 0 0 ',
							layout: {
								type: 'hbox',
							},
							items:[
								{
									xtype : 'label',
									html : 'Mentor Comment',
									id: 'idLabelMentorComment',
									cls : 'reviewFormLabelCLS',
									flex : 2,

								},
								{
									xtype: 'textareafield',
									name: 'mentorComment',
									id: 'mentorCommentMentorReviewForm',
									cls : 'textFieldDisableCLS',
									maxRows: 2,
									disabled :false,
									inputCls : 'inputCLS',
									clearIcon : false,
									flex : 3,
								},
							]
						},
						{
							xtype: 'panel',
							margin : '10 0 0 0 ',
							style : 'background-color: #f2f2f2;',
							items:[
								{
									xtype : 'label',
									html : 'Satisfaction with meeting',
									margin : '20 0 0 0',
									cls : 'reviewFormLabelCLS',
									style : 'text-align: -webkit-center;'

								},
								{
									xtype : 'label',
									html : '1 being very poor and 5 being very good.',
									style : 'text-align: -webkit-center;font-weight: bold;font-size:14px;color: #808080;'

								},
								
								{
									xtype: 'ratingfield',
									name : 'usefull',
									label: ' ',
									labelWidth : '0%',
									value: 0,
									id:'idSatisfactionWithMeeting',
									labelCls : 'background:red',
									//style: 'background:red',
									style : 'background-color: #f2f2f2;'
								},
							]
						}
						
						
						
						
					]
		},
		
		{
			xtype : 'button',
			text : 'Submit',
			id : 'idBtnSubmitMentorReviewForm',
			cls : 'signin-btn-cls',
			action: "doSubmitMentorReviewForm",
			margin : '10,0,0,0'
		},
				
			
		
		],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelActionItemByMentor').setHtml('Action Item By '+Mentor.Global.MENTOR_NAME);
		Ext.getCmp('idLabelActionItemByMentee').setHtml('Action Items By '+Mentor.Global.MENTEE_NAME);
		Ext.getCmp('idLabelMentorComment').setHtml(Mentor.Global.MENTOR_NAME+ ' Comment' );
		
		
	}
            
       
});
