Ext.define('Mentor.view.MentorPendingRequest', {
    extend: 'Ext.dataview.List',
    xtype: 'mentorPendingRequest',
	itemId : 'itemIdmentorPendingRequest',
    requires: [
    ],
    config: {
		cls: 'meetinghistory',
		store : 'PendingRequest',
		itemTpl :'<div class="myContent" style = "display: flex;">'+ 
						'<div style="width:20%; float:left; ">'+ 
							'<img src={MenteeImage} id="cricle" height="50px" width="50px" style = "border-radius: 50%;">'+ 
						'</div>'+ 
						'<div style="width:80%;float:left;padding: 0px 0px !important;vertical-align: bottom;min-height: 16px;">'+
							
							'<div style="width:100%"><div style="width:100%; float:left;font-size: 14px;"><b>Invitation from {MenteeName}</b></div>' +
							
							//'<div style="width:100%;float:left;"><span style="font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
							'<div style="width:100%;float:left;margin-top:5px;">'+
								'<span style="float:left;font-size:11px;color: black;">Main Topic : {TopicDescription}</span>'+
								
								'<div style="float:right;">'+
									'<span style="float:left;vertical-align: baseline;"><img src="./resources/images/clock.png"  height="20px" width="20px"></span><span style="vertical-align: text-top;float:right;font-size:11px;color: black;">{inviteTime}</span></div>'+
							
						'<div>'+
				'</div>',
		
		
		
		/*	itemTpl: '<div class="myContent">'+ 
				'<div><b>Invitation from {MenteeName}</b></div>' +
				'</div>',*/
				
		listeners:[
			{
				event: "itemtap",
				fn : function(view, index, item, e)
				{
					Mentor.app.application.getController('AppDetail').respondPendingRequest(view, index, item, e);
				}
			},	
			{
				event: "activate",
				fn: "onPageActivate"
			}
		],
		items:[ 
			{
				docked: 'top',
				xtype: 'toolbar',
				title: 'Pending Invitations',
				cls:'my-titlebar',
				defaults: {
					iconMask: true
				},
				items:[
					{
						iconCls: 'refreshIcon',
						action : 'btnWaitScreen',
						docked: 'right',
						action : 'btnGetPendingRequest'
					},
					/*{
						iconCls: 'reply',
						action : 'btnAddMeetingDetail',
						docked: 'right',
					},*/
					{
						iconCls: 'btnBackCLS',
						action : 'btnBackMentorPendingRequest',
						docked: 'left',
						//style : 'visibility: hidden;',
					}
				]
			},
			{
				xtype : 'label',
				id : 'idUserNameMentorPendingRequest',
				style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
				docked: 'top',
				hidden : true,
			},
			{
				xtype : 'label',
				html : Mentor.Global.VERSION_NAME,
				style : 'text-align: right;padding-right: 10px;padding: 10px;',
				docked: 'bottom',
				hidden : true,
				
						
			},
			/*{
				xtype : 'container',
				layout: {
					type: 'hbox',
					pack: 'center',
					
				},
				docked: 'top',
				items:[{
					xtype : 'button',
					label : 'Add New Meeting',
					cls : 'signin-btn-cls',
					text : 'Log new meeting details',
					style : 'width:45%;margin-bottom:10px;font-size: 15px;',
					action : 'btnAddMeetingDetail',
				},
				{
					xtype : 'button',
					label : 'Add New Meeting',
					cls : 'signin-btn-cls',
					text : 'Pending Request',
					style : 'width:45%;margin-bottom:10px;font-size: 15px;',
					action : 'btnGetPendingRequest',
				}
				]
				
			}*/
		],
		
			
	},
	onPageActivate: function(){
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Mentor.app.application.getController('AppDetail').btnGetPendingRequest();
		
	}
            
       
});
