Ext.define('Mentor.view.EnterpreneurActionItems', {
    extend: 'Ext.Panel',
    xtype: 'enterpreneur_action_items',
    requires: [
    ],
    config: {
		cls: 'enterpreneur_action_items',
		//scrollable: "vertical",
		scrollable: {
			direction: 'vertical',
            indicators: {
				y: {
                    autoHide: false
                }
            }
        },
		defaults:{
			xtype:'container',
        },
		items:[ {
			docked: 'top',
			xtype: 'toolbar',
			title: 'Action Items',
			cls:'my-titlebar',
			defaults: {
                iconMask: true
            },
			items : [
				{ 
					//iconCls: 'arrow_left',
					iconCls: 'btnBackCLS',
					action : 'btnBackEnterpreneurActionItems'
					
				},
			]
		},
		{
			xtype : 'label',
			id : 'idUserNameEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;background-color: #C2C2C2;',
			hidden : true,
		},
		{
			xtype : 'label',
			id : 'idVersionEnterpreneur',
			style : 'text-align: right;padding-right: 10px;padding: 10px;',
			docked: 'bottom',
			hidden : true,
		},
		{
			xtype : 'panel',
			margin : '20 0 0 0',
			layout :{ type: 'vbox',},
			items:[
				{
					xtype:'label',
					html : 'Enterpreneur Action Items',
					style : 'margin-left:10px;margin-right:10px;border: 0px solid #eee;border-left-width: 0;border-bottom: 1px solid #66cc66;font-weight: bold;',
					id:'idLabelMenteeActionItemEnterpreneurActionItemScreen',
				},
				{
					xtype : 'panel',
					id : 'idEnterpreneurActionItemHeader',
					margin : '10 0 0 0',
					layout :{ type: 'hbox',},
					items : [
						{
							xtype:'label',
							html : 'Done',
							style : 'margin-left: 9px; font-size: smaller;',
						},
						{
							xtype:'label',
							html : 'ToDo',
							style : 'margin-left: 2%; font-size: smaller;',
						}
					]
				},
				{
					xtype : 'panel',
					margin : '10 0 0 0',
					layout :{ type: 'hbox',},
					items : [
						{
							xtype : 'panel',
							margin : '0 0 0 10',
							id : 'idMenteeActionItemDoneOnMeetingUpdate',
	
						},
						{
							xtype : 'panel',
							id : 'idEnterpreneurActionItem',
							style : 'width: 85%;'
						},
						/*{
							xtype : 'panel',
							id : 'idMenteeActionItemDoneOnMeetingUpdate',
	
						},*/
					]
				},
				/*{
					xtype: 'checkboxfield',
					name : 'color',
					value: '0',
					label: 'Practice',
					checked: true,
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '1',
					label: 'Market Validation',
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '0',
					label: 'Cold Calls',
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '1',
					label: 'Meeting with Client',
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '1',
					label: 'Develop Plans',
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '1',
					label: 'Write up Meeting Notes',
					labelWidth: 200,
					labelCls : 'radio-btn-cls'
				},
				{
					xtype: 'checkboxfield',
					name : 'color',
					value: '1',
					label: 'Develop Financial Projections',
					labelWidth: 250,
					labelCls : 'radio-btn-cls'
				},*/
				{
					xtype : 'container',
					layout :{ type: 'hbox'},
					items:[
						{
							xtype : 'button',
							text : 'Next',
							cls : 'signin-btn-cls',
							action: "btnNextEntrepreneurItems",
							id : 'idBtnNextEntrepreneurItems',
							flex : 1,
						},
						/*{
							xtype : 'button',
							text : 'Update',
							cls : 'signin-btn-cls',
							flex:1,
							hidden : true,
							id : 'idBtnUpdateEntrepreneurItems',
							action : 'btnUpdateEntrepreneurItems' //UpdateMeetingDetail Controller
		
						}*/
					]
				}
			]
		}
		
		],
		listeners:[{
			event: "activate",
			fn: "onPageActivate"
		}]
	},
	onPageActivate: function(){
		
		var MeetingDetail = Mentor.Global.MEETING_DETAIL; // From Meeting History
		
		var EnterpreneurActionItemStore = Ext.getStore('EnterpreneurAction').load();
		var MeetingDetailPanel = Ext.getCmp('idEnterpreneurActionItem');
		MeetingDetailPanel.removeAll();
		
		var MentorLoginDetail = Ext.decode(localStorage.getItem("idMentorLoginDetail"));
		var MenteeLoginDetail = Ext.decode(localStorage.getItem("idMenteeLoginDetail"));
		
		Ext.getCmp('idEnterpreneurActionItemHeader').setHidden(true); // Display headder
		
		Ext.getCmp('idVersionEnterpreneur').setHtml(Mentor.Global.VERSION_NAME);
		
		if(MentorLoginDetail!=null){
			Ext.getCmp('idUserNameEnterpreneur').setHtml(MentorLoginDetail.MentorName);
		}
		else if(MenteeLoginDetail!=null){
			Ext.getCmp('idUserNameEnterpreneur').setHtml(MenteeLoginDetail.MenteeName);
			Ext.getCmp('idEnterpreneurActionItemHeader').setHidden(false); // Display headder
			if(MeetingDetail != null){
				Ext.getCmp('idBtnNextEntrepreneurItems').setText('Update');
			}
		}
		
		
		
		for(i = 0 ; i<EnterpreneurActionItemStore.data.length;i++){
			var checked = false;
			
			if(MeetingDetail != null){
				var MenteeActionItems = MeetingDetail.MenteeActionDetail.split(',');
				var MenteeActionName;
				for(j = 0;j<MenteeActionItems.length;j++){
					var MenteeAction = MenteeActionItems[j].split('~')
					if(MenteeAction[0] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID)
					{
						checked = true;
						break;
					}
				}
			
				
			}

		
			var checkBoxField = Ext.create('Ext.field.Checkbox', {
				name: 'recorded_stream',
				value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
				label: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionName,
				labelCls : 'radio-btn-cls',
				//labelWidth: 280,
				checked : checked,
				labelWidth: '90%',
				labelAlign : 'right',
				listeners: {
					'check': function(radio, e, eOpts) {
						//me.radioHandler(radio.getValue());
					}
				}
			});
			MeetingDetailPanel.add(checkBoxField);
		}	
		if(MeetingDetail != null){
			var MenteeActionPanelDoneOnMettingUpdate = Ext.getCmp('idMenteeActionItemDoneOnMeetingUpdate');
			MenteeActionPanelDoneOnMettingUpdate.removeAll();
			for(i = 0 ; i<EnterpreneurActionItemStore.data.length;i++){
				var checked = false;
				var Disable = true;
				
				var MenteeActionItems = MeetingDetail.MenteeActionDetail.split(',');
				var MenteeActionName;
				for(j = 0;j<MenteeActionItems.length;j++){
					var MenteeAction = MenteeActionItems[j].split('~')
					var MenteeActionItemsDone = "";
					if(MeetingDetail.MenteeActionItemDone!=null)
						MenteeActionItemsDone = MeetingDetail.MenteeActionItemDone.split(',');
					if(MenteeAction[0] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID)
					{
						for(k = 0 ; k<MenteeActionItems.length;k++){
							if(MenteeActionItemsDone[k] == EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID)
								checked = true;
						}
						//checked = true;
						Disable = false;
						break;
					}
				}
					
				
			
				var checkBoxField = Ext.create('Ext.field.Checkbox', {
					name: 'recorded_stream',
					value: EnterpreneurActionItemStore.data.getAt(i).data.MenteeActionID,
					checked : checked,
					disabled : Disable,
					//labelWidth: 200,
					labelWidth: '90%',
					labelAlign : 'right',
					listeners: {
						'check': function(radio, e, eOpts) {
							//me.radioHandler(radio.getValue());
						}
					}
				});
				MenteeActionPanelDoneOnMettingUpdate.add(checkBoxField);
			}
		}
		
		//Change the lable of Mentor and Mentor where Name Exits according to Application Configuration
		Ext.getCmp('idLabelMenteeActionItemEnterpreneurActionItemScreen').setHtml(Mentor.Global.MENTEE_NAME +' Action Items');
		
		
	}
            
       
});
