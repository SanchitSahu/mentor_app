Ext.define('Mentor.model.WaitScreenMentor', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'InvitationId', type: 'string' },
            { name: 'MentorComments', type: 'string' },
			{ name: 'MentorID', type: 'string' },
			{ name: 'MentorName', type: 'string' },
			{ name: 'Status', type: 'string' },
			{ name: 'MentorImage', type: 'string' },
			{ name: 'TopicDescription', type: 'string' },
			{ name : 'inviteTime' , type : 'string'}
        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
