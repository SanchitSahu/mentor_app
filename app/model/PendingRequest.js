Ext.define('Mentor.model.PendingRequest', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'InvitationId', type: 'string' },
            { name: 'MenteeID', type: 'string' },
			{ name: 'MenteeName', type: 'string' },
			{ name : 'ManteeComments',type : 'ManteeComments'},
			{ name : 'TopicDescription' , type : 'TopicDescription'},
			{ name : 'MenteeImage' , type : 'String'},
			{ name : 'inviteTime' , type : 'String'},
			
        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
