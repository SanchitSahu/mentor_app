Ext.define('Mentor.model.InviteMentor', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'MentorID', type: 'string' },
            { name: 'MentorName', type: 'string' },
			{ name: 'rStatus', type: 'string' },
			
        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
