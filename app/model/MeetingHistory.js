Ext.define('Mentor.model.MeetingHistory', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'EditedMeetingEndDatetime', type: 'string' },
            { name: 'EditedMeetingStartDatetime', type: 'string' },
			{ name: 'MeetingElapsedTime', type: 'string' },
            { name: 'MeetingEndDatetime', type: 'string' },
			{ name: 'MeetingFeedback', type: 'string' },
            { name: 'MeetingID', type: 'string' },
            { name: 'MeetingPlaceID', type: 'string' },
			{ name: 'MeetingPlaceName', type: 'string' },
            { name: 'MeetingStartDatetime', type: 'string' },
			{ name: 'MeetingSubTopicID', type: 'string' },
            { name: 'MeetingTypeID', type: 'string' },
			{ name: 'MeetingTopicID', type: 'string' },
            { name: 'MeetingTypeName', type: 'string' },
            { name: 'MenteeActionDetail', type: 'string' },
            { name: 'MenteeID', type: 'string' },
			{ name: 'MenteeName', type: 'string' },
            { name: 'MentorActionDetail', type: 'string' },
			{ name: 'MentorID', type: 'string' },
            { name: 'MentorName', type: 'string' },
            { name: 'SubTopicDescription', type: 'string' },
            { name: 'TopicDescription', type: 'string' },
			{ name: 'SubTopicName', type: 'string' },
			{ name: 'MenteeImage', type: 'string' },
			{ name: 'MentorImage', type: 'string' },
			{ name: 'MenteeInvitationComment', type: 'string' },
			
			
			{ name: 'MentorActionItemDone', type: 'string' },
			{ name: 'MenteeActionItemDone', type: 'string' },

            { name: 'MentorComment', type: 'string' },
			{ name: 'SatisfactionIndex', type: 'string' },

			{ name: 'MenteeComment', type: 'string' },
			{ name: 'CareA', type: 'string' },
			{ name: 'CareC', type: 'string' },
			{ name: 'CareE', type: 'string' },
			{ name: 'CareR', type: 'string' },
			
			{ name: 'average', type: 'string' },
			{ name: 'averageText', type: 'string' },
			
        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
