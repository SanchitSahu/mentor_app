Ext.define('Mentor.model.MeetingType', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'MeetingTypeName', type: 'string' },
            { name: 'MeetingTypeID', type: 'string' },
           

        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
