Ext.define('Mentor.model.SubTopic', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'SubTopicDescription', type: 'string' },
            { name: 'SubTopicID', type: 'string' },
			{ name : 'TopicID' ,type : 'string'},
           

        ],
        /*proxy: {
        	type: "awsajax",
        	url: AllInOneWorldSport.Global.SERVER_URL + "/ListRootLevel",
        }*/
    }
});
